let base_url  = 'http://localhost/projects/chatzap/Chat/';
let base_url2 = 'http://localhost/projects/chatzap/';
let id_       = null;
let click     = false;
let baseUrl   = [];

$.getJSON("../config.json", (json) => processaJson(json));

$(document).ready(() => {
    carregarusuarios();
    carregarmensagens(null, false);

    console.log($('.messages-box').height())
});

setInterval(() => {
    carregarmensagens(id_, click);
}, 1000);

function processaJson(data) {
    baseUrl.push(data)
}

function apagarmensagens() {
    $.ajax({
        type:'POST',
        url:'apagarmensagens.php',
        success: (msg) => {
            console.log(msg)
        },
        error: (e) => {
            console.log(e)
        }
    });
}

function carregarmensagens(id_contato = null, click = false) {
    let montaMessages = (data) => {
        let str = `
            <br><br><div class="row">
                <div class="offset-2 col-8">
                    <div style="text-align: center">
                        <div class="card wa-card-chat wa-card-yellow">
                            As mensagens que você enviar e as ligações que você fizer nessa conversa estão protegidas com criptografia de ponta-a-ponta.
                            Clique para mais informações.
                        </div> 
                    </div>
                </div>
            </div>
        `;
        
        if(data) {
            let d = JSON.parse(data);

            d.messages.forEach((messages) => {
                if(d.id_sender == messages.id_de) {
                    str += 
                    `<br><div class="row">
                        <div class="offset-6 col-5">
                            <div class="card wa-card-chat wa-card-green">
                                `+messages.mensagem+`
                                <div style="text-align: right">
                                    <span>`+messages.data_hora+`</span>
                                    <i class="large material-icons wa-icon wa-chat-icon">done_all</i>
                                </div>
                            </div>
                        </div>
                    </div><br>`
                } else {
                    str += 
                    `<div class="row">
                        <div class="col-5">
                            <div class="card wa-card-chat wa-card-default">
                            `+messages.mensagem+`
                                <div style="text-align: right">
                                    <span>`+messages.data_hora+`</span>
                                </div>
                            </div>
                        </div>
                    </div><br>`;
                }

                $('.wa-panel-texto').show();
                $('.navbar-message').css('display', 'flex');
            });

            $('.messages-box').html(str);
        } else { 
            //$('.wa-panel-texto').hide();
            //$('.navbar-message').css('display', 'none');
        }

        if(click) {
            $('.messages-box').html(str);
            $('.wa-panel-texto').show();
            $('.navbar-message').css('display', 'flex'); 

            console.log($('.messages-box').height())
            //fixBottom();
        }
    }

    $.ajax({
        type: 'POST',
        url: base_url+'returnMessages',
        data: {
            id_contato: id_contato
        },
        success: (data) => {
            montaMessages(data);
        }, 
        error: (e) => {
            console.log(e)
        }
    });
}

function fixBottom() {
    $(".messages-box").animate({ 
        scrollTop: 500 
    }, "slow");
    
    return false;
}

function carregarusuarios() {
    let montaMenu = (data) => {
        let ellist = $('.list-contacts');

        JSON.parse(data).forEach((user) => {
            let mensagem = user.mensagem ? user.mensagem : 'Clique para iniciar..';
            let data = user.data ? user.data : '';
            let countmsg = user.mensagem ? 1 : '';

            let template = `
            <div class="row wa-item-chat" id="`+user.id+`">
                <div class="col-2">
                    <img src="`+base_url2+`assets/images/profile.png" class="rounded-circle"/>
                </div>
                <div class="col-6">
                    <b id="name-`+user.id+`">&nbsp;`+ user.nome+`</b><br/>
                    <span style="display: none" id="email-`+user.id+`">`+ user.email+`</span>
                    <p class="wa-preview-message">&nbsp;`+ mensagem+`</p>
                </div>
                <div class="col-4" style="text-align: right">
                    <span>`+data+`</span>
                    <span class="badge badge-pill wa-badge">`+countmsg+`</span>
                </div>
            </div>
            <hr/>`;

            ellist.append(template);

            $('#'+user.id).on('click', function() {
                click = true;
                id_ = user.id;

                carregarmensagens(id_, true);
                
                let nome  = $('#name-'+id_).text();
                let email = $('#email-'+id_).text();
                let inf   = nome.trim()+ ' &lt;' + email + '&gt; ';

                $('#nome_contato').html(inf);
                $('#status_contato').html('Online');
            });
        });
    }

    $.ajax({
        type: 'GET',
        url: base_url+'returnListUsers',
        success: (data) => {
            montaMenu(data);
        }, 
        error: (e) => {
            console.log(e)
        }
    });
}

$('.sent').on('click', function(){
    enviar();
});

$(document).on('keypress',function(e) {
    if(e.which == 13) {
        enviar(); 
    }
});

function enviar() {
    if($('#mensagem').val() == '') {
        alert('Digite uma mensagem!');
    } else {    
        $.ajax({
            type:'POST',
            url: base_url+'sendMessage',
            data: { 
                mensagem: $("#mensagem").val(),
                id_contato: id_ 
            },
            success: (msg) => {
                if(msg == 'OK') {
                    $("#mensagem").val('');
                    carregarmensagens(null, false);
                    //fixBottom();
                }
            }, 
            error: (e) => {
                console.log(e)
            }
        });
    }

    return false;
}