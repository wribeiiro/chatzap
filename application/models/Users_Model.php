<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_Model extends CI_Model {

	public function __construct() {
		parent::__construct(); 
	}
 
 	public function getUser($email, $pass) {

		$sql   = "SELECT * FROM usuarios WHERE email  = '$email' AND senha = '$pass' LIMIT 1";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0) {
			return $query->result_array();
		}

		return false;
	}
	
	public function updateWork($id, $inicio) {

		$this->db->set('inicio', $inicio);
		$this->db->where('id', $id);
		$this->db->update('usuarios');
		
		if($this->db->affected_rows() >= 0) {
			return true;
		}

		return false;
	}
	
	public function getALlUsers($iduserlogged) {

		$sql   = "SELECT usuarios.nome as nome, usuarios.email as email, usuarios.id as id, '00:00' as data, '' as mensagem
		FROM usuarios WHERE usuarios.id <> '$iduserlogged'";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0) {
			return $query->result_array();
		}

		return false;
	}
}