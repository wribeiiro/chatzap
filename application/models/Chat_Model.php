<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat_Model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
 
    public function getAllMessages($id_contato, $id_sender) {
        $sql = "SELECT id, id_de, id_para, mensagem, DATE_FORMAT(data_hora, '%H:%m') as data_hora FROM mensagens WHERE (id_de = '$id_sender' AND id_para = '$id_contato') OR (id_de = '$id_contato' AND id_para = '$id_sender') ORDER BY id asc";

		$query = $this->db->query($sql);
		
		return $query->result_array();
    }

    public function insertMessages($array_mensagem) {
        $this->db->insert('mensagens', $array_mensagem);
        
        if($this->db->affected_rows() > 0) {
            return true;
        }

        return false;
    } 

    public function deleteMessages() {
        if($this->_db->delete('mensagem')) {
            return true;
        }

        return false;
    }
}