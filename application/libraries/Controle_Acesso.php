<?php 
    class Controle_Acesso {

        public function __construct() {

        }

        public function controlar() {
            $CI   = &get_instance();
            $user = $CI->session->userdata("sessao_user");
            
            if (empty($user)) {
                $CI->session->set_flashdata('erro_login', 'Você não tem permissão para acessar este conteúdo!');
                
                redirect('');
            } else {
                redirect('Chat/index');
            }
        }
    }
?>