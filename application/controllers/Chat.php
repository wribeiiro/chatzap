<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {
    private $last_activity;

	public function __construct() {

		parent::__construct();
        $this->load->model('Chat_Model', 'chat');
        $this->load->model('Users_Model', 'userm');
        $this->load->library('Controle_Acesso', 'controle_acesso');

        $this->last_activity = date('Y-m-d H:m:s');

        //$this->controle_acesso->controlar();
    }
    
	public function index() {
		$this->dados['titulo'] = 'ChatZap';
		$this->load->view('chat/index', $this->dados);
    }
    
    public function auth() {
        $this->dados['titulo'] = 'Autenticaçao ChatZap';
		$this->load->view('chat/auth', $this->dados);
    }

    public function autenticar() {
        $email = addslashes(strtoupper($this->input->post("email")));
        $pass  = addslashes(md5($this->input->post("pass")));

        $user  = $this->userm->getUser($email, $pass);

        if($user) {
            $this->userm->updateWork($user[0]['id'], $this->last_activity);
            
            $this->session->set_userdata("sessao_user", $user);
            redirect('Chat/index');
        } else {
            $this->session->set_flashdata('erro_login', 'Login/Senha inválidos!');
            redirect('');	
        }        
    }

    public function checkUser() {
        
        $sessao = $this->session->userdata('sessao_user')[0];

		if($sessao['inicio']) {
            $segundos = $this->last_activity - $sessao['inicio'];
        }

        if($segundos >= $sessao['inicio']) {
            $this->userm->updateWork($sessao['id'], $this->last_activity);
            
            $this->session->unset_userdata("sessao_user");
            $this->session->sess_destroy();
            $this->session->set_userdata("sessao_user", "");

        } else {
            $this->userm->updateWork($sessao['id'], $this->last_activity);
        }
    }

	public function logoff(){
		if($this->userm->updateWork($this->session->userdata['sessao_user'][0]['id'], $this->last_activity)) {
            $this->session->unset_userdata("sessao_user");
            $this->session->sess_destroy();
		    $this->session->set_userdata("sessao_user", ""); 
        
            $this->session->set_flashdata('erro_login', 'Sua sessão foi encerrada!');
            
            redirect('');
        } 
    }
    
    public function returnListUsers(){
        $result = $this->userm->getALlUsers($this->session->userdata['sessao_user'][0]['id']);
        
        if($result) {
            echo json_encode($result);
        }
    }
    
    public function returnMessages(){
        $id_contato = $this->input->post('id_contato');
        $id_sender  = $this->session->userdata['sessao_user'][0]['id'];

        $result = $this->chat->getAllMessages($id_contato, $id_sender);
        
        if($result) {
            $arr['id_sender'] = $id_sender;
            $arr['messages']  = $result;
            
            echo json_encode($arr);
        }
    }
    
    public function sendMessage(){
        $id_contato = $this->input->post('id_contato');
        $id_sender  = $this->session->userdata['sessao_user'][0]['id'];
        $mensagem   = $this->input->post('mensagem');

        $result = $this->chat->insertMessages(array(
            'id_de'     => $id_sender,
            'id_para'   => $id_contato,
            'nick'      => '',
            'mensagem'  => $mensagem,
            'ip'        => $_SERVER['REMOTE_ADDR'],
            'data_hora' => date('Y-m-d H:m:i')

        ));
        
        if($result) {
            echo 'OK';
        }
	}
}