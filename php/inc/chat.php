	<?php require_once 'header.php' ?>
	<div class="container-fluid" style="padding-top: 60px;">
	    <div class="col-sm-8 col-md-8 col-md-offset-2">
		    <div class="row">
		    	<div class="col-md-12">
		    		<p class="text-center">Usuarios Disponiveis <span style="color: yellow"> *</span></p>
		    		<div class="text-center" id="usuarios"></div>
		    	</div>
		    </div>
	        <div class="col-xs-12 col-md-12">
	        	<div class="panel panel-default panel-collapsed" >
	                <div class="panel-heading top-bar">
	                    <div class="col-md-12 col-sm-12">
	                        <h3 class="panel-title">
	                        	<span class="fa fa-comments" style="text-align: center;"></span> 
	                        	Logado como: <?php echo $nick; ?>
	                        </h3>
	                    </div>
	                </div>
	                <div class="panel-body msg_container_base" id="mensagens" style="background: url('./img/bg.png') repeat;">

                    </div>
	                <div class="panel-footer">
	                    <form onsubmit="enviar();return false" method="post" action="">
	                        <div class="input-group">
		                        <input type="text" class="input-group form-control input-sm chat_inpu" id="mensagem" placeholder="Digite aqui sua mensagem..." required="" maxlength="255" autofocus>
		                        <span class="input-group-btn">
		                        	<button class="btn btn-primary btn-sm" id="btn-chat">
		                        		<i class="fa fa-microphone"></i>
		                        	</button>
		                        </span>
		                    </div>
		                    <input type="submit" style="display:none">
		                </form>
	                </div>
	    		</div>
	        </div>
	   
		    <div class="btn-group dropup">
		        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		            <span class="fa fa-cog"></span>
		            <span class="sr-only">Toggle Dropdown</span>
		        </button>
		        <ul class="dropdown-menu" role="menu">
		            <li>
		            	<a href="#">
		            		<span class="fa fa-remove"></span>
		            			<input type='button' style="background-color: transparent; border: none" id='limpar' value='Limpar Conversa'/>
		            		</span>
		            	</a>
		            </li>
		            <li class="divider"></li>
	           		<li>
	           			<a href="inc/logout.php"><span class="fa fa-close"></span> 
	           			Sair</a>
	           		</li>
		        </ul>
		    </div>
		</div>
	</div>
	<?php require_once 'footer.php'; ?>