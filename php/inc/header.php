<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>
		Bate Papo | <?php if (isset($nick)) { echo $nick; } else { echo " Entrar"; }?>
	</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script type="text/javascript" src="http://code.jquery.com/jquery-2.2.3.js"></script>
	<script type="text/javascript" src="plugins/bootstrap/js/bootstrap.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="plugins/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/chat.css">
	<link rel="shortcut icon" href="img/user.png"/>
</head>
<body>