<?php 
	require_once("inc/config.php"); // Retorna o arquivo de configurações do site.
	if (isset($_SESSION['usuarioNome'])) {
		$query = "SELECT * FROM usuarios";
		$sql = mysqli_query($con, $query);
		if (mysqli_num_rows($sql) > 0) {
			echo "<ul>";
				while($ln = mysqli_fetch_assoc($sql)){
					$id_usuario = $ln['id'];
					$usuario    = $ln['nome'];
					echo "<li style='color: #fff; display: inline; margin: 5px';></li><a href='#' style='color: #fff'>{$usuario} </a>";
					echo "-";
				}
			echo "</ul>";
		} else {
			echo "<p style=\"color: #303030\">Aguardando início da conversa...</p>";
		}
	} else {
		require_once("inc/nick.php"); // Retorna o arquivo para definir um nick
	}
?>