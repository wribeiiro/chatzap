$(document).ready(function() {
    carregarmensagens();
    
    setInterval(() => {
        carregarmensagens();
    }, 1000);

	var valor = $("#mensagem");
	valor.keyup(function(){
		if(valor.val() != '') {
			$("#btn-chat").find('i').removeClass('fa fa-microphone');
			$("#btn-chat").find('i').addClass('fa fa-send');
		}

		if(valor.val() == '') {
			$("#btn-chat").find('i').removeClass('fa fa-send');
			$("#btn-chat").find('i').addClass('fa fa-microphone');
		}
	});

	$("#mensagem").attr('autocomplete','off');

	$("btn-chat").click(function(){
		enviar();
	});

	$("#limpar").click(function(){
		apagarmensagens();
           
	   	return false;
	});

	carregarusuarios();
});

function apagarmensagens() {
    $.ajax({
        type:'POST',
        url:'apagarmensagens.php',
        success: (msg) => {
            console.log(msg)
        },
        error: (e) => {
            console.log(e)
        }
    });
}

function carregarmensagens() {
    $.ajax({
        type: 'GET',
        url: 'vermensagens.php',
        success: (data) => {
            $("#mensagens").html(data);
        }, 
        error: (e) => {
            console.log(e)
        }
    });
}

function carregarusuarios() {
    $.ajax({
        type: 'GET',
        url: 'listausuarios.php',
        success: (data) => {
            $("#usuarios").html(data);
        }, 
        error: (e) => {
            console.log(e)
        }
    });
}

function enviar() {
    $.ajax({
        type:'POST',
        url:'enviarmensagem.php',
        data: { mensagem: $("#mensagem").val() },
        success: (msg) => {
            if(msg == 'OK') {
                $("#mensagem").val('');
                carregarmensagens();
            }
        }, 
        error: (e) => {
            console.log(e)
        }
    });
}