-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 12-Jul-2017 às 22:42
-- Versão do servidor: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `help-desk`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `base_conhecimento`
--

CREATE TABLE `base_conhecimento` (
  `id` int(11) NOT NULL,
  `id_plataforma` int(11) NOT NULL,
  `pergunta` text NOT NULL,
  `resposta` text NOT NULL,
  `anexo` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `base_conhecimento`
--

INSERT INTO `base_conhecimento` (`id`, `id_plataforma`, `pergunta`, `resposta`, `anexo`) VALUES
(8, 1, 'Como atualizar sisauto?', '1- Acessa o servidor, usando a tecla Windows + R e digita \\\\192.168.2.245.\nPedirá user e pass\nuser: dados1\npass: nova@123\n2- em seguida acessar a pasta dados -> publico -> dropbox -> public\n3- nela pegar sisauto.exe e bdupdate.exe mais atuais\n4- enviar para pasta c:\\sisauto \n5- executar bdupdate2.exe modo completo\n6- iniciar sisauto atualizado', '7c0f0ecf20c5b5e881df252d20eeb38f.png'),
(10, 2, 'Como atualizar sisauto?', '1- Acessa o servidor, usando a tecla Windows + R e digita \\\\192.168.2.245.\r\nPedirá user e pass\r\nuser: dados1\r\npass: nova@123\r\n2- em seguida acessar a pasta dados -> publico -> dropbox -> public\r\n3- nela pegar sisauto.exe e bdupdate.exe mais atuais\r\n4- enviar para pasta c:\\sisauto \r\n5- executar bdupdate2.exe modo completo\r\n6- iniciar sisauto atualizado', 'c3cf99c26c0b212c7bdb7e54bf4f7456.png'),
(11, 1, 'erro envio email genus', 'testar acbrmonitorplus - envio de email - verificar antivirus - adicionar excecao pra porta 587 ou 465 - e habilitar desabilitar tls e ssl em opcoes de internet - avancadas', '7bc14a5d4f2a81073d20be79a9e5bc83.png'),
(12, 2, 'Testando envio imagem', 'carregamento da imagem no servidor ', '5a46db5ace72af752e91bc3432e9e431.png'),
(14, 3, 'Certificado digital', 'Para excluir, importar ou exportar um certificado digital vÃ¡ em painel de controle > opÃ§Ãµes da internet > conteÃºdo --------------------------------------------------------------------------------------- Para instalar um novo certificado digital > dois cliques no certificado, avanÃ§ar sempre > marcar as duas opÃ§Ãµes de baixo > avanÃ§ar > concluir ', '68b08b14b286f8b338dfca82db7ab243'),
(15, 2, 'Base de dados', 'Como jogar base de dados no servidor da support >> 192...> Clientes >dados_1 > pasta da empresa > CRIAR NOVA PASTA COM ANOMÃŠSDIA > colocar arquivo desejado > ARQUIVOS ( BDMARC, BDCONFIG, BDFISCAL --------------------------------------------------------------------------------------- SISAUTO... > config > Caminho --------------------------------------------------------------------------------------- UtilitÃ¡rios > manuntenÃ§Ã£o > SENHA: sis3642 ', '481a3ef7cecfb16f2f28367e2727fd00'),
(16, 3, 'Xml nfe', 'Baixar xml > clicar na chave xml> copiar cÃ³digo > pesquisar no site > consultar nfe completa > fazer download com certificado digital pelo internet explorer ', '03789368e483b9768e9908a686efa685'),
(17, 1, 'genus nota de entrada', '\r\n\r\ncliente precisa emitir nota com cfop 2202 - entrada do cliente\r\ndai no genus nao ta aceitando\r\ncomo deve ser feito nesse caso ? ele so aceita emitir com cfops iniciando com 5 ou 6\r\ncomo fazer quando cliente precisar emitir nota com cfop de entrada ?\r\n\r\nR: ref. A NF Ele lanÃ§a uma NF de entrada e marca a opÃ§Ã£o NFprÃ³pria. neste caso aparece no gerador. \r\n\r\n', '08d1bf93faa3501d77e594b05daa45b2'),
(18, 2, 'campos boletos sisauto support', '08-03-17 ficou campo informacoes comerciais-referencias comerciais - NFS para clientes que vai nota e campos abaixo pra citar vlr hosp e arrendamento e dia venc.', '30b3f2394911d43a05c50916e90fa216'),
(19, 2, 'Configurar ACBRmonitorplus', 'Instalar ACBR >>> Criar duas pastas na pasta acbrmonitoplus: entrada e saida >>> Em monitor\r\nmarque a opcao monitorar pasta, em entrada coloque ENTRADA e saida coloque SAIDA\r\n>>> Em Email no nome coloque o nome da empresa em seu email coloque Nfe.naoresponder@gmail.com em usuario Nfe.naoresponder@gmail.com em host SMTP coloque SMTP.gmail.com, marcar as opcoes SSL e TLS e em codificacao marcar UTF_8\r\ne em senha jean19sms e em porta coloque o valor 465 , apos clique testar configuracao\r\n>>> na pasta gernfe2 entre em config e na ultima linha escreva monitoracbr=S salve', '77a834374ea77079f09fd02d58c0505f'),
(20, 3, 'Liberar permissao Terminal Server para usuarios', 'Abrimos o \"Group Policy Managemente\" e editar a GPO \"Default Domain Policy\".\r\n\r\ncomputer configuration > policies > Windows settings > security settings > local policy > user rights assignment > \"permitir logar atravÃ©s do serviÃ§o de terminal services\".\r\n\r\nAdicionamos os grupos ou usuÃ¡rios para liberar a conexÃ£o via TS', 'f3d74adcf91440fff7be54feedcfa4ac.png'),
(21, 3, 'Valida Cliente Support', '\"Opa\r\n\r\nPara fazer o bloqueio e desbloqueio basta acessar o link:\r\nhttp://support-br.com.br/william/utilitarios\r\n\r\ncolocar a senha t568a\r\n\r\n--\r\n\r\nse o usuario ja estiver cadastrado, tem que ir em \"\"alterar situacao \r\ncliente\"\" digitar o cnpj, e selecionar se estÃ¡ em atraso ou em dia.\r\n\r\nse nao estiver, basta cadastrar na tela inicial, com um nome, e cnpj - \r\nele nao permite cadastrar o mesmo cnpj duas vezes. entao caso ja esteja \r\ncadastrado, vai aparecer uma msgbox.\r\n\r\nate  BLOQUEIO CLIENTES NFE\"	\"sÃ³ pra deixar registrado dados de conexao com a base de dados de bloqueio de clientes\r\nHost localhost\r\nusuario supportb_william\r\nsenha t568a\r\nnome  supportb_BdValidaClie \"\r\n\r\n**********\r\ncopiar a base e php acima pro site\r\nbrasilnota.com.br e configurar pra rodar por ele - ou entao verificar pra usar o modelo de codigo com a base do sistema help desk - de forma que retorne o valor N caso cliente nao esteja em dia ou caso o campo Atualizar automatico esteja como N - esse campo de atualizar automatico sera um campo novo pra mudarmos pelo help desk se ira atualizar cliente ou nao', '971e056fe40999c0cd02126295145e14');

-- --------------------------------------------------------

--
-- Estrutura da tabela `chamados`
--

CREATE TABLE `chamados` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `problema` varchar(200) NOT NULL,
  `data` date NOT NULL,
  `data_modif` date DEFAULT NULL,
  `conexao` text,
  `solucao` text,
  `status` char(1) NOT NULL DEFAULT 'A',
  `tempo` time DEFAULT '00:00:00',
  `anexo` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `chamados`
--

INSERT INTO `chamados` (`id`, `id_cliente`, `id_usuario`, `problema`, `data`, `data_modif`, `conexao`, `solucao`, `status`, `tempo`, `anexo`) VALUES
(17, 21, 2, 'Atualizar executÃ¡vel fiado', '2017-02-06', NULL, NULL, 'Enviado executavel atual', '2', '00:12:00', NULL),
(18, 32, 2, 'Erro ao cancelar NFE e erro cupom fiscal', '2017-02-06', NULL, NULL, 'Cliente estÃ¡ usando ECF com data de dia anterior (BEMATECH MP-2100), orientado ao cliente para ligar a WLE e depois nos ligar para atualizaÃ§Ã£o do checkout.', '2', '00:12:00', NULL),
(19, 33, 2, 'Enviar XML e PDF nas NFE', '2017-02-06', NULL, NULL, 'Gerar XML e PDF das NFE do mÃªs de janeiro\r\nno gerenciador de NFE e no arquivo, exportar NFE por periodo\r\ndepois informar do dia 01/01/2017 ao 31/01/2017 e informado o email\r\nsupport e mancar a opÃ§Ã£o PDF e enviar canceladas e depois clicar em\r\nenviar.', '2', '00:12:00', NULL),
(20, 34, 2, 'mapear nova estaÃ§Ã£o', '2017-02-06', NULL, NULL, 'Efetuado mapeamento de estaÃ§Ã£o estoque, \r\nmapeado o servidor \\user-canto\r\ncom a letra Y\r\n', '2', '00:12:00', NULL),
(21, 35, 2, 'NÃ£o saiu dados adicionais na nota', '2017-02-06', NULL, NULL, 'Ficou de mandar executÃ¡vel para eles testarem', '2', '00:12:00', NULL),
(22, 41, 2, 'problema no calcelamento de NF-e 9014', '2017-02-07', NULL, NULL, 'Deletado na tabela de cancelamento do arquivo gernfe.mdb a chave da nota que nÃ£o estava cancelando', '2', '00:12:00', NULL),
(23, 42, 2, 'Erro no uninfe', '2017-02-07', NULL, NULL, 'Instalado versÃ£o atual do uninfe', '2', '00:12:00', NULL),
(24, 33, 2, 'NF-e denegada', '2017-02-07', NULL, NULL, 'Orientado ao cliente a entrar em contato com seus clientes, porque denegado significa que cliente estÃ¡ com algum problema na inscriÃ§Ã£o estadual ou CNPJ.', '2', '00:12:00', NULL),
(25, 43, 2, 'Programa indesejado BityFense', '2017-02-08', NULL, NULL, 'Programa desinstalado pelo painel de controle (desinstalar um programa).', '2', '00:12:00', NULL),
(26, 44, 2, 'XML incorreto do CT-e', '2017-02-08', NULL, NULL, 'Efetuado validaÃ§Ã£o no site da receita, ver o caso com a contabilidade', '2', '00:12:00', NULL),
(27, 40, 2, 'uninfe nÃ£o inicializa automaticamente ao iniciar o micro (windows 10)', '2017-02-08', NULL, NULL, 'Configurado uninfe para executar na inicializaÃ§Ã£o e para executar em modo administrador, pelo executar do windows no caminho: %AppData%MicrosoftWindowsStart MenuProgramsStartup (Colocar atalho neste caminho). \r\n\r\n\r\n', '2', '00:12:00', NULL),
(28, 40, 2, 'Erro emissÃ£o nfe x03', '2017-02-08', NULL, NULL, 'Utilitarios > configuraÃ§Ãµes 3 > 3085 > valor mudar para 2', '2', '00:12:00', NULL),
(29, 45, 2, 'Atualizar sisauto para entrada de notas', '2017-02-08', NULL, NULL, 'sisauto atualizado em todas as estaÃ§Ãµes', '2', '00:12:00', NULL),
(30, 46, 2, 'Atualizar sisauto no servidor e no remoto', '2017-02-09', NULL, NULL, 'Atualizados', '2', '00:12:00', NULL),
(31, 47, 2, 'Repassar duas NFC-E de janeiro', '2017-02-09', NULL, NULL, 'Conectado e repassado pelo utilitÃ¡rios NFC-e', '2', '00:12:00', NULL),
(32, 38, 2, 'Instalar certificado digital', '2017-02-09', NULL, NULL, 'Foi instalado (em opÃ§Ãµes da internet > conteÃºdo foi excluÃ­do os certificados antigos)  ', '2', '00:12:00', NULL),
(33, 48, 2, 'Trazer Nfce para enviar a contabilidade', '2017-02-10', NULL, NULL, 'Recolhidos NFCE da otica real e popular enviados no email support...', '2', '00:12:00', NULL),
(34, 48, 2, 'NFC-E 639 nÃ£o encontrado da otica popular', '2017-02-10', NULL, NULL, 'sisauto otica popular atualizado e nota 639 inutilizada com sucesso', '2', '00:12:00', NULL),
(35, 21, 2, 'Problema com impressora MP4200 TH', '2017-02-10', NULL, NULL, 'app da impressora instalado', '2', '00:12:00', NULL),
(36, 49, 2, 'Solicitado XML mÃªs janeiro', '2017-02-10', NULL, NULL, 'XML arquivada em um pasta na Ã¡rea de trabalho \"XML\"\r\nmodo de soluÃ§Ã£o (utilitÃ¡rios > NFCe > Enviar NFCE por perÃ­odo > data 01 / 2017)', '2', '00:12:00', NULL),
(37, 45, 2, 'Atualizar sisauto', '2017-02-13', NULL, NULL, 'Atualizados em todas as estaÃ§Ãµes', '2', '00:12:00', NULL),
(38, 50, 2, 'Erro no MSLICENSING, nÃ£o abre conexÃ£o remota', '2017-02-13', NULL, NULL, 'em iniciar> executar > regedit > mslicensing excluir.', '2', '00:12:00', NULL),
(39, 51, 2, 'Erro outlook', '2017-02-13', NULL, NULL, 'Na caixa Abrir, digite OUTLOOK /RESETNAVPANE  e clique em OK.\r\nresolveu\r\nhttps://answers.microsoft.com/pt-br/msoffice/forum/msoffice_outlook-mso_other/n%C3%A3o-%C3%A9-poss%C3%ADvel-iniciar-o-outlook/f2cd4a94-6180-4390-ad75-be4d5f3779e7', '2', '00:12:00', NULL),
(40, 46, 2, 'Atualizar sisauto no servidor e no remoto', '2017-02-13', NULL, NULL, 'Atualizados', '2', '00:12:00', NULL),
(41, 47, 2, 'Erro inutilizar NFCe 1532', '2017-02-13', NULL, NULL, 'Localizar caminho NFCe e coloca-la na pasta NFCe', '2', '00:12:00', NULL),
(42, 53, 2, 'Sistema nÃ£o estÃ¡ calculando valor total do produto', '2017-02-14', NULL, NULL, 'Sisauto atualizado, e feita uma atualizaÃ§Ã£o de estoque', '2', '00:12:00', NULL),
(43, 53, 2, 'Mensagem de erro ao clicar em clientes no sisauto', '2017-02-14', NULL, NULL, 'executavel atualizado', '2', '00:12:00', NULL),
(44, 54, 2, 'Mensagem de erro ao clicar em clientes no sisauto', '2017-02-14', NULL, NULL, 'Executavel sisauto atualizado', '2', '00:12:00', NULL),
(45, 54, 2, 'Erro na mensagem do imposto do cupom', '2017-02-14', NULL, NULL, 'executÃ¡vel sisauto atualizado', '2', '00:12:00', NULL),
(46, 55, 2, 'Solicitou que mudasse o csosn de todos os produtos para 103', '2017-02-14', NULL, NULL, 'valores do csosn atualizados', '2', '00:12:00', NULL),
(47, 56, 2, 'Instalar certificado digital', '2017-02-14', NULL, NULL, 'Abrir certificado > avanÃ§ar > marca as duas ultimas opcÃµes > avanÃ§ar > concluir', '2', '00:12:00', NULL),
(48, 23, 2, 'NF-e denegada', '2017-02-15', NULL, NULL, 'destinatÃ¡rio da nfe com problemas fiscais, orientado a pesquisar no sintegra', '2', '00:12:00', NULL),
(49, 40, 2, 'NÃ£o envia emails de NFe', '2017-02-15', NULL, NULL, 'acbrmonitorplus nÃ£o estava em execuÃ§Ã£o, ao ligar enviou os emails', '2', '00:12:00', NULL),
(50, 57, 2, 'Erro ao gerar NFCe', '2017-02-15', NULL, NULL, 'XML copiado para 20170215', '2', '00:12:00', NULL),
(51, 50, 2, 'Erro ao conectar a conexÃ£o remota', '2017-02-16', NULL, NULL, 'em iniciar > localize a conexÃ£o remota > fixe ela na barra de tarefas > agora na na barra de tarefas de um clique contrÃ¡rio na conexÃ£o remota > outro clique nela > propriedades > avanÃ§adas executar como adm > aplicar ', '2', '00:12:00', NULL),
(52, 58, 2, 'NÃ£o funciona pen drive, corrompido', '2017-02-16', NULL, NULL, 'Pen drive formatado, criadas basetertulia e basemarka novamente no pen drive E', '2', '00:12:00', NULL),
(54, 58, 2, 'nÃ£o consegue tirar relatiorio balanÃ§o', '2017-02-17', NULL, NULL, 'Orientado sobre relatÃ³rio de balanÃ§o de estoque sisauto', '2', '00:12:00', NULL),
(55, 60, 2, 'Qualquer relatÃ³rio por falta de arquivo', '2017-02-17', NULL, NULL, 'Mandar pasta relatÃ³rio sisauto > base1. OBS: tirar uma cÃ³pia da base1 antes', '2', '00:12:00', NULL),
(56, 61, 2, 'Erro to unabled open ', '2017-02-17', NULL, NULL, 'Pasta configurada > Compartilhamento > Marca a opÃ§Ã£o permitir > aplica > ok', '2', '00:12:00', NULL),
(57, 62, 2, 'NFes rejeitadas', '2017-02-20', NULL, NULL, 'Fazer download dos xml pelo navegador explorer > Localizar a pasta unimake/unife/cnpj....../na pasta autorizados verifique o mÃªs e coloque as xmls sempre com uma cÃ³pia ( PROC E NFEPROC) > volte ao gerenciador e consulte a situaÃ§Ã£o das notas', '2', '00:12:00', NULL),
(59, 38, 2, 'Erro ao cancelar NFE', '2017-02-21', NULL, NULL, '', '2', '00:12:00', NULL),
(60, 67, 2, 'Base de dados corrompida sisauto 2016', '2017-02-21', NULL, NULL, 'Recolhido base de dados e feito reparacao localmente\r\nmicro do cliente, esta com office atual e nao abre banco de dados\r\ncom versao anterior\r\ntestado sisauto ok', '2', '00:15:00', NULL),
(61, 63, 2, 'erro ao escolher paÃ­s', '2017-02-21', NULL, NULL, 'Erro corrigido atualizando sisauto', '2', '00:20:00', NULL),
(63, 68, 2, 'Erro Not Found ao abrir Sisauto LB', '2017-02-21', NULL, NULL, 'Verificado caminho do atalho, estava chamando de c:sisautolbat_est.exe, executavel sisauto.exe foi deletado e nao encontrado na pasta\r\nusado exe de outra empresa, todos estao na mesma versao\r\ncorrigido atalho para iniciar em  c:sisautolbsisauto.exe\r\ntestado esta ok', '2', '00:15:00', NULL),
(66, 69, 2, 'Instalar Genus', '2017-02-24', NULL, 'Team Viewer\r\n556407781\r\n2086', '- Instalado Genus + dados emissor gratuito\r\n- Emitido NFe de teste e cancelado em seguida\r\n- Configurado logo empresa e nfe, email das notas enviando ok', '2', '00:10:00', NULL),
(67, 70, 2, 'nÃ£o imprime NFC-e', '2017-02-22', NULL, NULL, 'acbrmonitor executado', '2', '00:12:00', NULL),
(69, 76, 2, 'Baixar xml receita nfe 3093 ', '2017-02-22', NULL, '', 'baixado xmls', '2', '00:12:00', NULL),
(70, 71, 2, 'NÃ£o baixa estoque Genus/Pjcheckout', '2017-02-22', NULL, '486731874\r\nt5qu27\r\n', 'Eduardo verificou codigo e corrigiu problema exe integrador\r\npassado novo exe para personalize testado as baixas esta ok, \r\nconversado com herivelton, ira verificar as baixas dos itens, caso tenha algum problema\r\nentra em contato ', '2', '00:30:00', '4dbcc4749befaf78e530dbe1a18fb2d5'),
(72, 72, 2, 'Nao altera vendedor pjcheckout', '2017-02-22', NULL, 'Team viewer 906 275 596\r\npass 123456', 'enviado novo executavel, vendedor ok', '2', '00:15:00', NULL),
(73, 74, 2, 'erro data e hora horario de verao nfce', '2017-02-23', NULL, '486760421\r\nc75jj3\r\nmanias\r\najustar retorno horario de verao - fuso horario 03h', 'Fuso horÃ¡rio ajustado, Hora ajustada ...', '2', '00:15:00', NULL),
(74, 39, 2, 'molasul- erro nfe', '2017-02-23', NULL, 'teamviewer\r\n816551158   \r\npass 2702', 'erro item not found this collection\r\nfaltando campos \r\nenviado novo bdupdate2\r\ntestado notas ok', '2', '00:30:00', NULL),
(76, 63, 2, 'erro micro ponto', '2017-02-23', NULL, '134893807  5k93yr  - erro ao inicializar e antivirus avast desabilitado', 'Registro da licenÃ§a de um ano gratuita do avast, backups antigos deletados para liberar espaÃ§o no HD', '2', '00:25:00', NULL),
(77, 51, 2, 'Erro ao iniciar skype', '2017-02-23', NULL, 'ID: 423906589', 'Skype reinstalado', '2', '00:15:00', NULL),
(80, 64, 2, 'Configurar boleto sicoob', '2017-02-24', NULL, 'Team viewer FIXO\r\n134 617 345\r\n123456', 'Gerando boletos acbr ok\r\nenviado arquivos para analise do banco \r\naguardar retorno banco\r\navisado cliente sobre situacao OK', '2', '00:40:00', NULL),
(81, 73, 2, 'conferir backup', '2017-02-23', NULL, 'molasul\r\n321308125\r\n31sxm6\r\nconferir backup\r\n', 'Conferido ok', '2', '00:15:00', NULL),
(84, 73, 2, 'nao envia nfe', '2017-02-24', NULL, '560426596  bn1d74', 'Acbrmonitor reiniciado, rodar configuracoes de email e salva....', '2', '00:15:00', NULL),
(85, 45, 2, 'pedido nao edita', '2017-02-24', NULL, '606558959   4613\r\ntrazido base itaiop.\r\n606558959\r\n8859', 'recolhido base, configurando permissÃµes', '2', '00:20:00', NULL),
(86, 75, 2, 'Instalar impressora HP D110', '2017-02-24', NULL, 'Team viewer\r\n342  784 962\r\n123456', '- Baixado e instalado driver da impressora\r\n- compartilhado impressora\r\n- testes impressao e scan ok', '2', '00:20:00', NULL),
(87, 46, 2, 'campo tipopr em relatorio-adicionar', '2017-02-24', NULL, 'codigo prod - nome - ncm - custo unit - vlr unit venda - tipo prod \r\nenviar reltp_2.rpt  - ideale\r\nadicionado em novo rel.-prod.-prod-modelo normal-opcao 2 unidades\r\n774983531  5cct94             719651210 85jxc3', 'Arquivo reltp_2.rpt enviado para servidor e estacao', '2', '00:15:00', NULL),
(88, 39, 2, 'erro nota', '2017-02-24', NULL, '816551158   9655  - trazer base', 'Recolhido base de dados', '2', '00:15:00', NULL),
(89, 77, 2, 'erro tipo subst 060', '2017-02-24', NULL, 'new cosmeticos   706820262  123456   - trazer base de dados', 'substiruicao CST alterado para 060', '2', '00:15:00', NULL),
(90, 78, 2, 'Nao enviar email cte erro authentication required', '2017-02-24', NULL, 'TeamViewer FIXO\r\n100347527  \r\n123456', 'trocado conta configurada no ACBR = nfe.naoresponder@gmail.com\r\ncliente ligou novamente em seguida que nao estava enviando email de novo\r\n\r\nverificado motivo acbr fechado, apenas iniciado ok', '2', '00:20:00', NULL),
(91, 19, 2, 'Alterar NOTICIAS DASHBOARD', '2017-02-28', NULL, '', 'ALTERAR NOTICIAS DO G1 PARA\r\nNOTICIAS CONTABEIS -> LER RSS\r\nhttp://www.contabeis.com.br/rss/noticias/\r\nALTERADO OK', '2', '00:12:00', NULL),
(92, 79, 2, 'acesso wts mslicensing', '2017-02-28', NULL, '218730361  866gqr', 'Licenca servidor executada, licenca permitida acesso concluido', '2', '00:05:00', NULL),
(93, 80, 2, 'nao acessa programas', '2017-02-28', NULL, '866423415  78njk7', 'Processos finalizados e antivirus desabilitado para nÃ£o sobrecarregar\r\no sistema', '2', '00:15:00', NULL),
(94, 1, 2, 'lentidao ao emitir nota', '2017-02-28', NULL, '167565444  7u26cv - providencia mat. construcao (franÃ§a)', '', '2', '00:12:00', NULL),
(95, 82, 2, 'erro uninfe arquivo em uso', '2017-02-28', NULL, 'Team viewer FIXO\r\n609617567 \r\n123456  \r\n -  verificar tambem envio de email da nfe ', 'Reiniciado uninfe, porem ao abrir mensagens de dll faltando\r\nreinstalado uninfe e configurado certificado digital que nao estava selecionado\r\nconfigurado conta nfenaoresponde@gmail.com\r\nno acbr \r\nnotas e emails funcionando ok', '2', '00:30:00', NULL),
(96, 56, 2, 'Erro ao enfiar NFe 240', '2017-02-28', NULL, '', 'recolhimento de base de dados, uninfe, acbr para emissao da nota em outro micro e recomendado formatacao do micro por causa de um virus que esta criando certificados falsos.', '2', '00:30:00', NULL),
(97, 33, 2, 'erro nfe 369', '2017-02-28', NULL, '978591906  ushn48  nf 369', 'IE do cliente nao vinculada ao CNPJ, recomendado rever dados do cliente', '2', '00:05:00', NULL),
(98, 83, 2, 'Arquivos XML NFE/NFCE 201702', '2017-03-01', NULL, 'Team Viewer [FIXO]\r\n378808257\r\n123456', 'Recolhido base e xmls', '2', '00:30:00', NULL),
(99, 84, 2, 'Arquivos XML NFE/NFCE 201702', '2017-03-01', NULL, 'Team Viewer [FIXO]\r\n824107951\r\n123456', 'recolhido base e xmls', '2', '00:30:00', NULL),
(100, 63, 2, 'erro nfe cliente shirleno interestadual isento de icms', '2017-02-28', NULL, 'metalplas\r\ncsosn\r\ncliente isento interestadual pra ela ficou \r\ncsosn 5101 sem st  6101 cfop\r\ncsosn 5201 com st  6401 cfop\r\n18979579000141\r\nnf 5738\r\nshirleno -  inserido manualmente passou ok', '', '2', '00:12:00', NULL),
(101, 62, 2, 'erro nfe -verificar', '2017-03-01', NULL, '434959492 4ms52j', 'Cliente estava com IE como isento em minusculo foi alterado para maiusculo ISENTO, deu certo', '2', '00:15:00', NULL),
(102, 80, 2, 'Laserjet mfp1132 hp', '2017-03-01', NULL, 'Team Viewer [FIXO]\r\n667123545   \r\n123456', 'Instalado driver da impressora, ao ligar impressora\r\nabriu pasta com drivers, usado padrao mesmo\r\npaginas testes OK\r\n', '2', '00:25:00', NULL),
(103, 58, 2, 'instalar certificado digital', '2017-03-01', NULL, 'tertulia\r\n742159931\r\n123456\r\ninstalar certificado digital\r\ngerar ultima nota dele que esta presa na tela\r\n', 'certificado instalado', '2', '00:20:00', NULL),
(104, 85, 2, 'transmitir nfce', '2017-03-01', NULL, '591633938\r\n2n8vk1\r\notica marli\r\ntransmitir nfce e nfe \r\ncopiar pro pen drive dele\r\n', 'nfce e nfe copiadas para pen drive', '2', '00:25:00', NULL),
(105, 80, 2, 'Configurar impressora em rede', '2017-03-01', NULL, 'Team Viewer [FIXO]\r\n468200389\r\n123456 outro ', 'Compartilhado e mapeado impressora na rede\r\nmaquina imac-pc\r\ndefinido como padrÃ£o', '2', '00:15:00', NULL),
(106, 80, 2, 'Certificado digital enviar imac', '2017-03-01', NULL, '468200389  8fd91g', 'Exportado certificado na area de trabalho .pfx\r\nsobre impressoes, servidor desligado apenas pedido para ligar e imprimir novamente', '2', '00:15:00', NULL),
(107, 86, 2, 'envio nfce e nfe', '2017-03-01', NULL, 'mercearia toso ou tozo - TOSO\r\n730272485\r\n17k5qd\r\nnfce e nfe \r\ndeixa rodando rotina e salva na area de trabalho\r\n', 'NFce salvo na area de trabalho', '2', '00:15:00', NULL),
(108, 75, 2, 'trazer base sisauto', '2017-03-01', NULL, '861450362  h3gq64   -  ', 'Recolhido base de dados', '2', '00:15:00', NULL),
(109, 86, 2, 'NFce 7017 pulou numeracao no relatorio', '2017-03-01', NULL, 'Team Viewer [FIXO]\r\n730272485 \r\n123456  ', 'Instalado aplicativo RecuperarXML\r\nbaixado xml atraves da chave, repassado no sisauto\r\nsalvo arquivos e relatorio novamente, deixado arquivo\r\ncompactado na area de trabalho ', '2', '00:35:00', NULL),
(110, 87, 2, 'Arquivos XML NFce 201702', '2017-03-09', NULL, 'Team Vier [FIXO]\r\n847593436 \r\n123456 ', 'Gerado arquivos xml e relatorio mensal\r\nenviado para contabilidade ok', '2', '00:15:00', '1dc53b004025e4cf44adbc526fd05554'),
(111, 72, 2, 'atualizar sisauto e bdupdate2 - sped', '2017-03-01', NULL, '906 275 596\r\nk87si2\r\n', 'Atualizado em todas as estacoes', '2', '00:30:00', NULL),
(112, 42, 2, 'Instalar java - site sicoob nao entra', '2017-03-01', NULL, '996834615  qqq318', 'Instalacao JAVA Windows 8 OK', '2', '00:25:00', NULL),
(113, 1, 2, 'acesso nfe', '2017-03-01', NULL, '667123545  119arg', '', '2', '00:12:00', NULL),
(115, 80, 2, 'Nao imprime e desligamentos apos um tempo', '2017-03-02', NULL, '', 'micro configurado para nunca de desligar em opcoes de energia>equilibrado / Ao clicar para imprimir foi selecionada a impressora correta', '2', '00:15:00', NULL),
(116, 88, 2, 'Arquivos XML NFce 201702', '2017-03-02', NULL, '145564424 \r\n123456', 'Executado rotina enviar nfce por periodo\r\ncorrigido pulos de numeracao manualmente\r\ngerado arquivos xml e relatorio, enviado para contabilidade OK', '2', '00:20:00', NULL),
(117, 81, 2, 'REPARAR BASE DO SISAUTO PROVIDENCIA LENTIDAO AO IMPRIMIR NOTA FISCAL', '2017-03-02', NULL, 'PROVIDENCIA\r\n167565444\r\n89K1KK\r\nREPARAR BASE DO SISAUTO PROVIDENCIA\r\nLENTIDAO AO IMPRIMIR NOTA FISCAL\r\n', 'executado reparador em sisauto providencia', '2', '00:15:00', NULL),
(118, 69, 2, 'Treinamento Genus NFe', '2017-03-02', NULL, '', 'Realizado treinamento passo a passado\r\ncadastro de clientes, produtos e emissao nfe\r\nnota finalizada ok, qualquer duvida vai ligando', '2', '00:35:00', NULL),
(120, 63, 2, 'Notebook sem Antivirus', '2017-03-02', NULL, '', 'Baixado antivirus Avast e instalado\r\ntestado OK, liberado maquina pra usar ', '2', '00:40:00', NULL),
(122, 94, 2, 'erro emitir cte', '2017-03-02', NULL, 'erro cfop invalido para a operacao ao emitir cte ', '-  observar os campos de local de inicio e termino da prestacao -\r\n estava mafra e mafra - deve ser o inicio e termino do transporte - cfop 6352', '2', '00:15:00', NULL),
(124, 89, 4, 'SERPOL MADEIREIRA', '2017-03-02', '2017-05-24', 'SERPOL MADEIREIRA erro nfe', '', '2', '00:15:00', '7c8500637755fd730c92d0da6fb4c748'),
(125, 77, 2, 'Reinstalar Sistema, queimou HD', '2017-03-02', NULL, 'Team Viewer\r\n681808616 \r\ns 5590', 'Reinstalado gernfe, acbr, uninfe, unidanfe, acbr\r\ndeixado rodando cupom fiscal na maquina\r\npendente cliente verificar sobre certificado digital perdido\r\npara liberar as notas fisicas\r\ne configurar backup', '2', '00:40:00', NULL),
(126, 1, 2, 'mapear rede erro de acesso sisauto', '2017-03-02', NULL, '296379473   xuz847', 'Trazer micro para formatacao', '2', '00:15:00', NULL),
(127, 72, 2, 'gerar sped', '2017-03-02', NULL, '906 275 596\r\n677uft   atualizar sisauto.exe e gerar', 'Sped gerado , executavel sisauto atualizado', '2', '00:15:00', NULL),
(128, 90, 2, 'kel cosmeticos - cadastrar - trazer base de dados', '2017-03-02', NULL, '498327714 yt8r83  - trazer base de dados e trazer acbrmonitorplus mes 201702 e uninfe 201702 ', 'recolhidos', '2', '00:20:00', NULL),
(129, 57, 2, 'Envio email nfe e carta correcao', '2017-03-02', NULL, 'Team Viewer [FIXO]\r\n207657519   \r\ns 123456 ', 'Enviado novo exe,  verificado email, configurado conta gmail no acbr', '2', '00:15:00', 'ba94cb6bbf1331a8245b15dceb23121d'),
(130, 1, 2, 'cadastro clientes sisauto', '2017-03-02', NULL, '', 'tem um campo novo no cadastro de clientes do sisauto\r\nna aba\r\ninf. comerciais\r\n\r\ncampo pra por valor do arrendamento e dia de vencimento\r\ne mais um campo email cobranca na aba inicial pra por o email de quem recebera o boleto por email\r\nSE tiver algum caso que CLIENTE FOR RECEBER POR EMAIL E CORREIO COLOCAR A PALAVRA CORREIO \r\nno inicio do campo OBSERVACAO do cliente\r\n\r\ne usar o campo data nasc. pra preencher com o vencimento do certificado digital\r\nde quem fazemos certificado\r\n\r\nquem nao fazemos certificado deixar data de nasc em branco\r\n\r\n', '2', '00:12:00', NULL),
(131, 93, 2, 'remover team da inicializacao', '2017-03-02', NULL, '552675887  8123   ', 'desativado nos servicos para manual ao inves de automatico\r\ncadastrar cliente teoaldo schoereder', '2', '00:15:00', NULL),
(132, 91, 2, 'Erro ao atualizar Genus', '2017-03-03', NULL, 'Team Viewer [FIXO]\r\n279499520\r\ns 123456', 'Voltado executavel anterior, feito atualizacao ok\r\nverificado outro problema, ao inutilizar nfce retorna msg de numeracao ja utilizada, verificado chaves da 1745 e 1750\r\nxmls estao salvos na pasta, numeracoes estao oK para passar para contabilidade', '2', '00:25:00', NULL),
(133, 51, 2, 'Erro ao abrir Skype', '2017-03-03', NULL, '423 906 589\r\n123456', 'Cliente vai ligar para trazer a maquina', '2', '00:20:00', NULL),
(134, 63, 2, 'Nota nao vai p emissor', '2017-03-03', NULL, '540 678 683\r\ntvc192', 'uninfe executado', '2', '00:15:00', NULL),
(135, 92, 2, 'spolti transportes - cancelar manifesto', '2017-03-03', NULL, 'spolti transportes\r\nid\r\n515640101\r\n4043\r\nmanifesto 790\r\nnao aceita cancelar\r\nRejeiÃ§Ã£o: CirculaÃ§Ã£o do MDF-e verificada\r\nacessar pra consultar mdf-e no site da sefaz -sc\r\n', 'cte consultado', '2', '00:20:00', NULL),
(136, 90, 2, 'Arquivos xml nfce 201702', '2017-03-03', NULL, '', 'Executado rotina enviar nfce por periodo\r\ncorrigido furos de numeracao manualmente\r\nenviado relatorio e arquivos por email', '2', '00:40:00', NULL),
(137, 95, 2, 'Entradas de notas, produtos entrando com preco zerado', '2017-03-03', NULL, 'Team Viewer [FIXO]\r\n734159435', 'Enviado executaveis da public, cancelado notas e refeito notas com problema, com atualizacao resolveu problema das entradas OK', '2', '00:20:00', NULL),
(138, 54, 2, 'caixa 474', '2017-03-03', NULL, '823982798\r\n9cy2m4\r\n\r\n caixa 474\r\ndo dia 17-02\r\nnao apareceu suprimento - verificar\r\n', '', '2', '00:12:00', '3cc93cf77e148c7b6faab1a0c2abe6fd'),
(139, 45, 2, 'conectar estacao na rede', '2017-03-03', NULL, 'servidor 930110282  1x2cw9     estacao  721295552  9233  ', 'Rede mapeada ', '2', '00:15:00', NULL),
(140, 33, 2, 'arquivos contab - enviar pdf e xml nfe', '2017-03-03', NULL, '978591906   717fmy', 'Utilizao pasta no desktop XML e PDF, xml e pdf do mes de fevereiro colocados nessa pasta ', '2', '00:15:00', NULL),
(141, 96, 2, 'instalar certificado digital', '2017-03-03', NULL, '283066091  981xfz   36420228  cadastrar madelenha transportes - edson', 'Certificado instalado e configurado no uninfe', '2', '00:15:00', NULL),
(142, 79, 2, 'Arquivos sped/xml 201702', '2017-03-03', NULL, 'WTS \r\nserver: 187.17.226.158:3389\r\nuser: support\r\npass: a123456@\r\n', 'Gerado arquivos sped perfil a e b\r\ne arquivos xml empresa docelandia enviado por email ok', '2', '00:20:00', NULL),
(143, 58, 2, 'Pen drive', '2017-03-06', NULL, '742159931 123456', 'Criado pasta no disco C com o nome pendrive e sisauto tertulia configurado para esta pasta no config', '2', '00:20:00', NULL),
(144, 72, 2, 'Remoto 8 nÃ£o acessa', '2017-03-06', NULL, '', 'Editado conexao remota, deixado usuario e senha fixo\r\nuser remoto8, pass a@8\r\nconexao com servidor ok', '2', '00:15:00', NULL),
(145, 97, 2, 'ERRO NFE', '2017-03-06', NULL, '291650232  9429', 'NFE rejeitada, faltava clicar em consultar situacao, orientado ao cliente a reemitir a nota', '2', '00:15:00', NULL),
(146, 98, 2, 'MDFe nao autoriza, ha mdfe nao encerrado a mais de 30 dias', '2017-03-06', NULL, '', 'Encerrado manifesto em aberto do mes 12/16\r\ncorrigido percurso do transporte que estava invalido\r\ntransmitido e autorizado ok', '2', '00:15:00', NULL),
(147, 80, 2, 'nfe devolucao ensinar', '2017-03-06', NULL, '667123545  q4u69c   juliana', 'Nota de devolucao realizada com sucesso', '2', '00:20:00', NULL),
(148, 1, 2, 'Configurar estacao na rede', '2017-03-06', NULL, '695033849 w3r68x', '', '2', '00:15:00', NULL),
(149, 1, 2, 'Configurar estacao na rede', '2017-03-06', NULL, '695033849', 'Configurado para chamar sisauto automaticamente', '2', '00:15:00', NULL),
(150, 73, 2, 'pasta pdf-recriar pasta dentro do servidor pra Briani poder chamar de fora do server', '2017-03-06', NULL, '712838244  7647   -', 'Pasta Documentos do remoto, criado atalho na area de trabalho no server e no micro (pasta compartilhada)', '2', '00:15:00', NULL),
(151, 56, 2, 'erro ao emitir nfe', '2017-03-07', NULL, 'madeireira campo da lanca (valmor)\r\n696317285\r\nn766vv\r\n', '', '2', '00:15:00', NULL),
(152, 90, 2, 'Faltando arquivos xml nfce', '2017-03-07', NULL, '', 'Passado arquivos faltantes para contabilidade, os arquivos faltantes da 12290 atÃ© 12316 \r\narquivos foram movidos para xml_naoconsta, provavelmente apos rotina, enviado para contabilidade novamente', '2', '00:15:00', NULL),
(153, 79, 2, 'Contabilidade nao recebeu arquivos da empresa GUILHERME TSUNEMI', '2017-03-07', NULL, '', 'Arquivos foram enviados no dia 03-03\r\nreenviado arquivos novamente conforme pedido ', '2', '00:15:00', NULL),
(154, 35, 2, 'atualizar sisauto e bdupdate2 ', '2017-03-07', NULL, 'aquarela\r\n241664216\r\n58w4by\r\natualizar SISAUTO\r\ne trazer base E CERT DIGITAL\r\nE COPIAR CSC DO ACBR\r\nE TRAZER PASTA 201702\r\nDO ACBR E DO UNINFE\r\n', 'Atualizado sisauto, consultado as nfces que estavam faltando', '2', '00:15:00', NULL),
(155, 80, 2, 'impressao na rede', '2017-03-07', NULL, '468200389  5i8f6a', 'Apenas definido impressora \r\n\\imac-pcmp1132 como padrao\r\nimpressoes rodando OK', '2', '00:15:00', NULL),
(156, 96, 2, 'Java erro manifesto gratuito', '2017-03-07', NULL, 'java 283066091  fone 36420228  ', 'Emissor mdfe nao esta disponivel modo producao\r\nsomente nova versao 3.00 em homologacao\r\natualizacao e versoes do java nao funcionam\r\n\r\nsera migrado tudo para o genus - as duas empresas configuradas\r\nsera feito importacao de clientes sisauto > genus', '2', '00:35:00', NULL),
(157, 1, 2, 'conectar servidor', '2017-03-07', NULL, '695033849  q154zu', '', '2', '00:15:00', NULL),
(158, 1, 2, 'arq contabilidade ', '2017-03-07', NULL, 'tocke modas\r\nsheila\r\narq fev\r\nnota de ajuste nao considerar na saida de produtos\r\nrelatorio - saida - produtos\r\n729562979\r\n8uee64\r\nenviar rel. pra contabilidade\r\ndeixar arquivo na area de trabalho da nfce\r\nconferir primeiro pelo relatorio', 'atualizado sisauto', '2', '00:15:00', NULL),
(159, 100, 2, 'envio email genus', '2017-03-07', NULL, 'recebe.cte@cooperalfa.com.br   (vanir e da sintia) do dia 25-02 ate dia 07-03-17 mandar todos de novo por email -  634693933 2d6sx5        3643 1619 robetti transportes ', 'Email enviados', '2', '00:15:00', NULL),
(160, 99, 2, 'Gerar manifesto', '2017-03-07', NULL, '604913862  d6mu51', 'Encerrado manifesto em aberto, gerado manifesto que cliente precisava ok', '2', '00:15:00', 'e1ad06aa9cc78d81d04f34c5bf8276a0'),
(161, 96, 2, 'Importacao dados sisauto -> genus', '2017-03-07', NULL, '', 'Feito importacao dos clientes do sisauto matriz e filial \r\npara genus matriz e filial\r\niniciado treinamento, cadastros de pessoa, motoristas e veiculos\r\nsera continuado amanha ', '2', '01:20:00', NULL),
(162, 101, 2, 'Mandar calculo 1', '2017-03-08', NULL, '585 013 287  97aa2y', 'enviado para virtual caljuris na pasta c:/caljuris', '2', '00:15:00', NULL),
(163, 100, 2, 'envio email genus', '2017-03-08', NULL, '634693933  8pkc88   o de sintia esta mandando - vanir nao manda', 'SMTP erro to login, Ver no cadastro de empresa se a configuracao de email esta selecionado Genus ERP se nao estiver selecione para solucionar o erro', '2', '00:15:00', NULL),
(164, 102, 2, 'Atualizar sisauto e recriar atalho na area de trabalho', '2017-03-08', NULL, '729562979  v97q4k  ', 'Atualizado e criado atalho no desktop', '2', '00:15:00', NULL),
(165, 19, 2, 'Teste anexo', '2017-03-08', NULL, '', '', '2', '00:15:00', '04eba64dda510ad0303605d6b0f306c2.png'),
(166, 103, 2, 'Checkout em modo desconectado', '2017-03-08', NULL, '', 'Detectado ecf configdes\r\niniciado checkout ok', '2', '00:15:00', '7df10bb7583a8c43aedd21266f1e2ce6'),
(167, 104, 2, 'Erro gerenciador INPUT PAST END OF FILE', '2017-03-08', NULL, '', 'Gerou arquivo txt na pasta \notas em branco \r\ndeletado arquivo, reenviado nota novamente, rejeitou devido cliente estar informado como isento, corrigido IE, exportado nota novamente e autorizada OK', '2', '00:20:00', 'a99f02d8d374d379dc9c9f969cab010f'),
(168, 73, 2, 'conferir backup hd externo cobian e na nuvem google drive', '2017-03-08', NULL, '321308125  t3ff75  conferir backup hd externo cobian e na nuvem google drive', 'Backups ok', '2', '00:15:00', '6db85c96c941d1f566d641a965f85e56'),
(169, 58, 2, 'encerrar manifestos', '2017-03-08', NULL, '495482186   de85r6', 'Manifesto com data de emissao muito antiga, para encerrar deve consultar situacao e apos isso encerrar o manifesto', '2', '00:15:00', '18b0ab85fb6259635ce87d4485fe48a7'),
(170, 96, 2, 'Treinamento Genus MDFe/CTe', '2017-03-08', NULL, '', 'Feito emissoes de CTe e MDFe com cliente, passo a passo \r\ncte e mdfe autorizados, emails do genus enviando ok', '2', '00:40:00', 'd9ca1f04ed0d5ad208ea974a4644852d'),
(171, 105, 2, 'Instalar impressora HP deskjet 3050', '2017-03-08', NULL, 'Team Viewer [FIXO]\r\n695033849\r\ns 123456\r\n', 'Baixado e instalado driver da impressora, definido como padrÃ£o\r\npaginas e documentos impressos ok', '2', '00:25:00', '58b7383f9d657d1505dd517d4dab5bd9'),
(172, 79, 2, 'erro gerenciador', '2017-03-08', NULL, '136927529   9354', 'gernfe atualizado', '2', '00:15:00', 'c7f5ae9fa39264ffc1ba5e3def981b64'),
(173, 48, 2, 'enviar nfce e nfe otica real e popular', '2017-03-09', NULL, '752734465  945atc   trazer para enviar pra contabilidade', 'Recolhidos nfce de janeiro e fevereiro', '2', '00:15:00', '48e0af72aa76b1a2e944c15a1a76e6b5'),
(174, 74, 2, 'envio nfce janeiro e fevereiro', '2017-03-09', NULL, 'contador 3642-2437 adriana - nilton brandt\r\nnanyadri@msn.com\r\n\r\nlucimara\r\nmanias\r\n486760421\r\n2rgr97\r\n\r\nenviar nfce janeiro e fevereiro\r\nensinar cliente a usar cancelamento nfce e tal\r\n', 'Recolhido base, nfce fevereiro e janeiro, atualizado sisauto', '2', '00:30:00', 'b9a036fae42b029ff38ac7d34aa16443'),
(175, 86, 2, 'Verificar backup-> pen drive cheio', '2017-03-09', NULL, '', 'Verificado espaÃ§o do pen drive, cobian backup nao estava compactando arquivos, e ocupando muito espaÃ§o\r\nconfigurado compactaÃ§Ã£o, e recortado backups anteriores a 01/01/17\r\ndeixado na maquina em uma pasta _ANTERIOR\r\ncobian rodando OK', '2', '00:20:00', '2fea0d29f50947bb2b18fcf6aac391ed'),
(176, 107, 2, 'ver xml entrada-erro', '2017-03-09', NULL, '526785818  277ity   ', 'Atualizado sisauto, retransmitida a nota', '2', '00:15:00', 'd13c55402c698fda2902bf37694d519a'),
(177, 53, 2, 'atualizar sisauto e bdupdate2 ', '2017-03-09', NULL, 'ID: 745 848 259\r\n\r\nSENHA: cpj325\r\n\r\n \r\n\r\n ', 'sisauto atualizado, dado entrada na nota com sucesso', '2', '00:15:00', 'a50626ad7325a9017cac2e22e08bfc95'),
(178, 63, 2, 'Outlook nÃ£o abre/nao carrega anexos', '2017-03-09', NULL, 'Team Viewer [FIXO]\r\n540687683\r\np 123456', 'Finalizado processos do outlook estava acumulado mais de 6 processos abertos, iniciado novamente, testado envio/recebimento de emails OK\r\nverificado emails, ao abrir email clicando no anexo carrega OK', '2', '00:15:00', 'a9273e545c29deb1fd96e66eea267add'),
(179, 106, 2, 'conferir inutilizacoes nfe', '2017-03-09', NULL, 'tj acessorios    801306567  5nyt11    nfe 1824 1829 1838 - inutilizar', 'Inutilizadas', '2', '00:15:00', '6bd4f3168d828b0da1a3e57ec48c7996'),
(180, 79, 2, 'envio email nfe', '2017-03-09', NULL, '136927529  5086  136927529\r\n7030\r\n', 'Solucionado', '2', '00:15:00', '354d38b8889b81c03cc3119a2f287167'),
(181, 24, 2, 'Instalar certificado digital', '2017-03-09', NULL, 'Team Viewer [FIXO]\r\n768359496 \r\np 123456', 'Instalado e configurado certificado digital\r\nconsulta nfe e uninfe ok', '2', '00:15:00', '28a2c714a7659de814ae0ef36129dc04'),
(182, 108, 2, 'Danfe nÃ£o mostra data emissao e data saida', '2017-03-09', NULL, '', 'Atualizado acbr e unidanfe, testado verificado danfe gerada pelo sisauto na visualizacao, e danfe enviada pelo acbr ambas ok', '2', '00:20:00', '21a35125fae4731547053010ef59ff52'),
(183, 77, 2, 'instalar certificado digital', '2017-03-10', NULL, '681808616\r\n2umk97\r\nesta na tela em pasta downloads - o primeiro arq.', 'Instalado acbr e configurado, uninfe configurado BKP e copiado pasta autorizados em BKP', '2', '00:15:00', '8120a19facb8f714e74d2b7e55423dc9'),
(184, 96, 2, 'Rejeicao mdfe CÃ³digo do MunicÃ­pio do DestinatÃ¡rio diverge do cadastrado na UF', '2017-03-10', NULL, '', 'Corrigido Municipio, e exportado mdfe novamente, mdfe autorizado ok', '2', '00:15:00', 'a90eb9e6539b0986e29b380091240b7c'),
(185, 53, 2, 'Instalar sisauto e checkout', '2017-03-10', NULL, 'Team Viewer [FIXO]\r\n739884557\r\n7817\r\n', 'Instalado driver x64 mp4000 usando bematool, copiado conteudo da pasta bematool\r\ne colado em checkout, registrado dll necessarias em system32 e syswow64\r\nconfigurado uninfe e acbr inicializacao do sistema, consulta nfe, impressao nfe e envio email ok', '2', '00:35:00', 'd9358bba4fc6348b61fd091e6e2d85af'),
(186, 109, 2, 'Nao entra no sistema, instalar tocken novo', '2017-03-10', NULL, '875 078 287  2556', 'instalado token', '2', '00:15:00', 'e101a3581b9d120754947130ae5a42d2'),
(187, 80, 2, 'erro GERNFE', '2017-03-10', NULL, '667123545  214rnt', 'base corrompido trocado por base de BKP', '2', '00:15:00', '2bd6acc1ff7b6116ccbe5e44f9e31337'),
(188, 110, 2, 'Checkout em modo desconectado', '2017-03-10', NULL, '906997487  96irc1    ', 'rodar o configdes e deixar icone na tela, detectado ecf configdes\r\ndeixado icone na area de trabalho ok\r\n', '2', '00:15:00', 'e8fbcbed547bb0d9858f9870a21840a4'),
(189, 96, 2, 'MDFe autorizou, mas ficou com status assinado e nao imprime', '2017-03-10', NULL, '', 'Consultado situaÃ§Ã£o do mdfe, verificado xml estÃ¡ ok\r\nsituaÃ§Ã£o autorizado, impressao damdfe ok\r\n', '2', '00:15:00', '03253ea7a3aee233c3100325b0665d0a'),
(190, 51, 2, 'Skype nao inicia, maquina que veio para verificar', '2017-03-10', NULL, '', 'verificado motivo, foi feito atualizacao do skype e arquivos corromperam novamente\r\ndesinstalado skype com atualizacao, feito instalacao novamente, e passado novamente\r\npara clicar para nao atualizar skype ao iniciar, skype ficou rodando reiniciado maquina para teste ok', '2', '00:15:00', '9c0cd9a69bba4d0ccec0fb4bc9d1362b'),
(191, 91, 2, 'XML NFCe 201702 faltando pra contabilidade', '2017-03-10', NULL, '', 'Verificado arquivos faltantes, da numeracao 1759 atÃ© 1769\r\nsalvo arquivos faltantes, da 1770 atÃ© 1780, inutilizado numeracoes OK\r\nconforme numeracoes passado por contabilidade', '2', '00:20:00', '84763a288d3660a76d245c96fd3d8f5b'),
(192, 111, 2, 'Arquivos XML NFE/NFCE 201702', '2017-03-10', NULL, '', 'Gerado arquivos xml nfce e nfe 201702\r\nenviado para contabilidade OK, aproveitado e atualizado sisauto servidor e stacao', '2', '00:20:00', '044e151234e0fea4bd26ef5066a59862'),
(193, 54, 2, 'configurar estacao ', '2017-03-10', NULL, 'inove tintas\r\nestacao\r\n742728279\r\n5808\r\n\r\n823982798\r\n5nu86f\r\n', 'ICONE: \r\nNET USE X: /DELETE\r\nNET USE X: NUMERO IPNOME COMPARTILHAMENTO\r\nSISAUTO\r\n\r\nSALVAR COMO EXECUTA_SISAUTO.BAT NA PASTA SISAUTO DA ESTACAO  E CRIAR ATALHO NA AREA DE TRABALHO PRA ELE', '2', '00:15:00', '3a742f03a3d69c8e2f79a3b0587a1bd8'),
(194, 81, 2, 'Msg windows nÃ£o Ã© original, erro ao buscar paf gerenciar estacoes', '2017-03-13', NULL, '', 'Criado atalho CONECTAR CHECKOUT.bat\r\ne usado ativador do windows 7 para remover mensagem\r\ndeletado registro mslicensing usadn o DELETA LICENCA SERVER.bat', '2', '00:30:00', '4e011b5967dee3cad00424af166ec281'),
(195, 112, 2, 'Msg Ecf nao cadastrada ao iniciar checkout', '2017-03-13', NULL, '', 'Verificado problema, usado configdes para detectar impressora, pedido para cliente testar, esta rodando ok agora, qualquer problema entra em contato', '2', '00:15:00', '29d8588dd5825a5be3608cd4d1fe4bbd'),
(196, 75, 2, 'Instalar sisauto estacao, maquina formatada', '2017-03-13', NULL, '', 'Registrado dll necessarias, mensagem nao foi possivel encontrar o modulo especificado\r\nwindows 7 64bits e office 2013 instalado, resolvido problema instalando office 2007\r\nmapeamentos com servidor e caixa ok, testado sisauto ficou rodando ok', '2', '00:30:00', 'f6b24ee47c987d659d2d62fb66e2fe0f'),
(197, 76, 2, 'Msg xml nao encontrado no diretorio especificado', '2017-03-13', NULL, '', 'Verificado nfe autorizada mas sem xml, corrigido xml na pasta do uninfe\r\nrepassado OK, consultado notas rejeitadas, mas que alteraram status devido conexao com internet nao conseguiu repassar, deixado notas e danfes OK\r\n', '2', '00:15:00', 'f48cddd62fd5cbb595d996a11ffb7fd1'),
(198, 47, 2, 'Furo numeracao NFCe 1572-1595', '2017-03-13', NULL, '', 'Verificado numeracao, inutilizado numero 1572, e consultado 1595\r\nambas repassadas no relatorio mensal da nfce, esta ok', '2', '00:15:00', '16ecbe3a1f3460d390bdcb57705220d8'),
(199, 111, 2, 'Enviar inventario sintega contador', '2017-03-13', NULL, '106374804  c5a5y7   trazer arquivo do sped e sintegra - trazer base de dados e gernfe tambem', 'Enviado arquivo para contabilidade por email, copia contador e giuliano', '2', '00:20:00', 'de6a70f72790f2ae83cce3730c5930fb'),
(200, 62, 2, 'Cod cupom', '2017-03-13', NULL, '434 959 492   2j8j9b', 'CÃ³digo do cupom com mais de 14 digitos, codigo editado', '2', '00:15:00', 'eb2566e38f9f9e8be10068455a801e71'),
(201, 114, 2, 'envio email nfe', '2017-03-13', NULL, '705823259  rs49d9   mapele modas ', '', '2', '00:12:00', '8f9d6547a4ca3b5f6b3181bb967b6617'),
(202, 115, 2, 'Instalar certificado digital', '2017-03-13', NULL, 'wilson veiculos -  291650232  1599', 'Configurado certificado na empresa W Stoeberl\r\nreenviado notas validadas e autorizadas ok', '2', '00:15:00', 'ac7f676bca8be8bfca226082f9593a05'),
(203, 80, 2, 'Erro ao gravar nota, fecha tudo', '2017-03-13', NULL, '', 'Verificado nota de devolucao com juliana, foi citado chave sem selecionar documento na aba notas referenciadas, editado nota selecionado documento e chave, gravado nota e autorizada ok, qualquer problema entra em contato', '2', '00:15:00', 'f8567ce4430913e54c91b5abead425eb'),
(204, 80, 2, 'lentidao - verificar', '2017-03-13', NULL, 'executar bdupdate2 modo completo e reparador  -  667123545  219hbg  cobian e google drive tambem - fazer ', 'Backup configurado conta gmail: Bkpimac@gmail.com  jean19sms', '2', '00:15:00', 'b7599861dbf58561b200be2ba64ac8f8'),
(205, 42, 2, 'Nota 3883 a enviar no gerenciador', '2017-03-13', NULL, '996834615  g142kj  ', 'Retorno do sefaz, CLiente definido como ISENTO indevidamente\r\nverificado ie no sintegra, corrigido cliente, enviado nota e autorizada', '2', '00:15:00', '6bc98480c7161a9e1bb1b4e91e6a3dc1'),
(206, 114, 2, 'Nao envia email nfe erro authentication required', '2017-03-13', NULL, '', 'Verificado opcoes da internet - aba avancado, marcado opcao usar ssl 2.0\r\ntestado envio do email ok\r\n', '2', '00:15:00', 'f43d4aafba7041b3585355a027354421'),
(207, 42, 2, 'Erro ao emitir nfe', '2017-03-13', NULL, '750519964  5594   nfes  10821 10820 10819 10818 e 10817  - silvanei ligou\r\n', 'verificado motivo, uninfe fechado, iniciado e reenviado notas pendentes\r\nnfe 10820 e 10819 problema no icms da partilha, nao zeraram campos antes de gravar\r\ncorrigido e todas autorizadas', '2', '00:30:00', '44701f778ce108e88747d3a11d1af2a5'),
(208, 1, 2, 'ERR', '2017-03-13', NULL, '245977037  3816   NF 8971', '', '2', '00:12:00', 'b543f0706f58d8a1cbaa59c02f6f817a'),
(209, 73, 2, 'Erro acbrmonitor ao enviar email', '2017-03-13', NULL, '560426596  4B8K1K      ', 'Verificado erros, varias instancias do acbr em execucao, deixado somente uma instancia\r\nenvio email ok', '2', '00:15:00', '2d311e6c5fc98660735d89db351d454f'),
(210, 114, 2, 'erro bdibge2', '2017-03-13', NULL, '106374804  dvk266  enviar bdibge2', '', '2', '00:12:00', 'da71e30ea3d56129d4f0226fdd7c3b3b'),
(211, 116, 2, 'Conexao rede', '2017-03-14', NULL, '***\r\nsisauto normal - no que tira o cupom fiscal\r\nlideranca tintas (andreia)\r\nprincipal:\r\n495465945\r\n4uh6m6\r\nestacao:\r\n214439889\r\nfrk757\r\n\r\ncorrigir icone pro acesso da estacao\r\n', 'Maquinas em grupos de trabalho diferentes, unificado grupo de trabalho\r\nmapeado unidades de rede, e corrigido atalho wts \r\nacesso servidor e sisauto ok', '2', '00:20:00', '7af33e46f1f664fc8c68f246d7e90dbe'),
(212, 117, 2, 'config sisauto', '2017-03-14', NULL, '(cintia)\r\n435729197\r\n525wwr\r\ntrazer base de dados pra nosso servidor\r\ne rodar rotina pra estorno de titulos ate 31-12-16\r\nutilitarios\r\nexecutar sql\r\nsenha 3642\r\ndelete * from tbrec where emissao<=#12/31/16#\r\n\r\npedir pra cliente testar ver se deu certo', 'base recolhida sql executado', '2', '00:20:00', '35b07652b016717bccbf7426a873825a'),
(213, 118, 2, 'erro ao emitir nfe', '2017-03-14', NULL, '532371741   331vgw', 'Cliente cadastrado como isento porem tinha IE', '2', '00:15:00', '4b4099db36d1c2b0ef30d0411c4a6a90'),
(214, 49, 2, 'Arq. nfce deixar no desktop', '2017-03-14', NULL, '184871176  6010', 'recolhido acbr e base e os nfce colocados em pasta no desktop', '2', '00:15:00', '938c4f610ae51c4d4020188340207d8e'),
(215, 115, 2, 'Instalar certificado digital', '2017-03-14', NULL, '200870371  zw3z33     corrigir matriz e filial', 'Instalado certificado digital na maquina da loja matriz, consulta uninfe e nfe ok', '2', '00:15:00', '16fc0625c7634c2540d1e2d03bd563b4'),
(216, 114, 2, 'atualizar sisauto e bdupdate2 ', '2017-03-14', NULL, '**\r\nmapele \r\ntirar copia do sisauto.exe anterior\r\nenviar novo exe\r\nsisauto e bdupdate2\r\n705823259\r\n739tjk\r\ncorrigir questao entradas - pc\r\n', 'Sisauto atualizado', '2', '00:15:00', '23280b2c850903a6fb4b99f52751a33d'),
(217, 96, 2, 'erro site', '2017-03-14', NULL, '283066091  egr767', 'Solucionado', '2', '00:15:00', 'a48c5652aa231cdff13af6c61aad4e81'),
(218, 119, 2, 'erro ao emitir nfe', '2017-03-14', NULL, '288158766  123456   NF 590  montfer', 'Uninfe fechado, iniciado uninfe, nota autorizada', '2', '00:15:00', '33fd4123996bbe4cd128d1c6d8d3c0c1'),
(219, 49, 2, 'Gerar nfce fevereiro', '2017-03-14', NULL, '720460957  krv981   nfce   jair', 'Gerado arquivos xml, salvo na area de trabalho do cliente 201702', '2', '00:15:00', '69242134bbaac0282dcb233a597d0b82'),
(220, 120, 2, 'Erro cancelar nota', '2017-03-14', NULL, '956028371   cwt6u26', 'Nao conseguiu conectar com servidor ao mandar evento de cancelamento\r\nconexao voltou nfe cancelada', '2', '00:15:00', 'f2799d3459883a302cd7f8e4a4fc08f9'),
(221, 111, 2, 'Enviar bdibge2', '2017-03-14', NULL, '106374804  ', 'Enviado arquivo, pedido para cliente testar ok', '2', '00:15:00', '49cd49228007dabd8adb58748ea0285f'),
(222, 92, 2, 'erro genus carta correcao', '2017-03-14', NULL, 'spolti:\r\nmanifesto\r\ncidade de destino\r\nesta mantendo na caixa de listagem a cidade do manifesto anterior\r\n\r\ne no emitir cte\r\nao colocar placa\r\ncoloca a placa que adicionou e adiciona mais uma \r\nref ao cte anterior\r\n\r\nao enviar Ã© que ele acusa na receita\r\n***************\r\nver carta de correcao pra campo vprest comp xnome \r\n****\r\n***************\r\n515640101\r\n3740\r\ncte 1348\r\n', 'Instalado emissor gratuito no servidor, configurado emitente\r\nfeito carta de correcao cte 1348\r\ngrupo Comp, campo xNome, numero do item 2\r\ngerado OK, constando na receita OK', '2', '00:30:00', '13b70a442a11f4371c0329ae5385ad70'),
(223, 35, 2, 'enviar nfce e nfe ', '2017-03-15', NULL, 'ligaram contabilidade que nao receberam o email', 'gerado arquivos e relatorio enviado ok', '2', '00:25:00', 'aaf4e237f021b4d6be7264abd212f0ec'),
(224, 106, 2, 'Notas validadas', '2017-03-15', NULL, '', 'Verificado retorno, data de saida menor , corrigido date enviado nota e autorizada\r\nverificado outra numeracao, reclamou valores da partilha informado difere do calculado\r\nzerado dados, enviado novamente e autorizada', '2', '00:20:00', 'a5ce044e3751f21a636d6498b81ec0af'),
(225, 49, 2, 'desativar nfce do micro', '2017-03-15', NULL, '164871176  3589', 'desativado', '2', '00:15:00', 'e1ea39b455851693e3a4f5dce4966092'),
(226, 1, 2, 'prefeitura quit', '2017-03-15', NULL, 'prefeitura quit  -  financeiro - acao social - -  diones ligou - financeiro e saude sao os icones que esta usando - trocado micro e deu erro ao reinstalar - avisado pra Charles nos ligar pra ver o acerto do ano 2017 do sistema', '', '2', '00:12:00', '975d474c231170b39cd45cca82c237a4'),
(227, 115, 2, 'erro ao abrir sisauto', '2017-03-15', NULL, '200870371  2i2kv9', 'Sisauto atualizado', '2', '00:15:00', '44fbe115effaef695a8631263c7b5812'),
(228, 119, 2, 'Encerrar mdfe', '2017-03-15', NULL, '', 'Msg de duplicidade ao encerrar, consultado mdfe antes depois mandado\r\nevento de encerramento, encerrou OK', '2', '00:15:00', '853bc70c1d29b6140d571f03180fb9bf'),
(229, 121, 2, 'Indices caljuris sumiram', '2017-03-15', NULL, 'Team Viewer [FIXO]\r\n328124421\r\n123456', 'atualizacao indices caljuris ok', '2', '00:12:00', 'a3fde63578a8086d7fc056ba70dd4da5'),
(230, 73, 2, 'Gernfe nao envia email', '2017-03-15', NULL, '', 'Verificado problema, reiniciado acbr e deletado arquivo texto da pasta entrada\r\nreenviado email, voltou a funcionar ok', '2', '00:15:00', 'e116ef6b5cb77848786a025b55ec41c4'),
(231, 94, 2, 'Carta de correcao observacao Cte 1553 e 1554', '2017-03-15', NULL, '', 'Gerado carta de correcao com cliente, verificado grupo COMPL\r\ncampo XOBS, e corrigido texto da observacao, eventos vinculados ok\r\nsalvo xml dos eventos na area de trabalho', '2', '00:15:00', '5d5baaece348d0ddd294817b39ed6de1'),
(232, 121, 2, 'atualizar caljuris', '2017-03-15', NULL, '595475671  fwr293', 'Calculo1.mdb atualizado e executado funcao recebe indice para atualizar caljuris', '2', '00:15:00', '8bd03f8ac76d71fdb061cf55f26cc5e9'),
(233, 110, 2, 'erro windows update', '2017-03-15', NULL, '906997487  481hpn    desabilitar windows update', 'Windows update desabilitado', '2', '00:15:00', 'e1c06c034c640337c2336056613200bf'),
(234, 25, 2, 'Erro enviar NFCe e email', '2017-03-15', NULL, '926360185  1205', 'Sisauto atualizado, acbr configurado com email padrao', '2', '00:15:00', 'b3885e6ca72b66d39da1568ea1a656a8'),
(235, 100, 2, 'envio email genus', '2017-03-15', NULL, 'robetti\r\ncte -  emails - verificar\r\nclientes deles alegando que alguns chegam - outros nao\r\nver se da pra instalar outlook express e configurar genus na emissao do email\r\n634693933\r\na5x58y\r\n', 'Outlook express nao Ã© compativel com xp\r\nverificado cadastro de pessoas, no campo email xml alguns estavam com ,\r\npara envio o correto Ã© ; testado com cliente, cadastrando email deles + o meu + tomador\r\nverificado arquivos recebidos, ambos receberam, sugerido ir cadastrando aos poucos os emails serapado por ; porque as configuracoes do genus estao ok ', '2', '00:15:00', '8f435b93cb374b6e9bb86cf175853257'),
(236, 54, 2, 'instalar impressora', '2017-03-15', NULL, '742728279  4058  estacao -     instalar   epson fx 890  e laserjet m1132    em rede    -  servidor 823982798  ytj474  ', 'Configurado impressora hp e epson na rede, estacao imprimindo documentos em ambas OK', '2', '00:20:00', '2062750ae4f2f11741ee0c22b11a4fb8'),
(237, 33, 2, 'Csosn incompativel com operacao nao contribuinte', '2017-03-15', NULL, '978591906  8wq2e6   nf 445', 'corrigido csosn dos itens da nota, exportado novamente e autorizada', '2', '00:15:00', 'd710ba479005168756b261150cbdb24d'),
(238, 1, 2, 'transferencia genus', '2017-03-15', NULL, 'a.l. transportes\r\ntirar do micro e instalar no micro da universal\r\n826244650  \r\ntrazer dados pra support primeiro\r\ndai irao ligar da universal', 'recolhido dados servidor ok', '2', '00:15:00', 'd1edf5dd2e273ed0f0f174060e7333c1'),
(239, 122, 2, 'pjCheckout em modo desconectado', '2017-03-16', NULL, '', 'Detectado ecf configdes, testado acesso checkout, lx e rz ok', '2', '00:15:00', 'd836e51ea4fee0906495519bb0134399'),
(240, 73, 2, 'envio email acbr', '2017-03-16', NULL, '560426596  6bkv12  - verificar arquivo da pasta c:acbrmonitorplusentrada', 'Arquivo verificado, enviado novo executavel do gernfe2', '2', '00:15:00', '82522361686e8a9a0b4197f4131a9211'),
(241, 64, 2, 'instalar boleto sicoob', '2017-03-16', NULL, '134617345  954jnd   colocar pastas sisauto e jean e basesisauto como excecao no antivirus e  atualizar sisauto e bdupdate2 e mandar bdcob.mdb e config.ini pra pasta c:oletosicoob   (estao no servidor na pasta do autocenter 20170316)  -  depois gerar 5 boletos e depois uma remessa - enviar pro email da support', 'base recolhida, boletos configurados, gerados', '2', '00:15:00', '22013688f59f58ba255fb2678c5c0992'),
(242, 33, 2, 'Retorno NFe, IE do destinatario nao cadastrada', '2017-03-16', NULL, '978591906  16bzk3    nf 456  verificar mensagem ', 'CLiente nao tem IE, e no cadastro estava sendo informado, deixado como isento\r\nreenviado notas e autorizadas', '2', '00:15:00', '32593261aca705baf6e517d18172a96e'),
(243, 72, 2, 'erro acesso ao banco do brasil', '2017-03-16', NULL, '129682347  4735  erro no acesso ao banco do brasil  token  -   navegador firefox-mozilla - tambem nao entrando no sicoob hoje - antivirus norton avisando que expirou - sugestao instalar outro antivirus', 'Modulos seguranca e assinatura digita reinstalados, nao resolveu\r\nsera entrado em contato com banco para verificar problema, talvez seja maquina\r\npois ha varios aplicativos estranhos instalados, qualquer coisa entram em contato', '2', '00:30:00', 'ef2c02e04ae9070c0bf2e1be8baad69e'),
(245, 77, 2, 'Base de calculo icms difere do somatorio dos itens', '2017-03-16', NULL, '', 'Verificado itens com icms, usado csosn 900 para fazer o calculo\r\nreenviado nota autorizada', '2', '00:15:00', '44ab2950ef6419321ea82e9ab9ae90d6');
INSERT INTO `chamados` (`id`, `id_cliente`, `id_usuario`, `problema`, `data`, `data_modif`, `conexao`, `solucao`, `status`, `tempo`, `anexo`) VALUES
(246, 123, 2, 'Instalar Genus', '2017-03-16', NULL, '781571326  8760', '', '2', '00:12:00', '8b50cdbf2de1df8dd696be5837af7c59'),
(247, 72, 2, 'instalar antivirus avast', '2017-03-16', NULL, '129682347   168hrt   remover norton antivirus e instalar avast  do ninite ', 'Avast instalado', '2', '00:20:00', '203f81852374464b518d4109c579fb4a'),
(248, 113, 2, 'Remotos sem permissÃ£o para usuÃ¡rio no Terminal Server', '2017-03-16', NULL, 'WTS \r\n200.53.28.29\r\n\r\nOntem funcionou normalmente, hoje nao conseguiu acesso\r\n', 'Abrimos o \"Group Policy Managemente\" e editar a GPO \"Default Domain Policy\".\r\n\r\ncomputer configuration > policies > Windows settings > security settings > local policy > user rights assignment > \"permitir logar atravÃ©s do serviÃ§o de terminal services\".\r\n\r\nAdicionamos os grupos ou usuÃ¡rios para liberar a conexÃ£o via TS', '2', '00:20:00', '58eb4d0c3dd26242ec789a4ac2f75359'),
(249, 124, 2, 'inst. sisauto cilmar', '2017-03-16', NULL, '780764549  fh654k', 'Configurado sisauto cilmar\r\nBASE2, SISAUTO2, nova instancia do acbrmonitor, gerenciador de notas e uninfe\r\ncsc e certificado ok, consulta servidor ok tambem \r\nqualquer duvida entra em contato', '2', '00:25:00', '2f6c64bc6a33a748bc63fefee144cad2'),
(250, 125, 2, 'erro ao emitir nfe', '2017-03-16', NULL, '139160110   32n9zc     nf 1303', 'Certificado digital vencido, instalado novo certificado e configurado no uninfe\r\nreenviado nota e autorizada\r\n', '2', '00:15:00', '00c894702f778a3933dc8db979b73f04'),
(251, 57, 2, 'erro ao emitir nfce sem numero', '2017-03-16', NULL, 'celmoto\r\nerro nfce\r\n207657519\r\n84c7ig\r\n', 'atualizado sisauto, endereco + numero cliente \r\nemissao nfce ok', '2', '00:30:00', '4091fddfccde15e511b15d68e70bdb5b'),
(252, 72, 2, 'total da base de calculo difere do somatorio dos itens na nfe', '2017-03-17', NULL, '906275596   868wvj   nf 24725', 'Resolvido pela cliente', '2', '00:15:00', '551491a6dddf196f486c14de00530076'),
(253, 73, 2, 'erro ao enviar email acbrmonitorplus', '2017-03-17', NULL, '758353120   2196  servidor', 'ConfiguraÃ§Ã£o acbr sem codificacao utf_8', '2', '00:15:00', 'fbd79ab3df14287ccf8b030811674393'),
(254, 96, 2, 'configurar emails de carlos no genus', '2017-03-17', NULL, '283066091    ctemadelenha@gmail.com  matriz e filial  senha cte2015', 'Configurado conta, testado envio de emails OK', '2', '00:15:00', '18f20214b1532ba5f3425ee3bc60e067'),
(255, 107, 2, 'erro envio boleto como anexo', '2017-03-17', NULL, '526785818      ', 'Configurado visualizador de imagens para arquivo .jpg padrao, pois estavam usando adobe pdf e acusa erro, verificado emaisl enviados pelo imap smtp, estava sendo consultado em outlook ao inves de gmail, esta ok qualquer duvida entra em contato', '2', '00:20:00', '110dee3a8531580c3bb804239b5d8342'),
(256, 74, 2, 'impressao pedido sem pedir forma de pgto', '2017-03-17', NULL, '486760421  zw4q83    ', 'Desabilitado parcelas ao imprimir pedido, verificado impressora que nao imprimia tambem\r\nreiniciando spooler de impressao ok', '2', '00:30:00', '1772cf17b9c4dcaa1d75fc4129c6defc'),
(258, 1, 2, 'erro ao emitir nfe', '2017-03-17', NULL, '534778469  465ihh  vidracaria sao pedro   teste    update tbprod set codncm=39191090 where codncm=39191000', 'Sql executado erro ncm inexistente e bc icms', '2', '00:15:00', '5301111f1b2f2af8e75915d162284d5e'),
(260, 126, 2, 'erro ao emitir nfe', '2017-03-17', NULL, '754331165  e5c83c  - guth maquinas ', 'Verificado, reinstalado uninfe versao 5, ao baixar versao atual sÃ³ erro de conexao com internet, emitido notas presas todas autorizadas ok', '2', '00:25:00', '1cea0fa625f1115c3d70435c0b6b4b9a'),
(261, 70, 2, 'erro ao emitir cte', '2017-03-17', NULL, '643257701  5enc81  numero cte 469', 'CFOP INVALIDO, ALTERADO ok', '2', '00:15:00', '29e0fb07e47d1e4960cb916799d7a1fd'),
(262, 1, 2, 'instalar arquivo morto', '2017-03-17', NULL, '789552378\r\n1047\r\nsistema arquivo morto instalar\r\nfazenda potrich  - mato grosso - Josi\r\n', '...', '2', '00:15:00', 'da4cd15c5ac7a62a9ddafe39c84948c5'),
(264, 127, 2, 'Tela azul e reiniciando', '2017-03-20', NULL, '', 'Desinstalado avast, e instalado antivirus windows defender, micro ligado a um bom tempo nao reiniciou, qualquer problema entra em contato', '2', '00:30:00', '0aa7316b4319bd4e93d02f3d9664c117'),
(265, 99, 2, 'erro manifesto', '2017-03-20', NULL, '604 913 862\r\n141zsq\r\npedro peters\r\nmanifesto 37 e 38 \r\nverificar que estao emitidos', 'alterado situacao do mdfe para 3 \r\nfeito consulta novamete, status alterou para autorizado\r\nem seguida encerrado mdfe, liberado mdfe preso ok', '2', '00:20:00', '8924bece28d8a161ac8b50827efa365e'),
(266, 80, 2, 'Impressora hp 1132 nao imprime na rede', '2017-03-21', NULL, '', 'Maquinas em grupo de trabalho diferente\r\nunificado grupos, compartilhado e configurado impressora novamente\r\nteste impressoes e documentos ok\r\n', '2', '00:20:00', 'ed0a8e8ae1ddad8e72954d899d209264'),
(267, 107, 2, 'MSG CFOP nao cadastrada ao emitir nota fora do estado', '2017-03-21', NULL, '', 'Cadastrado CFOP 6404 para PR em util-fiscal, estava 6405\r\nemitido nfe para PR com cliente, autorizada ok\r\ndesmarcado executar como admin o unidanfe, sempre pede permissao para abri a nfe\r\ntambem ok', '2', '00:15:00', '7298e8a871da00d668a76947e56ee826'),
(269, 104, 2, 'NFes validadas', '2017-03-21', NULL, '', 'Certificado digital vencido, cliente ira verificar e entra em contato para correcao', '2', '00:12:00', '574541bb350b6427a318943065c45d31'),
(270, 100, 2, 'Rejeicao cte', '2017-03-21', NULL, '', 'Verificado retorno do sefaz, digito verificador da chave de acesso informado invalido\r\nconsultado chave da nota na receita, receita retorna que o cnpj informado na chave Ã© invalido tambem, mas era cte do ano de 2016 que estava sendo enviado\r\ntinha varios ctes inutilizados mas que estavam constando na grid para gerar\r\nalterado status deles para inutilizados no mysql, liberado novo cte que precisava ok', '2', '00:25:00', 'aaa8b19ead230c3dbe0e9885c5647003'),
(271, 1, 2, 'emitir nfe ensinar', '2017-03-21', NULL, '679174045   34hxb6   restaurante rionegrense - deixado em modo producao- cliente ira cadastrar produto ainda e cliente', '', '2', '00:12:00', '22b5e612a99695a680e8f3c90bd99f6c'),
(272, 1, 2, 'tornex-genus-nfe devolucao - como gerar', '2017-03-21', NULL, 'marcar a chave referenciada e nf entrada - escolher emissao propria - e no gerador mudar a finalidade pra devolucao no alto - ao gerar a nfe', '', '2', '00:12:00', '547998390e6bbde556b57d3c8936f5b6'),
(273, 95, 2, 'Erro comunicacao com impressora nfce', '2017-03-21', NULL, '', 'Configurado portas com no acbr e da impressora mp4200\r\ntestado impressoes e nfce ok', '2', '00:20:00', 'a86c8c443d688e57619d4dfc20b9948c'),
(274, 116, 2, 'NFe validadas no sisauto deposito', '2017-03-22', NULL, '', 'Verificado retorno, problema nos ncm, atualizado sisauto conforme versao do sisauto loja, corrigido ncm dos itens, exportado nota e autorizada ok', '2', '00:20:00', '52c0b8609cc7af066f78f2bd8d33e7b1'),
(275, 124, 2, 'Erro ao enviar nfce', '2017-03-22', NULL, '', 'Acbr das duas empresas aberto, ocorrendo conflito, explicado como ira funcionar as instancias, quando for usar empresa cilmar, encerrar acbr miram e abrir cilmar e assim vice versa, verificado nota fiscal tambem, configurado layout x03 3.10, enviado nota novamente e autorizada ok, envio de emails tambem ok', '2', '00:25:00', '5cfd8b738413fcf2647af76d94388e49'),
(276, 128, 2, 'Enviar Crystal Reports', '2017-03-22', NULL, '789552378 f3p49z', '', '2', '00:12:00', '01ee5fd74b6f4b9f10efaf967afc0db1'),
(277, 81, 2, 'Instalar impressora', '2017-03-22', NULL, '', 'Baixado e instalado driver impressora hp 1102w \r\npaginas e documentos impressos OK\r\ninstalado estacoes, e estacoes remotas', '2', '00:01:40', 'd176ad024a643cc3714e133bf4837c57'),
(278, 64, 2, 'atualizar cobranca', '2017-03-22', NULL, '134617345\r\nn7r14h\r\n', 'atualizado e gerado remessa para analise', '2', '00:15:00', 'dbec90799f8c7cb2807b3a6f8bf65bd0'),
(279, 32, 2, 'instalar antivirus DEFENDER', '2017-03-22', NULL, '534778469  4TI76q', 'Instalado anti-virus Essentials', '2', '00:15:00', 'e5476591e79261fe067821fa2a28839b'),
(280, 74, 2, 'instalar cobian e google drive', '2017-03-22', NULL, '486760421 6n6we1   instalar cobian e google drive', '', '2', '00:12:00', '99d91f1ef40c4e70814298a6c9385449'),
(281, 110, 2, 'Enviar inventario contabilidade', '2017-03-22', NULL, '', 'Enviado arquivo inventario retroativo para email da contabilidade ok', '2', '00:15:00', '7a6f5442a50f6df816f79cf160b621c6'),
(282, 45, 2, 'Relatorio de estoque minimo, mostrando produtos Inativos', '2017-03-23', NULL, 'Diogo pediu para retornar ligacao para verificar sobre estoque', 'alteracao ok \r\nenviado 31-03-17', '2', '00:12:00', 'e190d658fba7c58a6a5f329f43f7ba12'),
(283, 129, 2, 'pjCheckout em modo desconectado', '2017-03-23', NULL, '', 'desconectado impressora da energia, ligado novamente\r\ndetectado ecf em configdes, testado checkout lx e rz ok', '2', '00:20:00', 'da44673e3f15959883948870bda845a2'),
(284, 69, 2, 'Erro ao validar nfe genus', '2017-03-23', NULL, '', 'Verificado motivo, problema na unidade da embalagem dos produtos em branco\r\ncorrigido cadastros, exportado notas e autorizadas', '2', '00:15:00', '1002eec367dd670cbb410e5cbc516a40'),
(285, 127, 2, 'Nota rejeitada no gernfe', '2017-03-23', NULL, '', 'Valor total do Voutros difere do calculado, valor de 0,01 centavo\r\ncorrigido valor vOutros de 377,77 para 377,78 no arquivo txt\r\nnota autorizada ok\r\n', '2', '00:15:00', 'd847aa69610b1d9ca69e90cc9c119a27'),
(287, 1, 2, 'trazer base sisauto', '2017-03-23', NULL, 'vidr sao pedro\r\n534778469\r\n282fsf\r\ntrazer base de dados\r\n', 'recolhido base de dados para analise', '2', '00:15:00', 'a6050cb33fc9fc468aa393038ec67e6c'),
(288, 58, 2, 'Salvar excel em pdf', '2017-03-23', NULL, '', 'Instalado doro pdf e mandado impressao para doro pdf\r\nsalvo documento pdf na area de trabalho para anexar no email', '2', '00:15:00', 'd3e6ed39761152e74d074f891f05b1c2'),
(289, 96, 2, 'recuperar arquivo deletado', '2017-03-23', NULL, 'madelenha\r\n283066091\r\n123456\r\ncolocar a pasta do genus como excecao\r\ne recuperar o executavel do genus que foi arquivado pelo avg\r\n', 'Os arquivos estavam na quarentena do AVG anti virus, arquivos restaurados e colocado como excessao no antivirus', '2', '00:15:00', 'e025b554e6919136f9c080c0bd2a073a'),
(290, 84, 2, 'atualizar sisauto e bdupdate2 ', '2017-03-23', NULL, 'celi\r\n824107951\r\n123456\r\natualizar sisauto e bdupdate2\r\n', 'Sisauto atualizado ', '2', '00:15:00', 'c8a57f48fa2439f333b748c1efe37d87'),
(291, 79, 2, 'Instalar certificado digital', '2017-03-23', NULL, '', 'Instalado e configurado certificado digital da empresa guilherme\r\nconsulta nfe e uninfe ok', '2', '00:15:00', '44e7033d1af2230de0cc661340fbf830'),
(292, 44, 2, 'trazer base genus', '2017-03-23', NULL, '607 119 546\r\nzs35d9', 'Base genus recolhida', '2', '00:15:00', 'a3197f39192e72bf1d20ae232865d843'),
(293, 45, 2, 'atualizar sisauto e bdupdate2 ', '2017-03-23', NULL, 'rede rural\r\n930110282\r\nba5n66\r\nservidor\r\n', 'Sisauto do servidor e estaÃ§Ãµes atualizados', '2', '00:15:00', 'a931895df4105a6e2986619bd18699d0'),
(294, 130, 2, 'NFe de devolucao validada', '2017-03-23', NULL, '', 'Verificado nfe, nota de devolucao nao tem finalidade de devolucao\r\ncorrigido cfop e citado chave nfe referenciada\r\nenviado novamente e autorizada', '2', '00:15:00', '67a13a9c1ff1163fb99c3aeab22dbd27'),
(295, 64, 2, 'boleto sicoob', '2017-03-24', NULL, '134617345 523jyf  5 boletos de 1,00 gerar boletos e remessa - enviar pra email jean', 'concluido', '2', '00:15:00', 'ec448d01969c94e053df3df03e2ff7de'),
(296, 81, 2, 'Licenca ts expirou', '2017-03-27', NULL, '167 565 444\r\ns 6z2u79', 'Enviado deleta licenca server.bat\r\ndeletado registro, executado conexao ok', '2', '00:15:00', 'bd204fa172fff53bdfcf4304c8905194'),
(297, 82, 2, 'emitir nfe ', '2017-03-27', NULL, 'artepava\r\n609617567\r\n5ehz12\r\nnf 300 e 301\r\nautorizar e trazer por email\r\nenviar pra secretaria@support-br.com.br\r\npra daiana imprimir 2 copias\r\n', 'autorizados e enviados para a secretaria para realizar impressao', '2', '00:15:00', '6a7521aecd337c4ad63fbd9852257273'),
(298, 79, 2, 'Cadastros funcionarios', '2017-03-27', NULL, '', 'Cadastrado novo usuario sisauto, vend meri\r\ndefinido senha e permissoes, deletado alguns usuarios nao utilizados ok\r\n', '2', '00:15:00', 'd70dff8ef392e65b1f5aabcfd3084c21'),
(299, 63, 2, 'numero endereco nfe', '2017-03-27', NULL, '', 'boletos sup\r\nmetalplas erro ao colocar endereco av minas gerais, 1425 A\r\nsaiu um numero estranho ao emitir a nota\r\nver protecao pra nao permitir dessa forma\r\ne nao sair numero estranho pelo menos\r\n(24-03-17)\r\ntestado em 27-03-17 saiu ok\r\n', '2', '00:12:00', '5625abca39d918a323e8a883ad2fd520'),
(300, 73, 2, 'NFe 17935 do mes 12-16 nao mostra no gernfe e nao inutiliza', '2017-03-28', NULL, '', 'Verificado nfe, esta denegada na receita, nao permite inutilizar ou autorizar, enquanto cliente nao resolver pendencias na receita, cliente ira verificar situacao', '2', '00:15:00', '7f44f359bfa35d4dbd1bef7a3ae9f834'),
(301, 63, 2, 'atualizar sisauto e bdupdate2 ', '2017-03-28', NULL, 'metalplas\r\n540678683\r\n123456\r\n', 'Sisauto atualizado', '2', '00:15:00', 'c5f5bc08c1bae144967209d837e4f9a5'),
(302, 131, 2, 'instalar genus micro', '2017-03-28', NULL, '931275074   5342', 'apos instalar - passar pra welisson instalar base da a.l. transportes\r\n-- Importado dados, instalado certificado digital, atualizado schemas, e arquivos xml\r\ntestado consulta de cte, envio de emails e impressao dacte, exe atual OK', '2', '00:20:00', '64dfb48a1300ca9cdc0fdbc487f3cc30'),
(303, 41, 2, 'instalar impressora fiscal mp4200  e sisauto', '2017-03-29', NULL, '864363751  3358', 'Instalado impressora fiscal, configurado checkout e sisauto estacao\r\nregistrado dll necessarias OK', '2', '00:15:00', '86315185570c0ab36aecc76d5fb3ad3a'),
(304, 92, 4, 'carta correcao cte', '2017-03-29', NULL, '515640101    1500   -  carta correcao do cte 64   onde diz FRETE VALOR no campo de 435,00\r\nMUDAR PARA PEDAGIO\r\nenviar imagem da correcao pra email jean ou de fabricio\r\naguardar chave acesso\r\ncte 64\r\n515640101\r\n2797\r\n', 'Gerado carta de correcao, enviado arquivo pdf do evento email support\r\n', '2', '00:15:00', 'e25636a46de73d58ce011e7c3d462d2b'),
(305, 112, 2, 'pjCheckout em modo desconectado', '2017-03-29', NULL, '', 'Detectado ecf configdes, testado checkout vendas ok', '2', '00:15:00', 'c8ca94475c0f702d3601e981afd59597'),
(306, 88, 2, 'Erro imprimir nfce', '2017-03-30', NULL, '', 'Maquinas em redes diferentes, alterado nome do computador MANIASKIDS para \\ESTACAO, removido redes e deixado automaticamente rere Manias\r\nConfigurado novamente impressora 4200, testado impressao de nfce OK\r\nZerado estoque de produtos negativos conforme pedido, e habilitado opcao para nao permitir vender sem saldo em estoque OK', '2', '00:30:00', '44c7ab1813d892279b93bc75c97d0b1a'),
(307, 79, 2, 'Gerenciador de notas nao abre, aviso que ja esta sendo executado', '2017-03-30', NULL, '', 'Ensinado marcio finalizar processo, pelo gerenciador de tarefas\r\nquando precisar ok', '2', '00:15:00', 'c41e4eb6fc588651396e84f3e142e623'),
(308, 103, 2, 'Inconsistencia ao importar cupom', '2017-03-30', NULL, '', 'Verificado itens deletados do sisauto , corrigido cupom e itens pelo banco de dados\r\nimportado novamente todos os registros, todos ok', '2', '00:20:00', 'df8d291fdd0643a640bcb9fbba75122a'),
(309, 35, 2, 'Nota validada do dia 27-03', '2017-03-30', NULL, '', 'Reenviado nfe com data atual 30-03\r\nnota autorizada ok', '2', '00:15:00', '6bc18393baea2c6c6e73af5b2f7b073f'),
(310, 132, 2, 'Enviar copia certificado para contabilidade', '2017-03-30', NULL, '', 'Exportado certificado do cliente, e enviado por email para contabilidade', '2', '00:15:00', '542037a34e2b25e3396044b6069c2eb2'),
(311, 133, 2, 'erro do desconto genus', '2017-03-30', NULL, '175906041\r\nviviane\r\n', 'Corrigido valor desconto no xml, autorizado pelo acbr\r\nrepassado no genus xml OK e danfe', '2', '00:12:00', '6a3b5d6e76133035ff0616376b031890'),
(312, 111, 2, 'rio motoserras', '2017-03-30', NULL, 'emitir nfe complemento 106374804  sg3u93   -   nota original-numero 200  -   emitir 172,55', 'Cadastro produto complemento de icms\r\nzerado valores de quantidade e preco, informado icms st de 172,55 \r\nusado csosn 201, autorizado NFE ok', '2', '00:20:00', 'a835098c35a46a15b4c208c7c47489f1'),
(313, 49, 2, 'GERAR RELATORIO INVENTARIO', '2017-03-31', NULL, 'ATUALIZAR RPTS PRA GERAR O RELATORIO INVENTARIO\r\n\r\ntrazer a base deles e instalar cobian e google drive\r\n720460957  123456', '', '2', '00:12:00', '9c429db7a487c432383972a75e8967be'),
(314, 132, 2, 'erro imprimir orcamento ', '2017-03-31', NULL, 'trazer base de dados sisauto pra examinarmos aqui - jhp\r\norc 51601\r\nerro ao imprimir \r\ntrazer base pra corrigir\r\nteam 567744523  senha 123456', 'enviado nova versÃ£o do sisauto', '2', '00:15:00', 'a11515e14146833a701db36a643fc460'),
(315, 45, 2, 'instalar cobian e google drive', '2017-03-31', NULL, '930110282  123456  ou 671auu\r\n', 'instalados , conta do gmail rede rural enviada no email support@sup...', '2', '00:15:00', '3e5178aee05b32cc9ea6bc863b1fbe9e'),
(316, 100, 2, 'Carta de correcao Cte', '2017-03-31', NULL, 'Corrigir campo chave NFe\r\ngrupo <infNFe> campo <chave> \r\nnova chave', 'Sefaz rejeita evento, informacao nao pode ser corrigido atraves da cce, cliente verificou com contabilidade, nao pode ser mesmo, ira verificar procedimento qualquer problema entra em contato', '2', '00:15:00', 'e91f0ce84944dafb25b33d6a57405346'),
(317, 33, 2, 'criar logomarca word', '2017-03-31', NULL, 'alterar o seguinte: retirar o HB -  mudar de hortobrasil para ortobrasil -  deixar tamanho maior e um fundo amarelo com verde degrade - no rodape negrito e fundo tambem amarelo e verde - fazer uns 3 modelos pra cliente escolher', 'enviado 3 modelos', '2', '00:15:00', '7456574c86230c751647d5d380ef0910'),
(318, 1, 2, 'trazer base sisauto', '2017-03-31', NULL, 'erincar - jaqueline - 675536445  \r\n', 'base recolhida', '2', '00:15:00', 'a9fb5f5974fdf38067b0b2237cedd88b'),
(319, 125, 2, 'erro ao converter nfe erro de conversao', '2017-03-31', NULL, 'usando cfop incorreto 5900 nao aceita no gerenciador - dai da erro de conversao  - usado cfop 5949 deu certo dai', '', '2', '00:12:00', '98676a32b64ff2d70b806491e7c9a29d'),
(320, 134, 2, 'Mostrar nome VENDEDOR impressao pedido', '2017-04-03', NULL, '', 'Alterado rl_pedidos_prod.rpt deles, inserido novo campo VENDEDOR\r\natualizado em todas as estacoes, testado impressao com cliente, saiu vendedor conforme precisava, esta OK\r\nComentado sobre comissoes, mostrado campo % para cada vendedor em cad-funcionarios, e mostrado relatorio de comissoes, ainda estao estudando a questao\r\nassim que tiver algo concreto entram em contato sobre comissao', '2', '00:30:00', 'f2a4d967ede56ae5757f0a79ef63cf51'),
(321, 85, 2, 'nfce conexao', '2017-04-03', NULL, 'otica marli\r\n591633938\r\natualizar sisauto e rpt\r\ngerar nfce mes de marco\r\n', 'Pasta com os XMLs do mÃªs copiadas para o pen drive, sisauto atualizado e rpts faltantes colocados.', '2', '00:15:00', 'b9af869473745497f407b5e61b82dc3c'),
(322, 87, 2, 'conexao nfe', '2017-04-03', NULL, 'sorvetemania\r\n876593436\r\ni6wm75\r\n', 'Mandar XML do mÃªs > acessar genus ERP com ID:g senha:1 > PAF - ECF > NFCE de consumidor > colocar o periodo > verificar se nÃ£o hÃ¡ faltantes > agora no genus NFCE >  menu principal > gerente > digitar id e senha > pesquisar mÃªs > marcar todas > clicar em salvar > criar pasta com ANOMÃŠS NFCE> clicar em ok > Gerar relatÃ³rio pelo genus ERP > na mesma aba de NFCE de consumidor clicar em imprimir > salvar como pdf > colocar na pasta dos XMLs', '2', '00:15:00', '8ae7c45e67feb604fce45c30445e9f28'),
(323, 33, 2, 'criar logomarca word', '2017-04-03', NULL, '', 'enviado modelos via email - conffecao escolhida modelo 2  ok - ', '2', '00:12:00', '7548e8909a6b4b54e06ea4007202b6df'),
(324, 79, 2, 'CONFIG ESTACAO SERVER', '2017-04-03', NULL, '', 'DELETADO MSLICENSING E CONEXAO DAI TEM QUE SER EM MODO ADMINISTRADOR O NOVO ICONE PRA ACIONAMENTO - ', '2', '00:12:00', '8cc0fc5843dc53f94bddbdf12f7cc15d'),
(325, 35, 2, 'NFCEs', '2017-04-04', NULL, 'Team: 241664216 / q69p1b  ', 'Recolhido NFCES e nfes e gerado relatÃ³rio', '2', '00:15:00', 'a308acd855608edf76c5e7860fd47fdd'),
(326, 83, 2, 'backup', '2017-04-04', NULL, '378808257\r\n\r\nmichele\r\nfazer backup\r\ninstalar cobian backup\r\ne google drive\r\n', 'recolhido nfces e nfes de marÃ§o', '2', '00:15:00', 'ff55823e8a6f117e35d3f803f07cc3b5'),
(327, 81, 2, 'trazer dados areal campo do tenente', '2017-04-04', NULL, '167565444\r\nf6pg42\r\n\r\ntrazer certificado digital e dados do sisauto areal', 'Recolhido base e certificado', '2', '00:15:00', 'a748fc1889668e377e2b3c66bb09c05f'),
(328, 123, 2, 'Instalar Genus', '2017-04-05', NULL, 'Team: 781571326\r\n123456', 'Recriado banco de dados e tabelas manualmente, ja que nao estava permitindo desisntalar genus e servico, \r\nconfigurado emissao NFe, testado emitindo nota e cancelando em seguida esta ok\r\ninstalado genus nfce, exes mais atuais', '2', '00:50:00', 'd2bcca08781412dabdca0182f519a660'),
(329, 54, 2, 'Nota devolucao com icms', '2017-04-05', NULL, 'Team Viewer [FIXO]\r\n823982798\r\n123456', 'Gerado nfe, destacado icms, informado st e ipi\r\noutras despesas e inf. adicionais OK', '2', '00:15:00', '60a63315edff783ecbdd0d8abb7d4f79'),
(330, 1, 2, 'reiturbo-config nova estacao wts', '2017-04-05', NULL, 'reiturbo\r\nnova estacao - configurar no wts\r\nestacao\r\n885315099\r\n22tyi9\r\nservidor\r\nvai passar id no bate-papo\r\n', 'configurado estaÃ§Ã£o wts', '2', '00:15:00', '250f3e466317065c2ffe3e28232a9665'),
(331, 127, 2, 'erro windows server conexao sefaz', '2017-04-05', NULL, '163857960   hu34h2\r\n\r\ninstalar gerenciador santana e nt   em uma das estacoes de fora pra emissao da nota que contenha windows 7', 'Servidor receita estabilizou, liberado notas do jeito antigo\r\ncancelado transferencia de dados ', '2', '00:12:00', '82fdf217a512432b1dc228295bca5bc6'),
(332, 88, 2, 'NFCEs', '2017-04-05', NULL, '145564424', 'Recolhido nfces e relatorio geral de marÃ§o', '2', '00:15:00', 'a34c5edc3509149eebcd3b6969383134'),
(333, 54, 2, 'erro http 500 na nfce - ', '2017-04-05', NULL, '', 'certificado digital vencido', '2', '00:12:00', '1204a07f01243730455fed8ea3a5f156'),
(334, 91, 2, 'Grid de produtos nao aparece coluna ESTOQUE', '2017-04-05', NULL, '', 'Copiado pasta Cliente da outra estacao, e configurado na maquina do tiago\r\nfeito testes de consultas dos itens, todas as colunas ok', '2', '00:15:00', '230ac6d70d847f5cd54edda0fd84042c'),
(335, 106, 2, 'Nova em processamento', '2017-04-05', NULL, '', 'Verificado retorno sefaz, IE informada invalida\r\ncorrigido IE do cliente, exportado nota e autorizada OK', '2', '00:15:00', 'f9db612693a9a60462513d12ece37a5b'),
(336, 24, 2, 'Gerenciador nao enviar notas mensal', '2017-04-05', NULL, '', 'Atualizado gernfe2.exe, configurado para usar acbrmonitorplus\r\ntestado envio de xml e pdf mensal pelo gernfe2 esta ok ', '2', '00:15:00', 'cbee428234fcab1f39679fbf5ac5ad9e'),
(337, 80, 2, 'Impressora estacao nao imprime', '2017-04-05', NULL, '', 'Maquinas em grupo de trabalho diferente\r\nunificado grupo de trabalho estacao e servidor\r\nmapeado novamente, testado impressoes ok', '2', '00:15:00', '28fe4e0fa87ac5521247b5812c307356'),
(338, 85, 2, 'Xml 10111 e 10112 nao foi para contador', '2017-04-06', NULL, '', 'Estava na pasta xml_naoconsta, colocado na pasta correta\r\nenviado contador ok', '2', '00:15:00', '4e7d55ed37ed67eb35efece3822a9da0'),
(339, 124, 2, 'Enviar arquivos xml nfe 201703', '2017-04-06', NULL, '', 'Mostrando procedimentos para enviar pelo gerenciador de notas\r\narquivos xml e pdf, feito passo a passo com cliente enviado OK', '2', '00:15:00', '76651351c67b576fa2cc32f0788ea175'),
(340, 90, 2, 'NFCEs e nfes', '2017-04-06', NULL, '880658407 / a65y3l   498327714 4ei56e', 'Recolhido os nfces nfes para envio ao contador', '2', '00:15:00', '8f082b8f694b4ce4f739bd5ec62674a7'),
(341, 55, 2, 'instalar sisauto', '2017-04-06', NULL, '913 138 155   686ejb      -   outro micro onde esta a impressora:   444 827 282     4760  -  instalar sisauto tonho motos no outro micro e testar o f9 no cadastro de produtos', 'Transferencia dos sisauto motos e car para outra maquina\r\nconfigurado acbrs e uninfe, testado consultas produtos velocidade OK\r\nconsulta notas OK, envio de emails impressao danfe OK\r\navisado para cliente poder usar ', '2', '00:30:00', '6743d267423f5e2bc2203b29c51929a0'),
(342, 49, 2, 'separar arquivos nfce e nfe pra envio contador', '2017-04-06', NULL, '720460957   4y52cu   atualizar sisauto e usar nova rotina', 'recolhido NFCES e NFES para enviar ao contador', '2', '00:15:00', '80a9775fe6c85e17ce47545a1e06be1a'),
(343, 135, 2, 'Ver versao paf-ecf', '2017-04-06', NULL, '321184082  evn122   gerar o relatorio identificacao paf\r\ngerar dai o espelho mfd de hoje\r\ntrazer o arquivo txt\r\nse nao estiver na versao 10.1 o paf-ecf atualizar o paf-ecf e sisauto\r\n', 'Atualizado checkout, gerado espelho e recolhido para servidor\r\nverificado versao 10.1 identificacao paf', '2', '00:15:00', 'a60daef2d969e04129d9d14a506ddc9a'),
(344, 33, 2, 'Arquivos xml e pdf', '2017-04-06', NULL, '', 'Salvo arquivos xml e pdf na pasta do cliente na area de trabalho\r\n', '2', '00:15:00', '5e4ddc15b0b5ca63eb6e3043b466874f'),
(345, 73, 2, 'Verificar onde programa SIM salva os videos', '2017-04-06', NULL, '', 'Programa SIM videos das cameras,\r\nverificado diretorio, salva na pasta SIM no disco c, gravacoes\r\nsalva as gravacoes, qualquer problema retorna', '2', '00:15:00', '160e4db40ce695337ca901f201e26275'),
(346, 114, 2, 'conferir backup cobian e na nuvem google drive', '2017-04-06', NULL, '705823259', 'cobian e drive instalados / conta: backupmapelle@gmail.com', '2', '00:15:00', 'cc2b61075d3368d737d69c3ae790c63a'),
(347, 55, 2, 'mapeamento rede', '2017-04-07', NULL, '913138155   \r\nestacao\r\n444827282 servidor', 'mapeado servidor', '2', '00:15:00', '1278401c01d64019d1e0e2ad9175ca11'),
(348, 136, 2, 'AtualizaÃ§Ã£o PreÃ§os', '2017-04-07', NULL, '', 'Importado registros sisauto, deixado transferindo registros para checkout, irao entrar em contato para avisar se finalizou\r\nverificar para consultar precos antigos\r\n', '2', '00:10:00', '8165e44855802b9c250a4c272a8c56ed'),
(349, 50, 2, 'checkout atualizar', '2017-04-07', NULL, 'cofepar\r\n879620731\r\n5726\r\n\r\n\r\nATUALIZAR  PJCHECKOUT e bdupdate3\r\n1-EXECUTAR em CONFIGURACOES-PROGRAMAR FORMA DE PGTO- ira disponibilizar na leitura x a partir do proximo dia\r\n\r\n2-PROGRAMAR no SISAUTO-CADASTRO DE FORMA DE PAGAMENTO:\r\nCARTAO CREDITO\r\nCARTAO DEBITO\r\n(EXATAMENTE NESTE NOME E EM MAIUSCULAS)\r\n\r\n3- ESTARA HABILITADO TAMBEM AUTOMATICAMENTE REDUCAO Z E GERACAO DO RELATORIO PAF-ECF\r\n', 'pjcheckout e bdupdate3 atualizados, configuraÃ§Ãµes realizadas', '2', '00:15:00', 'ebc860e5824ccc6f9ff30cc3a88e0884'),
(350, 33, 2, 'erro canal seguro, virus', '2017-04-07', NULL, '978591906   881zrg', 'Maquina sera trazida para formatacao', '2', '00:12:00', '4c5ef9353f65e96310706a0c033acd76'),
(352, 92, 2, 'encerrar manifestos', '2017-04-07', NULL, '935649896     9096\r\n', 'encerramento pelo gratuito nao funcionou, devido fuso horario\r\ncriado registro manualmente na tabela mdf\r\ninformado dados manualmente, chave de acesso, protocolo..\r\nem seguida encerrado pelo genus, deixado recado para cliente usar o sistema normalmente', '2', '01:35:00', '5658fb51a099999b556ce874e318a3fc'),
(353, 111, 2, 'instalar certificado digital cartao', '2017-04-07', NULL, 'riomotoserras\r\n936560995\r\n4674\r\n\r\nsenha do cartao: 1234\r\nleitora de cartao\r\nmarca pertosmart\r\ncertisign\r\n', 'Baixado driver e controladora, configurado uninfe e acbr\r\nliberado nota presa gernfe, emitido nfce de teste e cancelado OK', '2', '00:40:00', '0b5890c12de5b7d0d38517a6ef973b14'),
(354, 55, 2, 'ver backup rede e servidor', '2017-04-10', NULL, '913138155\r\najustar backup unidade Z:\r\ne o mapeamento de rede mudar o arquivo .bat\r\npra ja vir com user administrador ', 'Conferido backup', '2', '00:15:00', '700d96a508835e88751c99cffc9bc7c0'),
(355, 107, 2, 'atualizar sisauto e bdupdate2 e checkout', '2017-04-10', NULL, '526785818   t5uf66', 'Sisauto atualizado', '2', '00:15:00', 'ae60d0479f1848faf7d0703527abde06'),
(356, 135, 2, 'checkout atualizar', '2017-04-10', NULL, '321184082  265ptp  atualizar checkout e mandar forma pgto cartao credito e cartao debito', 'Checkout atualizado e dados do sisauto transmitidos para checkout', '2', '00:15:00', '4708424bd83cee8027eb36caecb47b10'),
(357, 127, 2, 'erro ao emitir nfe ', '2017-04-10', NULL, '163857960  -  acusando erro gerenciador notas e ao gerar nota 11144 ', ' campo razao social nao estava preenchido - orientado cliente a preencher - orientado tambem sobre reinicializar servidor em caso de erro anterior no gerador acusando de arquivo ja aberto. ', '2', '00:12:00', '523b266622667d9d891e866d7d254792'),
(358, 102, 2, 'Arquivos XMLs', '2017-04-10', NULL, '729562979 / rk13s2', 'Gerado relatorio nfce geral / recolhido nfces 201703', '2', '00:15:00', 'ba17ea125c19ab4d74a1292142890c17'),
(359, 132, 2, 'atualizar sisauto e bdupdate2 ', '2017-04-10', NULL, '567744523    enviar tambem novos rpts da public\r\nelatorios-sisauto', 'sisauto atualizado e enviado novos arquivos.rpt', '2', '00:15:00', 'c4a62747a9a092bb93217032a139d48c'),
(360, 131, 2, 'genus conexao erro mysql', '2017-04-10', NULL, '931875074  9j89tz\r\n', 'Verificado arquivo de configuracao do genus, alterado\r\nhost para localhost, ao inves do ip\r\ntestado genus OK', '2', '00:15:00', 'cca220a1e9e54344f3f40d0854aa28a5'),
(361, 79, 2, 'LicenÃ§a TS expirou CaÃ§ador-PR', '2017-04-10', NULL, '', 'deletado registro mslicensing, deixado atalho para remover na area de trabalho\r\ntestado conexao remota OK', '2', '00:15:00', '6eb9f157f514498fc3d0c5588c2c3719'),
(362, 74, 2, 'configurar email', '2017-04-10', NULL, '836610578  senha 764hfd   criar email   storelulu@gmail.com   para configurar no acbr', 'email configurado STORELULU66.GMAIL.COM senha: SUPPORT@123', '2', '00:15:00', 'e70adb69490739fec79d9ea9c48ab16d'),
(363, 48, 2, 'envio arquivos nfce e nfe', '2017-04-10', NULL, 'enviar por email pra jean - otica real e popular (nfe e nfce)   752734465 wds294', 'recolhido relatorio e nfce, nao foram emitidas nfe esse mes', '2', '00:15:00', '55710a439ff4610058ddcf446d9f4f2a'),
(364, 61, 2, 'Erro ao enviar nota, error code 000', '2017-04-10', NULL, '', 'Apenas reenviado nota fiscal e autorizada, provavel problema com conexao da internet', '2', '00:15:00', '92df658b9cb85386536f9692d7a07643'),
(365, 1, 2, 'el quimica', '2017-04-10', NULL, '750519964  2502  atualizar sisauto nf  10930', '', '2', '00:12:00', 'b37f4c65721a707bc0c73dcc9cad8798'),
(366, 114, 2, 'reinstalar danfe view', '2017-04-11', NULL, '705823259   reinstalar danfe view - testar abrindo xml da pasta downloads', 'orientado ao cliente a usar webdanfe.com.br', '2', '00:15:00', 'cb9c2595432e4d1ccc28adba05bbbba6'),
(367, 81, 2, 'instalar cobian e google drive', '2017-04-11', NULL, 'providencia\r\n167565444\r\n1h39ft\r\nerro no email acbr\r\ne instalar cobian e google drive\r\n', 'email do acbr estava sem marcar a codificaÃ§Ã£o utf_8, criado tarefa para fazer backup das pastas gernfe2 e gernfe,unimake(cnpj..)', '2', '00:15:00', 'b2b78c4fa846c6621ee3ab4cf9e3f5fd'),
(368, 71, 2, 'pjCheckout em modo desconectado', '2017-04-11', NULL, '', 'Pedido para desligar impressora da energia, ligar novamente e segurar botao selecao por 5s, impressora desbloqueada, detectado ecf configdes, testado checkout e cupons OK', '2', '00:15:00', '50afa6d028e782a443206d1de646e10b'),
(369, 63, 2, 'Resposta automatica email david', '2017-04-11', NULL, '', 'Configurado resposta automatica direto no cpanel\r\nna conta david@metalplas.ind.br \r\nconforme texto passado no email OK', '2', '00:15:00', 'f41320e97906734204506acaa26409ca'),
(370, 96, 2, 'instalar cobian e google drive', '2017-04-11', NULL, '283066091\r\nmadelenha\r\n-arquivos da cte\r\ndas duas empresas\r\ndeixar na area de trabalho pra ele mandar pro contador\r\n-retirar icone do sisauto da area de trabalho (so esta usado o genus atualmente)\r\n-instalar cobian e google drive\r\n', 'Instalado cobian e google drive, xml cte colocados em uma pasta na area de trabalho', '2', '00:15:00', '9235415ae574e15f706e36f524077ed7'),
(371, 116, 2, 'Entrada nfe com xml', '2017-04-11', NULL, '', 'Feito entrada com andreia passo a passo, relacionado item com um ja existente na loja\r\ngravado nota, entrada realizada ok', '2', '00:15:00', 'a7e7438ed287717d899a3d3019071e18'),
(372, 53, 2, 'Nota de devolucao com icms e st', '2017-04-11', NULL, '', 'Gerado nota com cliente, preenchido dados conforme passado\r\nbase de calculo icms, aliquota e valor icms, citado valor de icms st em outras despesas\r\ntotal da nota e demais valores fechou ok\r\n', '2', '00:20:00', '4bce9fd7b23b299d0133bc848531f64e'),
(373, 51, 2, 'Cancelar entrada de nota', '2017-04-11', NULL, '', 'Franciele deu entrada de notas no sisauto da empresa errada\r\nfeito cancelamento da nota junto com ela, cancelamento ok\r\nsÃ³ dar entrada na empresa certa ok', '2', '00:15:00', '6ce911e3ff8a0d63da4f5a2afc183cf4'),
(374, 101, 2, 'Problema caljuris', '2017-04-11', NULL, 'Pediram pra retornar ligacao, ver problema juro2', '', '2', '00:12:00', '427f9b81bc2484ba738ff5538113b5ec'),
(375, 42, 2, 'Dados da mercadoria incompleta ao gravar nfe, faltando ncm', '2017-04-11', NULL, '', 'Verificado e corrigido informacoes da mercadoria, e corrigido ncm dos itens\r\nexportado nota fiscal e autorizada', '2', '00:15:00', '17a4fb5a2a7d7530181cca2ec11f3b85'),
(376, 100, 2, 'configurar online cte vanir', '2017-04-11', NULL, 'robetti\r\n960378404\r\n2633\r\nnote pra criar o link do sistema online\r\ncnpj vanir\r\n\r\n634693933\r\nyy9k52\r\nmicro que tem o certificado digital atual\r\n\r\ncnpj vanir em modo homologacao no online\r\n\r\ndeixar icone pra ele acessar e mexer no sistema dai\r\n\r\n', 'Configurado empresa no sistema web, deixado modo homolagacao\r\nconfigurado dados e certificado digital. deixado atalho na area de trabalho\r\ncomo SISAUTO WEB', '2', '00:20:00', '7ab02c8b99a1241cb58ff9ad5cd079f6'),
(377, 64, 2, 'checkout atualizar', '2017-04-11', NULL, '134617345    idz911', 'checkout atualizado e sisauto tambÃ©m, teste de transmissÃ£o de dados entre sisauto e checkout feito com sucesso', '2', '00:15:00', 'a4dbd5602eb0a347893154d4f6294a7c'),
(378, 137, 2, 'Recolher base dados', '2017-04-11', NULL, 'dipafer\r\ntrazer base de dados\r\n491343676\r\n3782\r\nele ja ia deixar separado', 'Recolhido para servidor', '2', '00:30:00', 'f9ac1ecc848603316a50978dcecbcfd7'),
(379, 127, 2, 'erro acesso gerenciador nfe nt', '2017-04-12', NULL, '163857960  - mensagem gerenciador de ja estar sendo executado', 'efetuado 2 cliques no gerenciador - acionou normal', '2', '00:12:00', 'e467ddee4c565c0fee7341c6239bb1ba'),
(380, 81, 2, 'checkout atualizar', '2017-04-12', NULL, 'providencia\r\natualizar checkout\r\n171318115\r\n7dfs79\r\n', 'Checkout atualizado', '2', '00:15:00', '53d26f62ba4eacd63a76ac79ae09bcd8'),
(381, 91, 2, 'Grid produtos nao mostra coluna estoque', '2017-04-12', NULL, '', 'Copiado pasta Cliente da outra estacao, configurado na maquina do tiago\r\ntestado consultas produtos, coluna estoque OK', '2', '00:15:00', '79c1637c6c5414a0bd7a25c6707d9dcd'),
(382, 138, 2, 'Nota importacao, CNPJ do destinatario invalido', '2017-04-12', NULL, '', '', '2', '00:12:00', '4d8348c09cd252c121e5cb41feb14caf'),
(383, 1, 2, 'ACRIMASTEY', '2017-04-12', NULL, '288158766 / 346zzf', 'Certificado importado com sucesso', '2', '00:15:00', '1214072dee7874cefa2adfae88850a19'),
(384, 54, 2, 'atualizar sisauto e bdupdate2 ', '2017-04-12', NULL, '823982798  9q2v9c e inutilizar nfce 6439', 'Sisauto atualizado e nota inutilizada com sucesso', '2', '00:15:00', 'd0adec39e1e437128272ba44ba03bb0f'),
(385, 127, 2, 'servidor nao inicia', '2017-04-13', NULL, 'santana\r\nconfig servidor 1,5h - reparacao - 160,00\r\n1 pen drive 45,00\r\n12-04-17', '', '2', '00:12:00', '0b4768ab03ee339c8d6214c3f8e06490'),
(386, 70, 2, 'instalar cobian e google drive', '2017-04-13', NULL, '239664382   434bsj', 'instalado cobian e google drive', '2', '00:15:00', 'e75e92fa632c4608bc70b09a196f1b17'),
(387, 81, 2, 'Erro ao enviar nota, error code 000', '2017-04-13', NULL, '', 'Reiniciado uninfe, consultado servicos OK, reenviado nota fiscal e autorizada ', '2', '00:15:00', '620cb3b761bd01bcec4dc18f8688fc0f'),
(388, 57, 2, 'NFce nao emite', '2017-04-13', NULL, '207 657 519\r\ns 123456', 'Verificado problema, erro de comunicacao com impressora\r\npedido para delisgar impressora da energia e ligar novamente, instalado driver OK\r\npaginas teste OK, feito cupom de teste e cancelado em seguida, emissoes OK\r\nemitido NOta fiscal presa tambem, reenviado com data atual e autorizada\r\nliberado maquina para cliente', '2', '00:20:00', '1168aab430b4ad9c11b1538c7bbec2bc'),
(389, 68, 2, 'Erro validar nfe xml incosistente', '2017-04-13', NULL, '', 'Corrigido descricao dos produtos com acentuacao e caracteres especiais\r\nreenviado nota e autorizada', '2', '00:15:00', 'a7b2ad341a92af2fb07fd0a5b14b7b41'),
(390, 74, 2, 'Nfe de devolucao com icms', '2017-04-13', NULL, '', 'Feito nfe passo a passo com cliente, citado nfe referenciada\r\ne destacado icms usando csosn 900 e dados passados\r\nnota autorizada OK', '2', '00:15:00', 'f0f0dd356d5c193d5f8f08d367db24ea'),
(391, 41, 2, 'Instalar certificado digital', '2017-04-13', NULL, '140305666 / 274rxz', 'certificado instalado , e instalado tambem cobian e google drive', '2', '00:15:00', 'b2a5374cc583a70c011b4597e58cb88c'),
(392, 106, 2, 'ARQUIVO VDR', '2017-04-13', NULL, '801306567   1kf7a6   instalar visualizador de arquivo vdr', '', '2', '00:12:00', '4cc43533403ba12b896b9cd437febd0b'),
(393, 140, 2, 'Certificado digital novo emissor gratuito', '2017-04-17', NULL, '', 'Atualizado versÃ£o do emissor, configurado certificado digital novo para emissoes da nota\r\nusar repositorios do windows, para ao emitir nota, apenas selecionar o certificado desejado, ao inves de sempre digitar a senha', '2', '00:15:00', '62ab81a7eae51f31ad396b494dec1b26'),
(394, 134, 2, 'Relatorio de pedidos com observacao invertida', '2017-04-17', NULL, '', 'Corrigido rpt e enviado para todas as estacoes\r\nsisauto kerico e stock\r\n', '2', '00:15:00', '492390a41194f500feace0bf6da81360'),
(395, 45, 2, 'pjCheckout em modo desconectado', '2017-04-17', NULL, '', 'Erro ST3=13 ao iniciar venda, impressora offline\r\npedido para desligar impressora da energia e ligar novamente\r\ndetectado ecf configdes OK\r\ntestando vendas OK', '2', '00:15:00', '364771d0df7ac2e71f8807678a49b9e9'),
(396, 88, 2, 'certificado digital apagado', '2017-04-17', NULL, '145564424   k368jt', 'certificado colocado na pasta sisauto e configurado no acbr', '2', '00:15:00', '9f91cc6dfedbaa53e4fda7fd36d9c1ce'),
(397, 115, 2, 'instalar cobian e google drive', '2017-04-17', NULL, '200870371  6wxa45\r\ninstalar novo acbrmonitorplus - configurar email e testar no gerenciador de emails\r\ninstalar cobian e google drive', 'acbr instalado e configurado email, cobian e google drive instalados tambÃ©m', '2', '00:15:00', 'c788fcd723ef241514a24a23f8447d40'),
(398, 71, 2, 'pjCheckout em modo desconectado', '2017-04-17', NULL, '', 'Pedido para cliente desligar impressora da energia e ligar novamente\r\ndetectado ecf novamente, testado reducao z e vendas ok', '2', '00:15:00', '6971f6ce9e91f381f96d6594c10aaa80'),
(399, 83, 2, 'Nota em processamento no gernfe', '2017-04-17', NULL, '', 'Reenviado nfe e verificado retorno, cliente definido como ISENTO indevidamente\r\ncorrigido IE do cliente, exportado nota novamente e autorizada\r\nhabilitado tambem config3 para zerar dados do transporte sem aviso', '2', '00:15:00', '3cfe2ea409aebee834c45904370ada1d'),
(400, 141, 2, 'Configurar Sisauto para emissao NFe', '2017-04-24', NULL, '592140188 / m6bn57', 'Configurado sisauto pra emissao de notas, problema na autorizacao da nota usando uninfe com certificado A3, atualmente ficou configurado para autorizar pelo acbr\nfeito nfe de devolucao com cliente, proximas notas que cliente precisar sera acompanhado devido problema na assinatura digital', '2', '02:50:00', 'cfdcc38ccb7ab26501cc49e8ca8241c5'),
(401, 43, 2, 'atualizar sisauto e bdupdate2 ', '2017-04-17', NULL, '160951866  - atualizar sisauto e bdupdate2 e bdibge2 e acbrmonitorplus  instalar cobian e google drive\r\n', 'atualizados e instalados', '2', '00:15:00', '02d297fbc521a6d6ac2f7be269771577'),
(402, 88, 2, 'Erro ao gerar nfce servidor sefaz demorando para responder', '2017-04-17', NULL, '', 'reiniciado acbr, testado consulta servicos OK\r\ntestado reimprimindo nfce, em seguida testado emitindo nova nfce \r\nautorizou e imprimiu OK', '2', '00:15:00', '9065f964dd9e75f20b0ba501b8c51033'),
(403, 111, 2, 'enviar nfce e nfe ', '2017-04-18', NULL, 'nfce e nfe - enviar pra contabilidade- separar na maquina\r\n936560995\r\nu87f9s\r\nligar a impressora em rede\r\napos transmitir os arquivos pedir id e senha do outro micro\r\n', 'Compartilhado disco c, dado permissao total,  Todos\r\nmapeado unidade na estacao, testado acesso ao sisauto OK', '2', '00:15:00', '83691608f5eeed2455f4bd730d06b160'),
(404, 135, 2, 'copiar certificado digital', '2017-04-18', NULL, '321184082   y2my56   exportar certificado suvicor pra pasta pjcheckoutacbrenvioxml  com o nome suvicor_123.pfx   e acionar o programa dentro dessa pasta pra configurar o certificado', '', '2', '00:12:00', '586ed27f44e306d74d92e107db581463'),
(405, 90, 2, 'ver base dados', '2017-04-18', NULL, 'trazer base dados e xmls da pasta 201703  do nfce  e certificado digital tambem trazer\r\n880658407  \r\n', 'recolhido base', '2', '00:15:00', '2a46a087b41ec21df700f36fc07e2305'),
(406, 91, 2, 'Numeracoes faltantes nfce e nfe Curitiba', '2017-04-18', NULL, '', 'Verificado numeracoes que nao constam no genus\r\nsalvo xmls da pasta, e colocado faltantes na area de trabalho\r\nfoi excluido do genus as numeracoes, total 5 numeracoes\r\ninutlizado numeracao da nfce que pulou ok', '2', '00:15:00', '1fb037801f4b284ca190edc24ddb4846'),
(407, 34, 2, 'Instalar impressora maquina formatada Samsung WIFI', '2017-04-18', NULL, '', 'Instalado driver da impressora WIFI\r\nbuscado pelo endereco IP, paginas e documentos teste imprimindo OK', '2', '00:15:00', 'd0ad58048cd1f3bae9f8a19c658568aa'),
(408, 142, 2, 'Numeracoes faltantes nfce', '2017-04-18', NULL, '', 'Numeracao 2266 e 2267 pularam e nao inutiliza\r\nretonar que ja foram utilziadas\r\nverificado xml na pasta do genus, salvo os arquivos xml na area de trabalho para enviar separados OK', '2', '00:15:00', '2e5a9db0229415c8af2c187c92e68a0d'),
(409, 97, 2, 'Erro ao autorizar nota', '2017-04-18', NULL, '', 'Verificado retorno, certificado digital vencido\r\nconfigurado novo certificado no uninfe, enviado nota e autorizada', '2', '00:15:00', '79867c9c503edfccb5064919cda1ce08'),
(410, 34, 2, 'Configurar sisautos na estacao', '2017-04-18', NULL, '', 'Feito novo mapeamento na maquina, ja que servidor notas foi formatado\r\nconfigurado sisautos estacao, todos com acesso ok', '2', '00:15:00', '5e649971fc233deae50222e326c79415'),
(411, 43, 2, 'Navegadores nao carregam sites', '2017-04-18', NULL, '', 'Fixado DNS da Oi \r\n201.10.128.2\r\n201.10.120.3\r\ntestado acesso aos sites e emails todos ok', '2', '00:15:00', '972954ca044a242f6d792815fb9e6227'),
(412, 58, 2, 'Nao carrega gmail no internet explorer', '2017-04-18', NULL, '', 'Fica travado na tela inicial carregando email\r\nalterado para visualizacao em html basico e carregou, cliente ira usar assim qualquer coisa avisa', '2', '00:15:00', '939c4ea0d764f0320fd346971a08ebdf'),
(413, 143, 2, 'Erro suporte a canais seguros ao gerar MDFE', '2017-04-18', NULL, '', 'Alterado config internet, aba avancado\r\ndesmarcado usar tls 1.1 e tls 1.2\r\nreenviado mdfe e autorizado ok', '2', '00:15:00', '33a13980ffe405f8b578144bf41bab49'),
(414, 130, 2, 'atualizar sisauto e bdupdate2  e checkout', '2017-04-18', NULL, 'arten\r\n495519508\r\ne7wu61\r\natualizar sisauto e checkout  - depois receber cupons pelo sisauto\r\n', 'Recolhido base para analise\r\n', '2', '00:15:00', '6a50a7c79b4b0c6573b471d7529b82fb'),
(415, 130, 2, 'atualizar sisauto e bdupdate2 ', '2017-04-19', NULL, 'arten\r\n495519508\r\n466ivt\r\n\r\natualizar checkout tambem e instalar cobian e parece que ja tem dropbox - senao instalar google drive\r\n', 'atualizados', '2', '00:15:00', '9e01b30b962c41de00bbd35ed868efab'),
(416, 54, 2, 'atualizar sisauto e bdupdate2 ', '2017-04-19', NULL, 'inove\r\n823982798\r\nkb1g98\r\n\r\natualizar sisauto-bdupdate2\r\ndai avisar jean para terminar config', 'atualizados', '2', '00:15:00', 'cc0c7a197bd29bd37ff24f1dd1bc9687'),
(417, 45, 2, 'atualizar sisauto e bdupdate2 ', '2017-04-19', NULL, 'ammy 73644214', 'atualizados', '2', '00:15:00', 'e294fb686cfd1a855a3288e962ce36df'),
(418, 45, 2, 'atualizar sisauto e bdupdate2 e checkout', '2017-04-19', NULL, '894329572  nh36j6', 'concluido', '2', '00:15:00', '09bcc9bf593a07c24a7a75f0ef577e24'),
(419, 134, 2, 'AlteraÃ§Ã£o layout relatorio pedidos', '2017-04-19', NULL, 'Alterar cabeÃ§alho aumentar fonte, dados do cliente', 'Feito alteracao conforme necessidade do cliente, mandado e testado com clinete\r\naprovou esta ok', '2', '00:30:00', '484c4bbb37fda985e3dcda5b4aababd4'),
(420, 91, 2, 'Numeracoes faltantes nfce', '2017-04-19', NULL, '', 'Salvo todos os xml desde o mes 03-17 atÃ© 04-17\r\npara contabilidade importar e verificar ', '2', '00:15:00', '89b6cd11a496536259a7976e1779b92b'),
(421, 127, 2, 'erro ao emitir nfe', '2017-04-20', NULL, 'tabela corrompida  nf 11191 ao gerar - reiniciaram servidor pra verificar - tbcontas_rec crashed - acessado pelo remoto externo pra reparar base mysql da tabela acima - ensinado elizangela como cancelar nota fiscal da sequencia que ficou pulada - orientado sobre como gerar nota fiscal evitando o pulo-e inutilizar numeracao- 1h ', '', '2', '00:12:00', '467d5e742a033e97882e9494ec8f8651'),
(422, 145, 2, 'edson caloi - instalar genus', '2017-04-20', NULL, '582376857   6039', 'genus instalado com sucesso', '2', '00:15:00', 'be7a3c0c533472ca24f93777c10396ac'),
(423, 54, 2, 'atualizar sisauto e bdupdate2 ', '2017-04-20', NULL, '823982798  - dai transmitir a nota 384 (ver com jean)', 'atualizados e nota transmitida', '2', '00:15:00', 'de0651f061d027f4b7db045b69c2398c'),
(424, 72, 2, 'instalar paygo e tef', '2017-04-20', NULL, 'PAY&GO DUO Â­ INSTALAÃ‡ÃƒO COMPLETA\r\nDescriÃ§Ã£o\r\nEFETUAR A INSTALAÃ‡ÃƒO COMPLETA DO PAY&GO VPN INTEGRADA SSL Â­ NÃƒO Ã‰ NECESSÃRIO\r\nINSTALAR VPN EXTERNA DOWNLOAD DO PAY&GO Â­ www.ntk.com.br/download/paygo_160426.zip\r\nEFETUAR A INSTALAÃ‡ÃƒO DO HELPGO: https://ntkonline.com.br/hg/hg.exe (Somente Servidor) APÃ“S\r\nA INSTALAÃ‡ÃƒO FOCAR NO TREINAMENTO DO GUIA DE REFERENCIA E GUIA DE ADMINISTRAÃ‡ÃƒO COM\r\nO CLIENTE.', 'cefeq\r\n906275596\r\nq2d34n\r\n', '2', '00:12:00', '31dffa52b6faf6b8192e893468afc694'),
(425, 1, 2, 'tornex - sisauto', '2017-04-20', NULL, 'instalar sisauto - nova  empresa - instalar sisauto primeiro - dai jean ira pedir dados da empresa - rodar bdupdate2 modo 3 / atualizar bd2\r\n595272453  1x87zd', 'Sisauto instalado, orientado ao cliente a cadastrar produtos e se tiver alguma dÃºvida entrar em contato conosco', '2', '00:15:00', 'fa34eb78ec67bbaa195098367c310908'),
(426, 130, 2, 'Erro casas decimais ao transmitir nfe, apos atualizacao', '2017-04-20', NULL, '', 'Enviado executaveis do servidor, atualizado, reenviado nota e autorizada ok', '2', '00:15:00', '01fe9c1744f57ccb4b67160c2eedfaaf'),
(427, 115, 2, 'Nfe rejeitada, cst incompativel com operacao nao contribuinte', '2017-04-20', NULL, '', 'Alterado cst de 051 para 000, exportado nota e autorizada ', '2', '00:15:00', 'e2de3e73c5b4159c8fcc612efded170a'),
(428, 132, 2, 'Lentidao ao abrir form de orcamentos e finalizar', '2017-04-20', NULL, '', 'Com executaveis do dia 14-04 estava lento\r\nenviado de hoje para testes, antes reparado bases de dados\r\ntestado alguns orcamentos, a principio tudo ok', '2', '00:15:00', '3d3f5546a45bb5c81a38d7fb8f65d0ba'),
(429, 144, 2, 'Certificado digital novo', '2017-04-20', NULL, '', 'Configurado novo certificado digital no uninfe\r\nautorizado nota pendente ok', '2', '00:15:00', '71f3d7819ac04b0e1a47622518313695'),
(430, 45, 2, 'Erro ao transmitir nota, apos atualizacao sisaut', '2017-04-20', NULL, '', 'Enviado executaveis do servidor\r\natualizado na estacao e servidor do cliente\r\ntransmitido nota novamente e autorizada', '2', '00:15:00', 'b8f2e1b9fdeefed2d434cc6afcc7787c'),
(431, 1, 2, 'mercado prado', '2017-04-21', NULL, 'conexao via team e atendimento via celular- reconfiguracao usuario remoto2 - permissoes do servidor - 01h', '', '2', '00:12:00', 'a550830bd4f97510da1435aed1927022'),
(432, 80, 2, 'erro nota', '2017-04-24', NULL, 'imac\r\n468200389\r\nh712yz\r\nnao consegue emitir nota\r\n', 'problema nao era na nota, problema era para imprimir, configurado impressora novamente\r\ntestado impressoes ok', '2', '00:20:00', '386deb835456338c1b51963f52855fe1'),
(433, 42, 2, 'mapeamento rede', '2017-04-24', NULL, 'EL QUIMICA\r\nACESSO DA REDE\r\nMAPEAR NOVAMENTE\r\n860761646\r\n5619\r\n\r\nSERVIDOR\r\n750519964\r\nm5k19a\r\n\r\nocorreu ha um tempo atras, era o antivirus\r\ndo micro do servidor que bloqueava o acesso\r\nja tem icones pra mapeamento\r\n*silvanei', 'Unificado grupo de trabalho das maquinas, compartilhado rede, configurado mapeamento\r\ntestado conexao sisauto estacao e consultas ok', '2', '00:30:00', '7c0418ea1b59aed3df4da60f323dd557'),
(434, 127, 2, 'Nota autorizou mas nao baixou xml', '2017-04-24', NULL, '', 'Cancelado nfe e pedido para gerar nova nota, procedimento mais rapido', '2', '00:15:00', '16cb1d336c940382b7e2b5f2c76b141f');
INSERT INTO `chamados` (`id`, `id_cliente`, `id_usuario`, `problema`, `data`, `data_modif`, `conexao`, `solucao`, `status`, `tempo`, `anexo`) VALUES
(435, 63, 2, 'Nota rejeitada no gernfe', '2017-04-24', NULL, '', 'Total da nota fiscal difere dos valores que compoem o valor total da nota\r\nverificado itens da nota, csosn 500 nao calculava st, alterado para 5102 conforme nota anterior, nfe autorizada ok', '2', '00:15:00', '301724e50de2969f52baf0a3317fe6ac'),
(436, 145, 2, 'inst genus', '2017-04-24', NULL, '582376857\r\n9433\r\n\r\nGENUS', 'Cliente retornarÃ¡ posteriormente para finalizaÃ§Ã£o de uma nota', '2', '00:15:00', '32f51bb3dfb9319d4b06b50872bb354c'),
(437, 105, 2, 'Sisauto estacao nao conecta base de dados', '2017-04-24', NULL, '', 'Configurado atalho mapear e abrir sisauto automaticamente, unidade z\r\ntestado sisauto estacao ok', '2', '00:15:00', '6e1c8fee5606d7347a214bd36efa2a52'),
(438, 130, 2, 'erro emitir nfe', '2017-04-25', NULL, '495519508   711pdi   nf 707', 'retransmitido nota', '2', '00:10:00', '7eaca8364c740034ba66ec85c1deec32'),
(439, 34, 2, 'inst scanner samsung', '2017-04-25', NULL, '114386007\r\nu34a7j\r\nsamsung\r\ninstalar software edc de escaneamento', 'software reinstalado', '2', '00:12:00', '78232ac7abf5a60eed690bdd10731bcb'),
(440, 95, 2, 'Chamado papel', '2017-04-25', NULL, '', '', '2', '00:12:00', '9a8e8a06da2f5fb84df93799642b70a0.jpg'),
(441, 95, 2, 'Chamado papel', '2017-04-25', NULL, '', '', '2', '00:12:00', 'ae7d17e2efbd80902b6cbd89aa478587.jpg'),
(442, 34, 2, 'Chamado papel', '2017-04-25', NULL, '', '', '2', '00:12:00', '50b3f38dba7d8faad6419f9ada544967.jpg'),
(443, 34, 2, 'Chamado papel', '2017-04-25', NULL, '', '', '2', '00:12:00', '1e70114a085f97512e29e0513faa2052.jpg'),
(444, 46, 2, 'Chamado papel', '2017-04-25', NULL, '', '', '2', '00:12:00', '0624148fb44161a930cc8139b01d805d.jpg'),
(445, 146, 2, 'Chamado papel', '2017-04-25', NULL, '', '', '2', '00:12:00', '761e6136864da20b22be87c65346dd0d.jpg'),
(447, 51, 2, 'Chamado papel', '2017-04-25', NULL, '', '', '2', '00:12:00', '38dabf942ddba8eef5ad94a92c975d4f.jpg'),
(448, 41, 2, 'Chamado papel', '2017-04-25', NULL, '', '', '2', '00:12:00', '1a63f933ae5b8481d025373679dc675b.jpg'),
(449, 111, 2, 'Chamado papel', '2017-04-25', NULL, '', '', '2', '00:12:00', '967a444e09d0368aebef4f81ca4e9be2.jpg'),
(450, 33, 2, 'Chamado papel', '2017-04-25', NULL, '', '', '2', '00:12:00', '83af6c7298a80ae647b4d8d1131d376e.jpg'),
(451, 88, 2, 'Chamado papel', '2017-04-25', NULL, '', '', '2', '00:12:00', '0125a69ef852d26140bae808e5ca5f37.jpg'),
(452, 63, 2, 'Chamado papel', '2017-04-25', NULL, '', '', '2', '00:12:00', '53b8f7617f51643ca431aa059997eb2c.jpg'),
(453, 63, 2, 'Chamado papel', '2017-04-25', NULL, '', '', '2', '00:12:00', '195a85fbda42238d973aba09217eba3b.jpg'),
(454, 103, 2, 'Chamado papel', '2017-04-25', NULL, '', '', '2', '00:12:00', 'b5554a2eba8c7a899738b2543d4bb1c9.jpg'),
(455, 72, 2, 'atualizar checkout', '2017-04-25', NULL, '906275596  233qui     bdupdate3  escolher 1a  ', 'atualizado 1a', '2', '00:10:00', '799be5f7b200c29ad98c66c396fbc3b5'),
(456, 141, 2, 'emissao nfe', '2017-04-25', NULL, '592140188\r\ngdd228\r\n', 'Feito nfe de devolucao com cliente, finalizado nota pelo acbr, proximas notas ira ligar\r\npara fazer atÃ© corrigir os erros ', '2', '00:15:00', 'a5890f74880c08a3de71ff21bdefe11d'),
(457, 134, 2, 'Atualizacao relatorio pedidos', '2017-04-26', NULL, '', 'Atualizado rpt em todos os sisautos\r\ntestado impressoes saiu ok', '2', '00:25:00', '435720350066e67fd7f4065e73769cba'),
(458, 97, 2, 'emitir nfe', '2017-04-25', NULL, '534778469   v613vg   nf 741 e 742', 'executado novamente o uninfe', '2', '00:10:00', 'e5cce82db4e5d89e6107f7930c636c45'),
(459, 111, 2, 'atualizar acbrmonitor', '2017-04-25', NULL, '936560995  xbn769  atualizar acbrmonitorplus  - deixar pra ser executado como administrador - deixar como excecao no firewall', '', '2', '00:12:00', '724febeba7397ea9b39abe24df50beca'),
(460, 53, 2, 'config estacao', '2017-04-26', NULL, 'dreher\r\nconfigurar estacao sisauto\r\n166199692\r\n7491\r\n\r\nservidor\r\n739884557\r\nk74j9h\r\n\r\n', 'Mapeado servidor na estacao, registrado dll faltantes\r\nacesso sisauto OK', '2', '00:20:00', '358401c66b356e21db4579eec8d3e2ab'),
(461, 116, 2, 'at checkout', '2017-04-26', NULL, '495465945\r\n774qvu\r\n', 'atualizado e instalado cobian para fazer bkp as 9 horas', '2', '00:12:00', '1c3824ada0a123162cc39254ca867981'),
(462, 92, 2, 'Genus nao pode conectar ao mysql', '2017-04-26', NULL, '', 'Verificado servicos Dados e I2F, estavam parados\r\niniciado ambos, e alterado para inicializacao automatica, ambos com inicializacao manual\r\ntestado acesso genus OK', '2', '00:15:00', 'bda82bd3864cf60f385e5bc5fa0534bc'),
(463, 111, 2, 'NFCe nao imprime', '2017-04-26', NULL, '', 'Verificado motivo, impressora offline, foi mapeado impressora por ip do servidor\r\nmas servidor nao esta com ip fixo, alterado mapeamento para \\nomemicro\r\ntestado impressoes nfce ok', '2', '00:15:00', '67c3bd12d325bb09a1f4498ca4f37b34'),
(464, 35, 2, 'erro ao emitir nfe', '2017-04-26', NULL, 'aquarela\r\n241664216\r\nj679um\r\nver nota ncm erro\r\n', 'Problema, emitido nota com ncm errado\r\nsolicitado carta de correcao, emitido cce com cliente\r\ncriado pdf para enviar por email ok', '2', '00:15:00', 'b463823802fa367ca8069de80e699313'),
(465, 53, 2, 'Base de dados corrompida sisauto', '2017-04-26', NULL, 'dreher\r\n739884557\r\nk74j9h\r\nerro 3343 corrompendo dados\r\n', 'Reparado base de dados, executado bdupdate2 para recriar indices\r\ntestado sisauto servidor, e estacoes ok', '2', '00:15:00', 'f6c3fe490a23fb8a238c997266a6a19b'),
(466, 114, 2, 'at checkout', '2017-04-26', NULL, 'MAPELE\r\n705823259\r\nYK8Y14\r\nCOBIAN E GOOGLE\r\nE ATUALIZAR CHECKOUT\r\n', 'Atualizado checkout, verificado pasta do google drive', '2', '00:12:00', '848aa07b0be9dfb5d2f285fdbf465307'),
(467, 129, 2, 'pjCheckout em modo desconectado', '2017-04-27', NULL, '696132013\r\n2u36jp', 'Detectado ecf configdes. iniciado checkout, testado vendas ok', '2', '00:15:00', '49df2cf6d38ec3256dfeb41d8766ff15'),
(468, 132, 2, 'at sisauto', '2017-04-27', NULL, '567744523', 'atualizado e rodado bdup2', '2', '00:10:00', '1e37a21888106ff542033e739f19934b'),
(469, 46, 4, 'Erro nao catalogado nfce', '2017-04-27', NULL, '774983531\r\n123456', 'Servidores da receita ocilando, erros diferentes como erro nao catalogado, e servico paralisado momentaneamente, aguaradado alguns minutos, tentao emitir nfce ok\r\navisado sobre problema com receita, qualquer duvida entra em contato', '2', '00:12:00', 'f88e63a1db2ad4e9020fce09699d3756'),
(470, 148, 2, 'erro banco itau', '2017-04-27', NULL, '386865178   8618   silvane  - erro ao acessar banco itau', 'Baixado app guardiao do itau, e transferido pelo team viewer, cliente\r\ninstalou esta ok', '2', '00:35:00', 'd5d798a6bfcd3b9486b4ca3daacb1af4'),
(471, 86, 2, 'atualizar sisauto e bdupdate2 ', '2017-04-28', NULL, 'MERCEARIA TOZO\r\n730272485\r\nzd9q46\r\n-modo offline\r\n', 'atualizado e configurado nfc-e offline', '2', '00:10:00', '616eed494e1b6ce3f6d3585bc7fe8ad8'),
(472, 121, 2, 'erro ao iniciar outlook', '2017-04-28', NULL, '595475671   47d4vc  erro outlook e ver registro do windows - ativacao', 'comando no executar: outlook /resetnavpane\r\n', '2', '00:10:00', '6c6a38d9a1643a7d034c0a86d8ad2cdd'),
(473, 81, 2, 'checkout atualizar', '2017-04-28', NULL, '167565444   3n4sw7   -  checkout atualizar na outra estacao - providencia- estacao checkout - atualizar e testar o registros paf-ecf\r\n171318115\r\n71m9pa\r\n', 'atualizado e verificado registros paf-ecf', '2', '00:11:00', '1013ffc1bdf75b2f8552e60adc54f1a7'),
(474, 44, 2, 'Erro ao atualizar genus, sumiu executavel', '2017-04-28', NULL, '', 'Voltado exe anterior, atualizado genus, recriado atalho na area de trabalho\r\ntestado acesso genus OK', '2', '00:15:00', '8ac92901f37d67681dbaf4a8ca1cb3cb'),
(475, 73, 2, 'at relatorio briani', '2017-04-28', NULL, 'molasul\r\nbriani\r\n585577532\r\n6553\r\natualizar sisauto\r\ne avisar jean pra ver relatorio de ncm\r\n', 'atualizado e rodado bdupdate2 modo 3, enviado RPTs faltantes para relatÃ³rio de ncm', '2', '00:10:00', 'a8a1661764748d69361e75e99d0902fb'),
(476, 45, 4, 'baixar certificado digital', '2017-04-28', NULL, 'conexao- servidor- erro ao baixar xml pelo google chrome - utilizado outro navegador - explicado a usar internet explorer pra usar o certificado digital - 20m', '', '2', '00:12:00', '674ff9ff06781925051dbc8d9bdb97fd'),
(477, 81, 2, 'gerar reg paf ecf', '2017-05-02', NULL, 'providencia\r\ndaniel\r\n171318115\r\nkij248\r\ngerar registros paf-ecf e deixar na area de trabalho\r\n', 'Atualizado checkout.exe e bdupdate3 -1a\r\ngerado novamente arquivo, sem caracter  ? \r\ndeixado salvo na area de trabalho ok', '2', '00:15:00', 'e65960d1589b5ba7521eb9cc8149cca3'),
(478, 129, 2, 'erro comunicacao ecf', '2017-05-02', NULL, 'otica santa cecilia\r\n969132013\r\nx2cs53\r\nerro de comunicacao na ecf\r\nverificar\r\n', 'Pedido para desligar impressora da energia e cabo usb\r\nconectado novamente, foi preciso reinstalar drivers da ecf usb\r\ntestado comunicacao com configdes ok\r\nliberado maquina para usar', '2', '00:45:00', '07bbd2b1fca992a4ca51199eda11c4b7'),
(479, 84, 2, 'enviar arquivos para contabilidade â€“ NFE E NFCE', '2017-05-02', NULL, '824107951     uvt698', 'cobian configurado corretamente', '2', '00:10:00', 'df2f25181d19ff6543187ce89211aa0d'),
(480, 83, 2, 'enviar arquivos para contabilidade â€“ NFE E NFCE', '2017-05-02', NULL, '', 'arquivos recolhidos para enviar a contabilidade, base recolhida', '2', '00:12:00', '891189f51f83a924339f7c90241a796d'),
(481, 46, 4, 'enviar arquivos para contabilidade â€“ NFE E NFCE', '2017-05-02', NULL, '', 'caio mesmo envia os arquivos todo mes', '2', '00:12:00', '7b9c839fee5701bd3fba4176561739c5'),
(482, 35, 2, 'enviar arquivos para contabilidade â€“ NFE E NFCE', '2017-05-02', NULL, 'aquarela\r\n241664216\r\n123456\r\n', 'Atualizado Sisauto e BDup2, recolhido arquivos', '2', '00:10:00', '9c4d1bbce9c33954186a468b154ed0f6'),
(483, 88, 2, 'enviar arquivos para contabilidade â€“ NFECE', '2017-05-02', NULL, '', 'Gerado relatorio mensal\r\ninutilizacao faltantes, as que sobraram consultado\r\nsem faltantes, compactado arquivos e enviado por email', '2', '00:25:00', '9b4f1ee456e65ca0d037291906d9b4b4'),
(484, 90, 2, 'enviar arquivos para contabilidade â€“ NFE E NFCE', '2017-05-02', NULL, '', 'Gerado arquivos, corrigido pulos \r\ne buscado xml faltantes da pasta 201704\r\nestavam misturados alguns na pasta xmlnao consta\r\ne outros na pasta log dentro da raiz do acbr\r\ncorrigido arquivos, verificado furos novamente, sisauto nao reclamou\r\ntodas repassadas, enviado arquivos para contabilidade ', '2', '00:40:00', 'b2a30f8563ce77a4c632cef94184b4d6'),
(485, 87, 2, 'enviar arquivos para contabilidade â€“ NFCE', '2017-05-02', NULL, '', 'Gerado arquivos xml e relatorio mensal, enviado para contabilidade ok', '2', '00:15:00', '9d21bfad0b98a3bb5629721f59af0b59'),
(486, 79, 2, 'enviar arquivos para contabilidade â€“ NFE', '2017-05-02', NULL, '', 'Gerado arquivos sped perfil a e b - empresa box\r\narquivos xml da empresa docelandia, enviado todos por email ok', '2', '00:15:00', '20938b33311dd4982a6a794b80073206'),
(487, 150, 2, 'certificado digital vencido', '2017-05-02', NULL, 'GRAFITE\r\n586914501\r\n2h62ks\r\nverificar instalacao do certificado digital\r\ntestar consultando nota grande no uninfe e \r\nnota nfce\r\n', 'Instalado e configurado certificado digital novo\r\nconfiguracao uninfe micro notas, emitido nfe pendente ok\r\nconfiguracao acbr micro caixa, emitido nfce pendente ok', '2', '00:15:00', '845295922192baae17395e0adf2ff49f'),
(488, 135, 2, 'erro importar cupom', '2017-05-02', NULL, 'suvicor\r\n321184082\r\n65wzf6\r\ncupom 3911\r\nerro ao importar pra pedido\r\n', 'Copiado bdcheck, backup local\r\nalterado campo trans para S para todos os registros\r\ncomecara a controlar produtos pelo sisauto ao inves de usar tudo no checkout ', '2', '00:25:00', '23c52d1d6210b2eb2504543407c9a24c'),
(489, 102, 2, 'envio nfce', '2017-05-02', NULL, 'tocke modas\r\n729562979\r\n9ues79\r\natualizar sisauto\r\nverificar cobian\r\ngerar arquivo nfce do mes de abril\r\ndeixar na area de trabalho dela\r\n', '', '2', '00:12:00', 'f5850f928adfcb2ee579db1b232ba075'),
(490, 73, 2, 'correcao micro sheisa', '2017-05-02', NULL, 'molasul\r\nmicro sheisa - verificar lentidao - remover programas da inicializacao que possam estar incomodando\r\nverificar o que pode estar causando lentidao\r\n321308125\r\nk2t5c5\r\n', 'Chamado passado para welisson', '2', '00:10:00', 'd97c793a50f61f0e108ee6c6c5c44a58'),
(491, 73, 2, 'micro sheisa lentidao', '2017-05-02', NULL, 'molasul\r\nmicro sheisa - verificar lentidao - remover programas da inicializacao que possam estar incomodando\r\nverificar o que pode estar causando lentidao\r\nremover mensagem de cartuchos da hp se possivel\r\n321308125\r\nk2t5c5\r\n', 'retirado programas nao usados como toolbars, etc, \r\nlimpado temporarios com ccleaner, tirado programas inicializacao\r\nproblema esta no uso da cpu, processos usando\r\ncomo core.exe usado no banco do brasil', '2', '00:35:00', '7a8d1806677ad7df1fbbb17296c03988.png'),
(492, 50, 2, 'gerar reg ', '2017-05-02', NULL, 'gerar reg paf ecf mes 04 - mandar pro email jean  - 879620731  6843', 'gerado arquivo, enviado email ok', '2', '00:15:00', '03abe2023e38e407fca5f78804b3f1b6'),
(493, 72, 2, 'gerar reg paf ecf', '2017-05-02', NULL, 'cefeq\r\n906275596\r\npm927a\r\npara email jean', 'gerado arquivo e salvo na area de trabalho ok', '2', '00:15:00', 'feb40dfa7e78454acf58453916a676da'),
(494, 134, 2, 'mslicensing', '2017-05-02', NULL, ' kerico\r\nlicenca mslicensing\r\n333214161\r\n8802\r\nverificar\r\n', 'recriado registro ok', '2', '00:00:05', '9807667a3ae2ab2d1b829d2fde1e2180'),
(495, 116, 2, 'gerar reg paf ecf', '2017-05-02', NULL, '495465945   479mxs', 'gerado arquivo salvo na area de trabalho do cliente\r\npara enviar', '2', '00:15:00', 'f4caf1fe9ed88e4d41d320c6371b6680'),
(496, 86, 2, 'enviar nfce e nfe ', '2017-05-02', NULL, '730272485   r9z7d3   - atualizar sisauto e conferir cobian', 'inutilizado numeracoes faltantes, repassado nfce\r\nconsultado nfce que ainda faltaram \r\nrelatorios e xmls OK', '2', '00:15:00', '5eb03370e497d9bfc71244ab373bf9e5'),
(497, 72, 2, 'gerar reg paf ecf 2', '2017-05-02', NULL, 'cefeq\r\n906275596\r\npm927a\r\natualizar checkout e gerar novo reg paf ecf\r\n', 'feito no chamado anterior', '2', '00:12:00', 'a3afa99b7ed046ac59ed5ccd9aea7b90'),
(498, 85, 2, 'gerar nfce arquivos', '2017-05-02', NULL, 'otica marli\r\n591633938\r\ngeq154\r\n', 'copiado arquivos para pendrive OK', '2', '00:15:00', '03d04088a48a56dec3087b63c104d763'),
(499, 111, 2, 'enviar nfce e nfe ', '2017-05-03', NULL, '936560995  8jfu41', 'gerado arquivos mensal, nfce e nfe\r\nverificado pulos ok, enviado por email ok', '2', '00:15:00', '09053085374bc5c6bc116a1a2cd26aaf'),
(500, 93, 2, 'Cadastro pessoa', '2017-05-03', NULL, '', 'Feito novo cadastro com cliente passo a passo ok', '2', '00:15:00', 'cde14211192f4eb2cd18b5299c4da490'),
(501, 43, 2, 'trazer base sisauto', '2017-05-03', NULL, '160951866  917vkh', 'base recolhida para analise', '2', '00:10:00', 'ee05c162477a2e05187d8b0f215d3f5b'),
(502, 1, 2, 'atualizar sisauto e bdupdate2 ', '2017-05-03', NULL, '347179951   8765  bdibge2 tambem - dai avisar jean pra fazer testes nos remotos', 'Sisauto atualizado e verificado processos, remoto 11 com 2 processos de sisauto.exe', '2', '00:12:00', 'cd5415d331bd0d1432f437e167891bf5'),
(503, 151, 2, 'Erro ao atualizar genus, sumiu executavel', '2017-05-03', NULL, '', 'Voltado exe anterior, atualizado genus\r\nrecriado atalho na area de trabalho ok', '2', '00:15:00', '62c9a6c3d321accfce518922600ef8c7'),
(504, 112, 2, 'Diferenca valor cupom da nota', '2017-05-03', NULL, '', 'Verificado item que puxou com quantidade diferente\r\nalterado antes de gravar nota para quantidade correta\r\ncomparado valor total da nota igual valor total do cupom\r\n', '2', '00:15:00', '9cb47eaa5e40bd0f36251346d56a30a3'),
(505, 102, 4, 'Mensagem retentar nfce', '2017-05-03', NULL, 'id 729562979   atualizado ok e conferido com ela ok', '', '2', '00:12:00', 'eb2f772c0de0a7b09b3d75f6bd07c0f4'),
(506, 102, 2, 'atualizar sisauto e bdupdate2 ', '2017-05-04', NULL, '729562979   bhub59', 'Sisauto atualizado, configuracao 3139 alterado o valor para S', '2', '00:10:00', '38e68cef29bd898b62462a73c8135753'),
(507, 154, 2, 'Configurar impressora mp4200 th', '2017-05-05', NULL, 'Deixado recado bloco de notas para verificar com tecnico \r\npara ver impressora localmente\r\nmapeamento da lpt1 ficou ok, verificar impressora para depois ver sistema\r\nninguem respondeu durante a tarde toda', 'Ligou em 05-05\r\nmaquina foi colocado para formatar assim que estiver ok retorna', '2', '00:12:00', '781aa19f20817b6c00cfa916a2590ae2'),
(508, 119, 2, 'acrimastey verificar certificado digital', '2017-05-04', NULL, '288158766  p12ds7  acrimastey verificar certificado digital', 'Configurado certificado digital novo no uninfe\r\nreenviado nota e autorizada ok iniciado acbr que estava fechado', '2', '00:15:00', '50afe088cf64ac0ed79a6dc34d8428f2'),
(509, 150, 2, 'download nfe', '2017-05-04', NULL, 'grafite\r\n571700749\r\ne36fr6\r\ndownload de nota com certificado digital nao esta dando certo\r\ntentado por todos os navegadores\r\n', 'Cliente tentando baixar arquivo com certificado vencido\r\ninstalado novo certificado na maquina, testado baixando com chrome OK', '2', '00:15:00', '891a8113e80351de79a52bdcf356c832'),
(510, 93, 2, 'Cadastros pessoas, e gerar ctes', '2017-05-04', NULL, '', 'Cadastrado novas pessoas, e gerado ctes de pessoa fisica OK', '2', '00:50:00', '2e530a2b9686e6939316b4b7b5192b7f'),
(511, 71, 2, 'pjCheckout em modo desconectado', '2017-05-04', NULL, '', 'Detectado ecf configdes\r\ntestado acesso checkout ok', '2', '00:15:00', 'cd194a26d4c9b634d4fad8d663d56514'),
(512, 72, 2, 'Maquina formatada, recriar atalho sisauto', '2017-05-05', NULL, '', 'Recriado atalho para conexao remota\r\nusuario remoto8, testado acesso OK', '2', '00:15:00', '5214ec204b69f4281d3ab8b3a23053c6'),
(513, 54, 2, 'Notebook nao acessa basesisauto', '2017-05-05', NULL, '', 'Verificado mapeamento do servidor desconectado\r\nmapeado servidor novamente, e atualizado sisauto de acordo com versao do servidor\r\ntestado acesso OK', '2', '00:15:00', 'd645ad46290488195d871ddaa848764b'),
(514, 71, 2, 'pjCheckout em modo desconectado', '2017-05-05', NULL, '', 'Atualizado checkout + bdupdate3\r\npedido para desligar impressora da energia e ligar novamente\r\ndetectado ecf configdes \r\ncheckout ok e rz emitida', '2', '00:15:00', 'b387d93a836a61183fccc63a748419c2'),
(515, 155, 2, 'Reiturbo, erro ao gerar nota imposto', '2017-05-05', NULL, '564996225\r\nugu378', 'atualizado sisauto em todas as estacoes + bdupdate2\r\ntestado editando pedido e gerando nota, autorizada ok', '2', '00:20:00', 'e15482efa9f75eb1f22aefb0836c5a78'),
(516, 149, 2, 'baixar xml 85', '2017-05-05', NULL, 'baixar no site da receita a cte do xml 85 da salvelox   787113116', 'copiado arquivo do evento gerado, na pasta unicte -> cnp\r\ncolado na area de trabalho para cliente enviar por email para contador', '2', '00:15:00', 'bd6cf3fce4066835542e33961ee7e724'),
(517, 99, 2, 'Erro ao atualizar genus, sumiu executavel', '2017-05-05', NULL, '', 'Voltado exe anterior, atualizado genus ok\r\nrecriado atalho na area de trabalho\r\ntestado acesso ok', '2', '00:15:00', '87b0f85749370769c9614e142728d9aa'),
(518, 72, 2, 'Arquivo cce invalido ao gerar carta de correcao', '2017-05-05', NULL, '', 'Copiado arquivo xml do evento da pasta 201704 e colado na pasta 201705\r\ntestado impressao OK', '2', '00:15:00', '36bde97b95c76145e68041d18aa79ebc'),
(519, 42, 2, 'Atualizar team viewer, gernfe nao envia email, sisauto nao emite cupom', '2017-05-05', NULL, '996834615\r\n123456\r\n-verificar impressora digitalizacao\r\n-zerar bases gernfe e base sisauto', 'Atualizado team viewer OK\r\nreiniciado acbr, consultado servicos OK\r\ntestado emissao cupom OK\r\ntestado envio do email OK\r\ndeixado copias no cliente e no servidor support \r\nfeito zeragem atÃ© fim de 2015 OK\r\n', '2', '00:45:00', '161e48b715640dfceb14aa6042f2df16'),
(520, 72, 4, 'Erro checkout, e relatorio vendas cartao', '2017-05-08', NULL, '906275596\r\n123456\r\n09-05-17 -atualizar sisauto e checkout e mandar novo rpt da public rltitrec9 - gerar registros paf ecf mes de maio- se preciso rodar bdupdate3 opcao 1a novamente\r\n', 'Atualizado checkout, rodado bdupdate3 gerado registros OK\r\natualizado rpt, testado relatorio OK', '2', '00:20:00', '64cd34aff8be3cd08c73f9ac2997ef0f'),
(521, 154, 2, 'Configuracao MP4200 e sistema NFCE', '2017-05-08', NULL, '', 'Voltado pastas do backup, acbr e arquivos xml\r\nconfigurado atalho da conexao remota\r\nmapeado unidade do servidor porta lp1\r\nconfigurado impressora no acbr, feito testes de emissao e impressao\r\nsistema ficou rodando OK', '2', '00:02:00', 'ccc44e5fb83482a0bda4b2cad66b719f'),
(522, 156, 2, 'instalar cert digital', '2017-05-08', NULL, '951506387\r\njn278z\r\n\r\n', 'Certificado instalado e configurado no uninfe', '2', '00:10:00', 'edc9c363c491857fb957dfee483899f6'),
(523, 42, 4, 'at sisauto vanessa', '2017-05-08', NULL, '996834615  jgd458', '', '2', '00:12:00', 'a6c180831301b1e7c1060156236c2ced'),
(524, 137, 4, 'MONITOR VENDA', '2017-05-08', NULL, 'LEVARAM 1 CRT - 50,00 - USADO', '', '2', '00:12:00', 'f9576c093c84d6d0298a694962fa5615'),
(525, 42, 2, 'sem internet pra nota', '2017-05-08', NULL, '996834615\r\n1u5aq8\r\ntania da el quimica\r\nsem conexao email e sisauto notas\r\n', 'conexao com internet ok\r\ntestado consulta nfe ok\r\ntestado carregando email bol ok\r\nhabilitado modulos avast ok', '2', '00:15:00', '3e7788234a33795601f04013533eb33d'),
(526, 48, 2, 'xml contabilidade', '2017-05-08', NULL, 'enviar arquivos xml oticas real e popular pra cintia@universalcontabilidadesc.com.br', 'gerado arquivos xml e relatorio mensal\r\nenviado ok', '2', '00:15:00', 'aa20c2178e8b95030310b57189de03bb'),
(527, 115, 2, 'Erro ao gerar nfe, schemas nao localizados', '2017-05-08', NULL, '', 'Atualizado versao do uninfe, reenviado nota fiscal e autorizada OK', '2', '00:15:00', 'e31c086ad2a1ef64e2970e1d0f7433be'),
(528, 80, 2, 'reinstalar avast', '2017-05-08', NULL, 'imac - instalar avast - 667123545  auy849', 'Desinstalado versao do avast\r\ninstalado novo pelo ninite', '2', '00:30:00', 'e91df5ca996cad5dfec7559159773ff7'),
(529, 103, 2, 'erro ao enviar email', '2017-05-08', NULL, '640277368\r\nuq444s\r\ncetec\r\nerro ao enviar email gerenciador de notas\r\n', 'Alterado conta de email do yahoo para conta padrao gmail\r\nnfenaoresponde@gmail.com\r\nnotas2016 ', '2', '00:15:00', '92316a1f13c9223af18f69cf80c72ba2'),
(530, 42, 2, 'erro ao enviar novo email', '2017-05-08', NULL, '996834615   1u5aq8   - erro ao enviar novo email', 'conectado e nao tinha nenhum erro na tela\r\ntestado email funcionou tudo normal, cliente testou tambem normal\r\nemail no navegador bol, provavel que servidor esteja ocilando', '2', '00:12:00', '2f10c274497e7fb92993a0de0abea546'),
(531, 1, 2, 'Magrin - Tirar backup cobian genus e ver numeracao notas emissor free', '2017-05-08', NULL, '', 'Removido tarefas do cobian, e mostrado consulta das notas por periodo no emissor gratuito', '2', '00:15:00', '4f53d5ce5f830983c573a295ac0d435d'),
(532, 127, 4, 'erro ao emitir nfe', '2017-05-09', NULL, 'atendimento via celular e team viewer -  - ajuste manual na nfe via txt - diferenca campo outros informado centavos diferente do calculado. 20 min.', '', '2', '00:12:00', '12e09ce7cf3413b3eb45d6a82307ed99'),
(533, 47, 4, 'compra', '2017-05-05', NULL, '3 bobina etiq (45)=135,00  e  2 ribbon (35) - 70,00=  205,00', '', '2', '00:12:00', '206054d8e4f647a9172f8b632fd827d7'),
(534, 157, 4, 'compras', '2017-05-09', NULL, '2 bobinas 45x20 (45,00) = 90,00  (25-04-17)', '', '2', '00:12:00', 'df3818133ca4d24196d7e3d25b6e371f'),
(535, 127, 4, 'config aliquotas', '2017-05-09', NULL, 'atend via fone e team - config aliquotas NT- para 1,07  e santanna para 3,50 no icms - ligado pra contabilidade pra confirmacao e ajuste nos grupos fiscais - ok - 20 min ', '', '2', '00:12:00', '89fbe9f9489349fdeeb25496eaaf3b73'),
(536, 127, 4, 'config aliquotas', '2017-05-09', NULL, 'atend via fone e team - config aliquotas NT- para 1,07  e santanna para 3,50 no icms - ligado pra contabilidade pra confirmacao e ajuste nos grupos fiscais - ok - 20 min ', '', '2', '00:12:00', '51ae3501c4989a5e0ab04e1889e3e78a'),
(537, 127, 2, 'gerenciador de notas novo', '2017-05-09', NULL, 'enviar novo gernfe2 pra substituir o da santana - porque o deles nao esta aceitando inutilizar a nota - esta dando erro - copiar o executavel antigo de backup - inutilizar santana\r\ninutilizar\r\n11188 a 11190  e\r\n11192 e 11193\r\n- verificar os backups de santana e nt se cobian esta executando e tambem backup do mdb dos gernfe - se estao indo pra nuvem corretamente\r\n\r\n163857960 ', 'atualizado gernfe\r\n11188 a 11190  - ja estavam inutilizadas, o erro retornava que estavam inutilizadas no sefaz\r\n11192 e 11193 - inutilizadas ok\r\n- backups estao fazendo copia OK gernfe OK\r\n\r\n', '2', '00:20:00', '2f2bc70f214b8c62e49205f204ffccfc'),
(538, 96, 2, 'envio xml', '2017-05-09', NULL, 'madelenha\r\n283066091\r\nenvio xml cte\r\n', 'Salvo arquivos pela exportacao do genus,\r\ncompactado na area de trabalho\r\nambas empresas', '2', '00:15:00', 'd66346e50ca63f12ae3571a6ad87fbab'),
(539, 49, 2, 'enviar nfce e nfe ', '2017-05-09', NULL, 'TREVO\r\n720460957\r\nwr7k59\r\nnfce e nfe\r\narea de trabalho\r\n', 'salvo na area de trabalho', '2', '00:15:00', '0b1a1d277e5812b1518440bbb4b51aad'),
(540, 58, 2, 'Nota nao autoriza no gernfe', '2017-05-10', NULL, '', 'Verificado que uninfe nao estava funcionando corretamente, ao abrir form todo branco\r\nfinalizado processo e iniciado novamente para testar, voltou ao normal\r\nrenviado nota e autorizada', '2', '00:15:00', '552d4cf11e38dd6cf07ba847f28644fb'),
(541, 50, 2, 'Instalar MP4000 para USB', '2017-05-10', NULL, '', 'Instalado driver USB MP4000 TH FI 32bits\r\nalterado bemafi32.ini para porta USB\r\ndetectado ecf configdes OK\r\nfeito venda de teste, impressora funcionou normalmente', '2', '00:15:00', 'b9bbe8f6cb66af047f9e6e358441c202'),
(542, 33, 2, 'envio nfe', '2017-05-10', NULL, 'ortobrasil\r\n936405236\r\n5f41ix\r\nnfe pra area de trabalho\r\nem pdf e xml mes 04\r\n', 'salvo arquivos xml e pdf na pasta do cliente', '2', '00:15:00', '0eea19f213e371e24003c79366fdd16b'),
(543, 158, 2, 'Gerar Sintegra 2cx', '2017-05-10', NULL, '', 'atualizado checkout dois cx\r\ngerado arquivos enviado no email fiscal@orgama.com.br', '2', '00:20:00', 'c8810ea4be7b7d4247dc45a2ad372c9c'),
(544, 56, 2, 'erro ao emitir nfe', '2017-05-10', NULL, '696317285\r\ngfn632\r\nnf 280 ou 279 (podem ser as mesmas ele falou)\r\nvalmor\r\nmadeireira campo da lanca\r\nnfe com erro\r\n', 'Apenas reenviado nota com data atual\r\ninutilizado pulos ', '2', '00:15:00', 'aedb17e5a5f3f6a81af9a83200e5e894'),
(545, 90, 2, 'nfce-erro ao emitir', '2017-05-10', NULL, 'kel cosmeticos\r\n498327714\r\n546brn\r\nerro ao emitir nfce\r\nfizeram uma limpeza no micro\r\ntinha travado\r\ndepois disso dando erro na nfce\r\n', 'Erro de comunicacao com porta da impressora\r\nreiniciado acbr, alterado porta COM3 para COM1\r\nimpressao escpos, testado nfce e impressao ok', '2', '00:15:00', '7406a0f38472242192287c3e88520d76'),
(546, 80, 2, 'Transferencia sistema Mad. Campo lanÃ§a', '2017-05-10', NULL, 'imac\r\n468200389\r\n59p3ec\r\npara instalar sistema da madeireira campo da lanca\r\ne instalar cobian backup  nesse micro-cobian da pasta toda de dados\r\nautomatico pras 09h\r\nfalta valmor passar id do micro dele\r\n', 'feito transferencia dos dados\r\nconfigurado empresa uninfe, dados do gernfe para gernfe ja instalado\r\nconfigurado atalho SISAUTO CAMPO na area de trabalho,\r\nemitido nfe de teste e cancelado\r\nconfigurado cobian e gdrive, dados da conta no cad do cliente \r\n', '2', '00:01:50', 'f04d92a45699dc41ae8cee0b71b2a278'),
(547, 159, 4, 'CÃ³digo de barras nao encontra item no pedido', '2017-05-10', '2017-05-11', '640109385  1692  atualizar sisauto e dai pedir pra ele colocar o codigo de barras no campo codigo de barras do  produto e testar no pedido ver se sai ok', 'atualizado sisauto bdupdate2\r\njean verificou para nao usar os codigos no campo complemento e cod. barras ao mesmo tempo, usar somente cod. barras\r\ntestado ok', '2', '00:15:00', '2ee8cbbf891c63c61eb2fee5618a7ca0'),
(548, 54, 2, 'Notebook nao acessa basesisauto', '2017-05-11', NULL, '', 'Criado atalho sisauto.bat, que mapeia servidor e abre sisauto em seguida\r\npara sempre estar recriando a conexao\r\ntestado sisauto ok', '2', '00:15:00', 'f756eefbbb97108df1a078ebf6e5d423'),
(549, 80, 2, 'Impressora nao imprime', '2017-05-11', '2017-05-11', '', 'Deletado documentos da fila de impressao, reiniciado servico spooler \r\nna maquina servidor, reiniciado micro\r\ntestado impressao OK, testado na estacao tambem OK', '2', '00:20:00', '33abb9bb48bc82ec6819047ffe1a2429'),
(550, 42, 2, 'Relatorio de titulos pagos', '2017-05-11', NULL, '', 'Gerado relatorio de titulos pagos com silvonei\r\nverificado se era o que cliente precisava, esta ok o relatorio ', '2', '00:05:00', '940ee69ef27e9b488d7f5f4b3bddefd5'),
(551, 158, 2, 'Enviar inventario sintegra contador', '2017-05-11', NULL, '', 'Atualizado checkout nas estacoes, gerado arquivo dos caixas novamente\r\nenviado para email contabilidade', '2', '00:15:00', '9327825f69637fde19401efff6c9c749'),
(552, 63, 2, 'Configuracao assinatura webmail contato@ferplax', '2017-05-11', NULL, '', 'Recriado assinatura do email, com descricao e logo da empresa\r\nconforme dayane precisava OK\r\nverificado tambem opcao para receber confirmacao de recebimento e leitura de email no webmail OK', '2', '00:20:00', 'fe93b6de948e3819d5ec7b1cef618d82'),
(553, 50, 2, 'Cupons com titulos a receber duplicados', '2017-05-11', '2017-05-12', '', 'Atualizado checkout e bdupdate3\r\natualizado sisauto e bdupdate2\r\nalterado campo transf tbr04 para N todos os registros desde data 01/05 atÃ© o momento, porem os titulos ainda ficaram duplicados, sera enviado nova alteracao\r\n12-05-17\r\nalterado campo transf tudo pra S\r\nsisauto atual nas estacoes, passado para estornar manual os titulos que duplicaram\r\ndevido demora no importar cupons\r\nproximos estarao OK', '2', '00:45:00', 'e646eefedebca1c40c962b5553fed5a0'),
(554, 43, 2, 'atualizar sisauto e bdupdate2 ', '2017-05-11', '2017-05-11', '609617567  ATUALIZAR SISAUTO E RPTS ATUAIS - PRA RODAR O RELATORIO NFCES DE 10-05 ATE FIM DO MES - DAI NAS FALTANTES QUE EXIBIR - INUTILIZAR EM SERIE  \r\n\r\n 524896662\r\n8580\r\n\r\n165556707\r\nv1x15v\r\n\r\n', 'corrigido numeracoes do mes 04/17 e mes 05/17\r\ninutilizado sequencias, corrigido arquivos que estavam fora da pasta ANOMES\r\ne consultado faltantes para repassar, relatorios e arquivos OK\r\natualiazado demais estacoes, total 3 maquinas', '2', '01:05:00', '958587617f84618ccb31f15c5802726a'),
(555, 92, 4, 'cadastro exterior cliente', '2017-05-11', '2017-05-11', '', 'no genus pra cadastrar cliente como exterior - adicionar em cadastro - enderecos - UF igual EXTERIOR - codigo ibge 99 -  dai municipio cadastrar EXTERIOR com codigo ibge 9999999  - CNPJ TUDO ZERO e inscricao estadual isento', '2', '00:10:00', '8fe6e11ffd4bb94fbad6bd32cac9321b'),
(556, 54, 2, 'Erro ao converter arquivo nota gernfe', '2017-05-12', NULL, '', 'Verificado erro, reclamando do CNPJ nao atende 14 digitos\r\nverificado cad. cliente, cadastrado pessoa fisica como juridica\r\nalterado de juridica para fisica, reenviado nota e autorizada', '2', '00:10:00', 'c40ed396791b8764d9583249f6631a55'),
(557, 160, 2, 'Transferencia sistema para servidor novo', '2017-05-12', '2017-05-12', 'Team Viewer Server\r\nid 261459703\r\nsenha 9843\r\n\r\nEstacao\r\n602091936\r\nzzz966', 'Instalado sistema no servidor\r\nconfigurado base .sql \r\ninstalado e configurado driver odbc\r\noffice 2007 e dll OK\r\nsistema rodando no servidor OK\r\ncontinuar segunda, configuracao nas demais estacoes', '2', '02:15:00', 'de8ca96d8cbb1da498a793267f266a1c'),
(558, 64, 2, 'atualizar sisauto e bdupdate2 ', '2017-05-12', '2017-05-12', 'autocenter\r\n134617345\r\nm514hr\r\n\r\n', 'Atualizado sisauto e bdupdate2 \r\nAtualizado checkout e bdupdate3', '2', '00:10:00', '1555ac3eb9ed4b5007d05ec9573367fd'),
(559, 92, 2, 'erro ao emitir cte', '2017-05-12', '2017-05-12', '515640101   9726   erro soap - arquivo nao encontrado - ao consultar cte e ao emitir cte   -   653048376\r\nt977hb  novo micro\r\n', 'Transferindo bases .sql spolti e saf para notebook\r\n+ arquivos xml mdfe e cte de ambas empresas\r\nfeito backup da pasta i2f toda, e mais bkp em sql e no sqlyog das empresas\r\nimportado bases do pc > notebook, testado conhecimentos e mdfe feitos OK\r\nconsultas OK, emitido cte 251 saf OK', '2', '01:50:00', '2b85986fe42e58495c726e5e76259a9c'),
(560, 73, 2, 'micro briani', '2017-05-12', '2017-05-12', 'briani\r\n585577532\r\n2841\r\npassar programas \r\npara melhorar a velocidade da maquina\r\ndeletar temporarios\r\ndeixar um icone com nome SISAUTO ANTIGO\r\npara ela usar para consulta de cadastros\r\no icone atual do SISAUTO\r\nesta atual somente pra ela gerar relatorio de ncm\r\nou entao tentar rodar o bdupdate2 novo em modo sem verificar base aberta\r\ntambem ja deve resolver dai nao precisa 2 icones\r\n', 'Rodado ccleaner, deletado registros e temporarios \r\ndeixado atalho SISAUTO ANTIGO e o atalho do sisauto que esta atualmente\r\nporque bdupdate2 sem verificacao de estacao nao resolveu\r\n', '2', '00:20:00', 'da7e73c618310c97876b66d9d674e0c2'),
(561, 161, 2, 'reparador sisauto', '2017-05-12', '2017-05-15', 'tiago tratores 394551963   4467  repararsisauto e fazer backup -instalar cobian', 'reparado base de dados\r\nexecutado bdupdate2\r\ntestado consulta cadastros, e insercao de pedido OK\r\nconfigurado backup com drobpox do cliente OK', '2', '00:35:00', '679d9425a9ab571d248b737af823d194'),
(562, 50, 2, 'Licenca ts expirou 2 estacoes', '2017-05-15', '2017-05-15', '', 'Enviado deleta licenca server.bat\r\ndeixado na area de trabalho do cliente para usar quando precisar\r\nmostrado como usa, testado conexao com servidor ok', '2', '00:10:00', 'd6a29756f59410f58aa96149f981d0a9'),
(563, 162, 2, 'Hotmail nao acessa, mensagem de conexao segura', '2017-05-15', NULL, '', 'Restaurado configuracoes do IE\r\nsincronizado horario pela internet, nas config de data/hora\r\ntestado acesso site OK', '2', '00:10:00', 'f3195e865533bf44c90f31d097abaed0'),
(564, 73, 4, 'lentidao gravar prod molasul', '2017-05-15', '2017-05-15', 'molasul:\r\n809971605\r\n7244\r\ntrazer base de dados\r\nsisauto\r\nlentidao - nao precisou trazer - somente mudado a config 3169 pra N - ja deu certo', '', '2', '00:00:00', 'dff1c1a78da7945aa6daba640c019606'),
(565, 92, 2, 'genus na nuvem', '2017-05-15', '2017-05-22', 'testar genus na nuvem - transferir base dele pra servidor i2f.com.br\r\nmanter a base dele antiga no micro se precisar pra uma emergencia\r\n', 'Testado genus com base no server\r\nficou mais lento que local, carregamento de telas e registros demorando\r\nuns 15-20seg\r\n-----------------------\r\nVerificar como ficarÃ¡..\r\n\r\n', '2', '00:00:00', '55482161863026db4189f3e3e71f1d22'),
(566, 50, 2, 'erro ao imprimir orcamento', '2017-05-15', '2017-05-15', 'cofepar\r\n241969437\r\n8036\r\n\r\nerro ao imprimir orcamento\r\ndesde semana passada sem imprimir\r\n\r\n', 'ERRO Invalid Formula Name AO IMPRIMIR ORCAMENTO\r\nENVIADO RLORCACOMPLETO2.RPT, TESTADO IMPRESSAO POREM CLIENTE USAVA COM LOGO O RELATORIO, AVISADO QUE SERIA ALTERADO NOVAMENTE, MAS FECHARAM CONEXAO\r\nAGUARDAR RETORNO PARA ALTERACAO DO RPT', '2', '00:10:00', '09ab891088bdd4794f5af12489409695'),
(567, 106, 2, 'erro ao emitir nfe', '2017-05-15', '2017-05-15', '801306567\r\n2g71da\r\nnf 1887\r\nimprimiu - cliente levou a folha\r\nmas esta acusando rejeitada\r\n\r\navisar pelo bloco de notas o resultado\r\n', 'ADICIONADO NFE MANUALMENTE NO GERNFE.MDB\r\nPORQUE NOTA AUTORIZOU MAS NAO GRAVOU A INFORMACAO\r\nE NAO ESTAVA MAIS CONSTANDO COMO REJEITADA\r\nDEIXADO NFE OK NA GRID, TESTADO IMPRESSAO E CONSULTA OK', '2', '00:30:00', 'cf1cecb94601a45d520b1d18caff14f0'),
(568, 156, 4, 'micro - placa mae danificada', '2017-05-15', NULL, '\r\nstephanes\r\n9951 1145\r\n3642 9122 (rogerio trabalha la)\r\npassar orc\r\nmicro\r\nplaca mae -  2 gb\r\norc\r\nplaca mae 775 - 390,00\r\nservico config e entrega = 250,00\r\ntotal 640,\r\naprovaram ficou de levar de tarde\r\nver cobr. em 2 x\r\n', '', '2', '00:00:00', 'bcdbb4da8d3b84047015a4943dcfb304'),
(569, 123, 2, 'nfe genus', '2017-05-15', '2017-05-15', 'ari lajes\r\n781571326\r\n258dit\r\n\r\nerro cliente sem inscricao estadual no genus\r\n', 'RETORNO IE DO DESTINATARIO NAO INFORMADA\r\nAPENAS CADASTRADO IE DO CLIENTE\r\nREENVIADO NOTA E AUTORIZADA', '2', '00:10:00', '0f1c88a95b1c07989060766bd7b79928'),
(570, 63, 4, 'email ellen erro', '2017-05-16', '2017-05-16', '540678683   953zus   -   limpar cache do site metalplas.ind.br somente sem apagar os outros caches que ela tem - ao testar com webmail esta acessando email da nfe@ com a senha do antigo  nota_800', '', '2', '00:00:00', '586d7f902a24fb1a215d56688c493fb8'),
(571, 160, 2, 'inst sistema desempenho servidor', '2017-05-16', '2017-05-16', '261459703  5a95ww', 'Sistema servidor ok, qualquer problema entrarao em contato', '2', '00:00:00', '5008e1a5c7416792c0e17a481c8ce4e4'),
(572, 81, 2, 'Erro uninfe ao consultar CNPJ SC 403 FORBIDDEN', '2017-05-16', '2017-05-17', '259 396 335\r\n123456 x198hz', '- Configurado todas as 4 empresas no uninfe novamente com seus certificados digitais, testado um a um todos deram certo, feito 1 nota para cada empresa todas autorizadas Ok \r\n- Fixado IP na estacao PV que tem WIFI, devido conflito com outras maquinas \r\n- Configurado PV, copiado pasta sisauto para local, devido erro PATH NOT FOUND', '2', '01:30:00', 'd7a563e77e0600cbc05bca57e2e57155'),
(573, 163, 2, 'Erro emitir nfe, certificado vencido', '2017-05-17', NULL, '', 'Configurado novo certificado digital no uninfe\r\nconsultas ok, reenviado nota pendente e autorizada', '2', '00:10:00', 'b77a364b7375f38021e5c1d0a87cb37a'),
(574, 72, 2, 'Atualizar checkout, deletar registros tbregfiscoreducao', '2017-05-17', NULL, '', 'Atualizado checkout, deletado registros da tabela OK', '2', '00:10:00', '941d7136b50f17fb33590cb23175c3fc'),
(575, 63, 2, 'Email ferplax@ferplax nao envia sÃ³ recebe', '2017-05-17', NULL, '', 'Alterado config das portas do email\r\nporta pop para 110\r\nporta smtp para 587 \r\nteste de envio/recebimento\r\nesta OK', '2', '00:10:00', '5577a356b40a710313f8bc8ed0c82d3b'),
(576, 148, 2, 'baixar certificado digital', '2017-05-17', '2017-05-17', 'NÃºmero de ReferÃªncia do Pedido: \r\n13906742\r\nCÃ³digo de Acesso: \r\nef02de00 \r\n\r\nsenha melao123\r\nbaixar do site e colocar na pasta daianacertificados que vencem 2018', 'baixado OK', '2', '00:10:00', 'c9390f0e151e2449f21689f35a5b01a1'),
(577, 40, 2, 'baixar certificado digital', '2017-05-17', '2017-05-17', 'NÃºmero de ReferÃªncia do Pedido: \r\n13907517\r\nCÃ³digo de Acesso: \r\n5c87f1b3 \r\nsenha melao123	\r\n', 'baixado OK', '2', '00:10:00', '0c96ff01751542d944395f16c0244221'),
(578, 40, 2, 'marmoraria baixar cert', '2017-05-17', '2017-05-17', 'NÃºmero de ReferÃªncia do Pedido: \r\n13907472\r\nCÃ³digo de Acesso: \r\n39f873dd \r\nsenha melao123\r\n\r\nmarmoraria lindemberg', 'baixado OK', '2', '00:10:00', 'caa0e55935ae27d9e011014fe1f58aba'),
(579, 79, 2, 'zeragem', '2017-05-17', NULL, 'trouxe servidor na support- verificado e ligado micro na bancada - tudo ok - config pen drive - ok - 45min,', '', '2', '00:00:00', '24c58cd46dfcfdf88d0ba1026a05464f'),
(580, 81, 2, 'ERRO FORBIDEN', '2017-05-17', '2017-05-18', 'ERRO FORBIDEN PROVIDENCIA\r\nFICOU NA TELA O ERRO\r\nAO CONSULTAR NOTA JA O OCORRE O ERRO\r\nNO GERENCIADOR\r\nDA PROVIDENCIA MAT CONSTRUCAO\r\n259396335\r\n- FICOU PRA VER INICIO DA TARDE', 'servidor recolhido para atualizacao de server 2003 ppara 2008', '2', '00:00:00', '1747e1f05e6398f3f816a376136025f5'),
(581, 80, 2, 'Impressora nao imprime', '2017-05-17', '2017-05-17', '', 'Deletado documentos da fila de impressao\r\nreiniciado servico spooler\r\nreiniciado servidor\r\ntestado impressao OK', '2', '00:10:00', '3ffe05ecc8f78557acf5b9ceebc8445b'),
(582, 32, 2, 'instalar checkout', '2017-05-17', '2017-05-19', '138864057   1e31rs   -  instalar pjcheckout - emitir memoria fiscal do mes de abril pra eles -  LMFC completa  - e desativar genus na parte de cupom fiscal pra nao emitirem pelo genus - nova senha colocada', 'sera aberto chamado com wle para verificar conexao da impressora com maquina\r\nou usb ou serial, assim que estiver ok continuara instalacao e config checkout', '2', '00:30:00', '2b09900ca2fd5a52ead5dafcb18df84d'),
(583, 40, 2, 'conexao inst cert digital', '2017-05-17', '2017-05-17', 'instalar certificado LB e marmoraria - config uninfe   lb\r\n366631421\r\n6069\r\n\r\n\r\n345288282\r\n43s1sq\r\nmarmoraria\r\n', 'Instalado nas duas maquinas ambos certificados\r\nconsultas nfe ok', '2', '00:20:00', 'a03626125d16b0808cbed6f8f2836e60'),
(584, 160, 2, 'inst sistema desempenho servidor', '2017-05-17', '2017-05-17', '279667226  134jir   -  qdo entra com o usuario e senha da pessoa que usa, nao esta dando certo - pediu pra avisar no team pra ele explicar o problema', 'Configurado DSN do sistema, pois ao acessar com outro usuario remoto\r\nnao conectava o ODBC\r\nestava somente configurado por usuario\r\n', '2', '00:15:00', 'beaeccc9cdb8794baf44a1b7373fd95d'),
(585, 41, 2, 'inst impressora compartilhada', '2017-05-17', '2017-05-17', '864363751  5ed36v    servidor  (compartilhar nova impressora que instalaram)                        estacao:140305666  mpq744   depois tera mais 2 estacoes pra compartilhar', 'Compartilhado e configurado driver nos mapeamentos das estacoes\r\npaginas de teste imprimindo OK', '2', '00:45:00', '4a53a5b52128ea408914f3e298bc0269'),
(586, 154, 4, 'config aliquotas', '2017-05-17', NULL, 'ensinado a alterar aliquotas cad tipo de produto- prod com subst. tributaria - 30min', '', '2', '00:00:00', 'fdec2c00c77c9665c8defc99d2a6d3cf'),
(587, 106, 2, 'nota imprimiu e nao trouxe xml', '2017-05-17', '2017-05-17', '801306567   nkw598        nf 1925  ', 'apenas consulta nfe e autorizada\r\npassado sobre conexao com internet, que esta causando a rejeicao da nfe', '2', '00:00:00', 'b6b0a97fc7df4812dced6e608dade2de'),
(588, 105, 2, 'CLiente zerou estoque de TODOS os produtos sem querer', '2017-05-17', '2017-05-18', '719686687\r\n123456', 'Deletado da tbentrnota\r\nos registros de ajustes, referentes ao dia 16/05\r\natualizado estoque em seguida\r\nprodutos voltaram para o estoque ok\r\nconfigurado copia diaria no micro ', '2', '00:30:00', 'aa062d8b9d723b4a976981249b064a60'),
(589, 50, 2, 'Mensagem de bloqueio por reducoes z pendentes checkout', '2017-05-18', NULL, '', 'Enviado exe atual do servidor, deletado registros\r\nda tabela regfiscoreducao\r\ncheckout ok', '2', '00:10:00', 'd0b2ff9e04b462a97aeee7fcc178b8ed'),
(590, 63, 2, 'Email nfe@metalplas nao acessa webmail', '2017-05-18', '2017-05-18', '', 'passaram nova senha incorretada para daiane\r\nrepassado senha correta testado email ok\r\nconfigurado para idioma portugues e assinatura ok', '2', '00:10:00', '41c0ddaf3fc10695d5bb9078186b1235'),
(591, 148, 2, 'instalar certificado digital', '2017-05-18', '2017-05-18', '369992919  3859  apos instalar pedir id e senha do outro micro de cte pra instalar tambem -   \r\n\r\n 530007343\r\n4j5tz3\r\nferromax estacao cte\r\n', 'Instalado e configurado em ambas as maquinas\r\nconsultas OK', '2', '00:20:00', '7d3389e56be9b3fd5706c864cb8f9176'),
(593, 72, 2, 'instalar certificado digital', '2017-05-18', '2017-05-18', 'CEFEQ\r\nSENHA 1234\r\nCERTIFICADO NA AREA DE TRABALHO\r\n906275596\r\n2niw95\r\ninstalar e trazer copia pra servidor support\r\n', 'instalado e configurado OK\r\ncopia no servidor', '2', '00:10:00', '7717dc30a34aee900adb9bfd02c5cd25'),
(594, 72, 2, 'at checkout', '2017-05-19', '2017-05-22', '906275596  atualizar checkout  - atualizar o certificado digital na pasta -    no  configdes do cliente mudar o campo CREDENCIAMENTO para o valor  351008400000249   -  ', 'atualizado checkout e certificado digital novo em acbr_envioxml\r\nalterado credencimaneto OK', '2', '00:15:00', '02b9ea643e7cc3dc3d118a0aeb724b06'),
(595, 110, 2, 'configdes', '2017-05-19', '2017-05-19', '906997487\r\n9vwy31\r\nerro configdes ecf- corrigir pra entrar no checkout\r\ne ajeitar item\r\ncoxao mole do estoque \r\nfoi dado entrada em quantidade acima do correto-editar a nota e colocar a quant correta\r\n', 'detectado ecf configdes OK\r\ndeletado relacionamento do item \r\ndado entrada novamente, quantidade entrou OK', '2', '00:10:00', 'a1045ba9cfe65f0dc37da0e47ded1dbd'),
(596, 34, 2, 'alt rpt', '2017-05-22', '2017-05-22', 'rlpedidosOS4.rpt  - retirar a mensagem   \"e apoio tatico autorizado pela policia federal\"   -   ficara somente    \"mensais referente ao servico de monitoramento 24 horas\"     114386007   92rzi6', 'Alteracao OK\r\ndeixado copia no servidor pasta personalizacoes', '2', '00:15:00', '78e4015fcc8c07151ecff5b120689c62'),
(597, 116, 2, 'Informar codigo do pedido na nota para madem', '2017-05-22', NULL, '', 'Atualizado sisauto \r\nhabiltado config-fiscal usar xped e itemped\r\nmostrado opcao em cad. cliente para usar quando precisar informar os dados do pedido\r\nfeito nfe de teste para conferencia da informacao saiu OK\r\n', '2', '00:25:00', 'aacbe4a61fa3d3e447777ec40805a265'),
(598, 164, 4, 'ERRO AO ACIONAR SIST', '2017-05-22', '2017-05-22', '489551058   59Q2DV   SISTEMA CLIPPER -ERRO AO ACIONAR-enviado novo exe ok', '', '2', '00:00:00', '3ac1a07188ad84da90303b3c58e03195'),
(599, 110, 2, 'erro data estoque ecf', '2017-05-22', '2017-05-22', 'erro checkout ao acionar   906997487  364jye', 'detectado ecf configdes OK\r\ntestado acesso OK', '2', '00:10:00', '13b700fee03954e0d25902469a57e3c1'),
(600, 72, 2, 'at cert digital', '2017-05-22', '2017-05-22', 'cefeq\r\n906275596\r\natualizar cert digital\r\nna acbrenvioxml', 'certificado digital esta ok\r\natualizado pasta acbrenvio_xml ok\r\ndeletado registros tbregfiscoreducao ok\r\natualizado checkout ok', '2', '00:25:00', 'd9598ea43ad1808c5ecba03c402f72f3'),
(601, 80, 2, 'Impressora nao imprime', '2017-05-22', NULL, '', 'Mapeado novamente impressora na rede\r\npor nome do micro testado impressao ok', '2', '00:10:00', '81f9c0156fe68665ff0d0f10e611450b'),
(602, 88, 2, 'Produtos com mesma descricao puxando valor errado no pedido', '2017-05-22', '2017-05-22', '', 'Atualizado sisauto, mas nao deu para testar se resolveu pois\r\nPriscilla passou recado que nao quer atualize mais o sisauto', '2', '00:00:00', 'ecc91bb34a4a578a4e197d5a964436c8'),
(603, 32, 2, 'inst ecf bematech cabo usb', '2017-05-22', '2017-05-22', 'vidracaria sampaio\r\nconectar ecf  usb\r\n138864057\r\nb54zf2\r\n', 'Instalado driver usb impressora 2100th\r\nimpresso lx e lmf ok\r\nverificar vendas amanha', '2', '00:25:00', '53fdc8e58bcd554f12f840368a5e6516'),
(604, 127, 2, 'Inutilizacao nfe', '2017-05-23', NULL, '', 'Inutilizado nfe 11320 e 11321 ok', '2', '00:10:00', 'dd6d25059b50344e41c9abcd74ad8c09'),
(605, 88, 4, 'estoque negativo', '2017-05-23', '2017-05-23', '145564424\r\n123456', '', '2', '00:00:00', '536cca68bc295a16482dc4f6b3476f87'),
(606, 72, 4, 'icone remoto9', '2017-05-23', NULL, 'config icone remoto9 -havia sido desconfigurado-20 min', '', '2', '00:00:00', '3a14b9e82db556ac2e7aebab99e29ffa'),
(607, 89, 4, 'erro vlr nf devolucao', '2017-05-23', NULL, 'erro valor devolucao -nota do fornecedor(da nt) ', '', '2', '00:00:00', '283873109076c06b5c7fad0de0339c14'),
(608, 0, 4, 'erro ao emitir nfe', '2017-05-23', NULL, 'nfe devolucao - correcao valor outras despesas - 0,01 de diferenca - ajuste em txt e geracao nfe - 30 min', '', '2', '00:00:00', '27556b5552684ea2950c6cd92c24c00a'),
(610, 81, 2, 'erro checkout sisauto', '2017-05-23', '2017-05-24', 'providencia\r\n304446360\r\newc997\r\nver icone de mapeamento pro checkout\r\nexecutar reparador sisauto e deixar icone na tela-atualizar sisauto tambem em todas as empresas\r\ne deixar icone de mapeamento checkout e reparadores na tela\r\n\r\n-testar importar os cupons tambem', 'reparado todas as bases\r\natualizado sisauto de todas as empresas\r\ncriado atalho reparador e atalho mapear checkout\r\ntestado importacao cupons, normal ok', '2', '00:30:00', '586b50ffae9d88e9e23f2120fef45b19'),
(611, 77, 4, 'importacao cupom', '2017-05-23', '2017-05-24', 'importar cupons do checkout - verificar o cupom com erro - gerar espelho da mfd pra localizar os produtos pra poder importar - cadastrando como teste se preciso', '', '2', '00:00:00', '12ba807dcb05246f399e65fea34a2387'),
(612, 44, 2, 'Erro transmitir cte, chave nfe referenciada nenhum valor informado', '2017-05-24', NULL, '', 'Verificado aba documentos, foi inserido uma linha em branco\r\ndeletado registro, salvo cte e enviado novamente\r\nautorizou ok', '2', '00:10:00', '044d539d61c6fa8f3a415b7ea661919b'),
(613, 74, 4, 'Pedido com clientes trocados', '2017-05-24', '2017-05-25', 'Pedido 20324 - deveria ser DENISE ALVEZ\r\nPedido 20324 - puxou MELINA ALENCAR\r\n\r\ne no carnÃª saiu certo, e nos titulos a receber nao aparece o correto', 'nao deu para identificar o motivo exato do problema\r\nse foi por editar o pedido e trocar o cliente...\r\n\r\njean ira ligar e explicar sobre situacao', '2', '00:00:00', '2c6974a92b11671268dcd8387d663a0b'),
(614, 63, 2, 'Nfe validada, IE nao vinculada ao CNPJ', '2017-05-24', '2017-05-24', '', 'Deixado cliente como ISENTO, e marcado como nao contribuinte\r\nenviado nota novamente e autorizada\r\npendente verificar sobre msgbox do gernfe', '2', '00:10:00', '9dc9fc36d630dae460d82a149514fd61'),
(615, 77, 2, 'Inconsistencia ao importar cupom', '2017-05-24', '2017-05-24', '', 'atualizado sisauto nas estacoes, recolhido base para atualizar local devido automation error no bdupdate2\r\natualizado checkout\r\nimportado registros do paf 200 registros importados com sucesso OK', '2', '00:30:00', '83a7388becfa2c4bd4a0fd501f6eed82');
INSERT INTO `chamados` (`id`, `id_cliente`, `id_usuario`, `problema`, `data`, `data_modif`, `conexao`, `solucao`, `status`, `tempo`, `anexo`) VALUES
(616, 102, 2, 'atualizar sisauto e bdupdate2 ', '2017-05-24', '2017-05-24', '729562979 - atualizar sisauto e conferir os backups se estao ok - avisar pra ela testar os relatorios', 'backup OK\r\natualizado sisauto e rpts\r\npedido para testar, cliente ira testar com vendas que ira fazer, ai avisa', '2', '00:10:00', '9c716297068970049029ce5b50f61ab2'),
(617, 80, 2, 'Reinstalar Sistema, maquina formatada', '2017-05-24', NULL, '', 'Pastas ok\r\nconfigurado uninfe e acbr inicializacao\r\nnewpack marcelo e dlls ok\r\nconsultas notas e sistema OK', '2', '00:30:00', 'd87db02bfd980fe40d36f4897b30e5df'),
(618, 50, 2, 'Alteracao RPT orcamento ', '2017-05-24', '2017-05-25', 'Alterado rpt novamente conforme modelo passado que era usado la esta ok\r\nfalta apenas enviar para cliente', 'Enviado rpt OK para cliente\r\ntestado e conferido ficou rodando', '2', '00:15:00', 'e0bcb2c6c1c9832fb8837148bcc84ab7'),
(619, 150, 2, 'Orcamento duplo na folha encavalando informacoes', '2017-05-25', '2017-05-29', 'Alterado para rpt rlorcacompleto2\r\npara cliente poder enviar o orcamento\r\naguarda correcao', 'atualizado sisauto e rpts\r\ntestado impressao orcamentos\r\ne reimpressao OK', '2', '00:20:00', '5b334b92a5ab92da407b5407c7bfaa46'),
(620, 114, 2, 'devolucao de item de cupom', '2017-05-25', '2017-05-25', 'mapele\r\ndevolucao de mercadoria -mostra a rotina de devolucao do cliente pra loja\r\n705823259\r\n4bes93\r\n', 'Usado dados da angelita para gerar nfe\r\nja que nao possuia os dados do cliente\r\nautorizado nota ok', '2', '00:20:00', '2a8bf975d0d573e1b3c8daae7a4e43fe'),
(621, 127, 2, 'inutilizar nfe 11204', '2017-05-25', '2017-05-25', 'inutilizar nfe 11204 - 163857960', 'Nota do dia 24-04 \r\nja estava cancelada mas nao consta no gernfe\r\ncorrigido manualmente no gernfe.mdb OK\r\n', '2', '00:25:00', '9b286eddd713b4fc0be850a20a8db451'),
(622, 165, 2, 'ENVIAR CERT DIGITAL', '2017-05-25', '2017-05-25', 'rnscontabilidade@uol.com.br\r\nibsen\r\n602062159\r\n1v2wt3\r\ntrazer certificado digital pro servidor\r\ne enviar pro email da contabilidade acima - DEIXAR AVISADO NA TELA PRA ELE QUE FOI OK\r\n\r\n', 'Enviado para email ok', '2', '00:00:00', '68dc137676d8ac9529b1dd18a15e8e38'),
(623, 81, 2, 'at sisauto', '2017-05-25', '2017-05-25', 'PROVIDENCIA\r\n304446360\r\n543vxt\r\natualizar sisauto e bdupdate2\r\nao emitir nota eletronica ira sugerir 1 na quant do transporte\r\nautomaticamente\r\n', 'atualizado sisauto novamente\r\npara as 4 empresas\r\ndeixado recado para usar', '2', '00:20:00', '3ee7928b63d37ebf33e07064d100869d'),
(624, 155, 2, 'instalar boleto sicoob', '2017-05-25', '2017-05-25', '564153114\r\nugu378\r\nreiturbo-trazer base sisauto e pasta de boletos(deve ser a c:cobranca)\r\natualmente fazem boleto pelo itau\r\nao emitir nota responde Sim na pergunta - (ate esta vindo 2 vezes a pergunta)\r\ndai ele responde sim pras duas\r\n-quando precisa reemitir boleto-esta indo em titulos a receber- no menu\r\nSICOOB - pra dai gerar o boleto - pelo menu itau esta dando erro(deve estar habilitado acbr talvez ja)\r\n-trazer a base deles pra testarmos a questao boleto sicoob - pra ele poder usar um ou outro \r\n', 'Recolhido bases para servidor', '2', '00:25:00', '735d8342c37833517804b3377639d3db'),
(626, 54, 2, 'NFce nao autoriza, tolerancia 5minutos', '2017-05-26', NULL, '', 'Consultado servicos acbr\r\nusado acbr emitir nfce, enviado 2 orcamentos ambas com nfce autorizaram ok', '2', '00:10:00', 'cb945ad0bf9b7aff8cf2499b2e116ba3'),
(627, 81, 2, 'Sisauto travando na entrada ', '2017-05-26', '2017-05-30', '259396335\r\n123456\r\n\r\nem movimentacao -> entrada de notas\r\n', 'atualizado sisauto e bdupdate2 \r\nprovidencia', '2', '00:15:00', '32a5cdfbc4c2d528a33712320ac4b3e2'),
(628, 73, 2, 'Copia bdmarc para atalho etiquetas', '2017-05-26', NULL, '', 'Corrigido arquivo .bat\r\npara mapear a unidade antes de fazera copia\r\nmapeamento por nome do servidor + user e senha\r\ntestado ficou ok, avisado na tela para usar', '2', '00:15:00', '9a0a464bf5c244cf1b2252f70232da49'),
(629, 148, 2, 'ENVIAR CERT DIGITAL', '2017-05-26', '2017-05-26', 'sercofu@hotmail.com\r\na-c daiane\r\npassar certificado da ferromax pra eles da contabilidade\r\n', 'Enviado por email ok', '2', '00:00:00', 'e7defe99afe979a3e4a081ec8f1a1370'),
(630, 50, 2, 'erro checkout', '2017-05-26', '2017-05-26', 'cofepar\r\n338664748\r\n6105\r\nconfigdes na area de trabalho\r\nerro ao entrar no checkout\r\n', 'Criado atalho configdes na area de trabalho\r\nusado senha 1\r\nrecriado OK\r\npassado para cliente\r\ncheckout rodando OK', '2', '00:10:00', 'e7a47e4e4f4a1b0400b5e358b6bf190c'),
(631, 1, 2, 'rocar veiculos-copiar base', '2017-05-26', '2017-05-26', '352423338  trazer pasta admdados  para buscar clientes e produtos   - programa usuario 1  senha: senha', 'Dados sistema no servidor\r\nclientes e produtos', '2', '00:00:00', '5046ca9ddce392b9335895ee979221ef'),
(634, 21, 2, 'inst etiquetas argox', '2017-05-26', '2017-05-26', '239664382  bb2e34', 'Configurado SISAUTO ETIQUETAS\r\ne COPIA DADOS ETIQUETAS.bat\r\n\r\ntestado impressoes de etiquetas esta ok\r\nso ver fonte da descricao do produto na etiqueta, que achou que ficou pequena\r\n', '2', '00:30:00', 'b3f9679c694e814bf43396e534bc0ca3'),
(635, 42, 2, 'Erro ao validar nota, comunicacao com webservice.xml', '2017-05-29', NULL, '', 'Atualizado uninfe e schemas \r\ndeletado arquivos da pasta erro e retorno do uninfe\r\nreenviado nota e autorizada ok', '2', '00:15:00', 'c29f26687f069bd416cccc1cd4860df6'),
(636, 73, 2, 'Nota devolucao com icms', '2017-05-29', NULL, '', 'Feito nfe com cliente, destacando icms, e valor de ipi no campo outras despesas\r\nnota autorizada OK', '2', '00:15:00', 'eaa51739bae8ad728789e0d0878ab8d3'),
(637, 166, 2, 'erro sisauto ao entrar', '2017-05-29', '2017-05-29', 'erro ao acionar sisauto  ziemmer motores\r\n966832641\r\n4603\r\nsisauto dando erro ao entrar\r\n(ver se corrompeu base de dados)\r\n', 'Erro Classe nao registrada\r\nenviado newpack marcelo\r\nregistrado dlls \r\ntestado sisauto OK', '2', '00:20:00', 'd70f306537eaae834a40b949b24d63a5'),
(638, 73, 4, 'ver backup rede e servidor', '2017-05-29', '2017-05-29', 'molasul\r\n321308125\r\n32kky8\r\nverificar backup hd e nuvem\r\n(jean verifica)\r\n\r\n', 'verificado ok - pasta google drive e extraido bdmarc.mdb e .ldb pra pasta bkp pra ver se abre ok - abriu ok - tabela tbprod ok', '2', '00:00:00', '3ca1ff05e47d04e982c1c4f12d423259'),
(639, 55, 2, 'erro estacao', '2017-05-29', '2017-05-29', 'tonho\r\n913138155\r\n1dt61n\r\nmensagem de windows nao original\r\nverificar\r\ne nao estava entrado-entrou de repente dai\r\n', 'usado windows loader\r\ne chew-vga \r\npara remover msg windows OK', '2', '00:20:00', 'ae1b22d1f487332c1e230fc5c62bc2be'),
(640, 107, 2, 'erro ecf ao inicializar', '2017-05-29', '2017-05-29', 'nortevet\r\n526785818\r\njei433\r\nerro ao entrar no checkout\r\nao acionar a impressora fiscal\r\n', 'detectado ecf configdes OK\r\n', '2', '00:10:00', '1fd3699bd1b26b76c84005a973097ca9'),
(641, 63, 2, 'Nota devolucao validada', '2017-05-29', NULL, '', 'Informado CFOP invalido para devolucao\r\ncorrigidio cfop aba produtos para 6201\r\nautorizada ok', '2', '00:10:00', 'a6c2185ada2c727873c78670b2f581ee'),
(642, 99, 2, 'instalar certificado digital', '2017-05-29', '2017-05-29', '376610876\r\n796krw\r\npedro peters\r\nfiscal6@peschelcontabilidade.com.br\r\nirao nos mandar certificado digital\r\nfoi no email welisson encaminhado', 'Instalado e testado consultas ok', '2', '00:10:00', 'e010749cf73a0d7a01c7314fdd06baa7'),
(643, 63, 2, 'retirar promocao do site', '2017-05-29', '2017-05-29', 'metalplas e ferplax-retirar promocao', 'Retirado banner ok', '2', '00:15:00', '9c8be6f3ee8dcabb5194c6e96f9a9501'),
(644, 53, 2, 'Erro ao atualizar estoque', '2017-05-30', '2017-05-30', 'ao atualizar estoque, bdmarc aumentando de tamanho\r\natÃ© chegar 1GB, e retorna erro que atingiu o tamanho maximo', 'Reparado base de dados\r\ncopiado para nosso servidor e copia no cliente\r\ntestado com base local mesmo problema, em seguida testado local com sisauto e bdupdate2 novo, rodou ok\r\natualizado no cliente, e rodou ok tambem', '2', '00:25:00', 'f492ec00fb9bbd8bad72bcda73d6b82b'),
(645, 77, 2, 'Pedidos lancados ontem 29-05 puxaram com data 26-05(sisauto consultas)', '2017-05-30', '2017-05-30', 'Pedido 32019 atÃ© 32024\r\nforam lancados com data 26-05 e confirmados tambem\r\nmas foram gerados ontem dia 29-05', 'ficou de ver daqui pra frente', '2', '00:00:00', 'f4bc509260fddbbece076155bacc65cf'),
(646, 120, 2, 'inutilizar nfe ', '2017-05-30', '2017-05-30', 'relvado\r\nnotas 4077 e 4078\r\ninutilizar\r\n956028371\r\n4ij48j\r\n', 'inutilizado numeracoes ok', '2', '00:05:00', 'dad90afa403ace5bf54d37a1d981d12a'),
(647, 105, 2, 'inst impressora', '2017-05-30', '2017-05-30', 'indianara\r\n719686687\r\ngz821f\r\nimpressora desconectada e ao conectar\r\nnao imprime a nfce\r\nimpressora pequena\r\n', 'Alterado porta COM config do acbr escpos\r\nimpressoes ok', '2', '00:10:00', 'b06892eb58bb8c3a4aef13109048030c'),
(649, 1, 2, 'atualizar sisauto e bdupdate2 ', '2017-05-30', '2017-05-30', 'canoinhas diesel\r\n202912234\r\n9r46aq\r\n-enviar novo rpt relatorio da public\r\nrelatorio-produtos-todos\r\nja no visualizar sai apagado os dados\r\nfazer relatorio sair por secao\r\ne organizado por codigo\r\nfazer pergunta por secao\r\n- feito rotina e agora ao gerar pede ordenamento e modelo - escolher modelo 2 negrito\r\ne ordenamento codigo\r\n', 'Atualizado sisauto bdupdate2\r\nmais rpt\r\ntestado impressao OK', '2', '00:15:00', 'b7d7b29cf13153c9dd70954619c252d7'),
(651, 134, 2, 'atualizar sisauto e bdupdate2 ', '2017-05-30', '2017-05-30', 'kerico\r\n762820960\r\n2169\r\natualizar sisauto\r\nmandar os arquivos primeiro\r\ne dai avisar ele pelo team\r\npra ele fechar o programa em todas as estacoes\r\natualizar kerico e stock\r\n', 'Atualizado sisauto e bdupdate2\r\nambas empresas KERICO E STOCK', '2', '00:20:00', 'a599dc06608bcd90b5c1875e80ee016b'),
(652, 141, 2, 'Nota devolucao', '2017-05-31', NULL, '', 'Emitido nfe com cliente e finalizado autorizacao devido erros do uninfe com certificado', '2', '00:15:00', 'a7c239d55fbba9c56c4ae960b1916ea6'),
(653, 148, 2, 'cert digital', '2017-05-31', '2017-05-31', '530007343\r\nqjz674\r\ncertificado digital ferromax\r\ninstalar pro cte\r\nela foi emitir e deu erro\r\n', 'alterado configuracao do certificado no UNICTE que esta configurado para cte\r\ntestado OK', '2', '00:05:00', 'ade98c2f76d38cc5d534a406125d5e4e'),
(654, 73, 2, 'Impressora hp 1125 nao imprime na rede', '2017-05-31', NULL, '', 'Deletado mapeamento na estacao\r\nreiniciado spooler na estacao e servidor\r\nreiniciado maquinas\r\nmapeado novamente\r\ntestado impressoes ok', '2', '00:15:00', '62e72154da59448495e7914b1d5656a4'),
(655, 81, 2, 'Registros paf ecf 05-17', '2017-06-01', NULL, '', 'Gerado arquivo, salvo na area de trabalho\r\nconferencia do arquivo sem caracter ? ok', '2', '00:15:00', 'eca4290dd62a48d1ea3c6895acc2a519'),
(656, 81, 2, 'Erro ao validar nota, informado anomes 1705 na chave da nota', '2017-06-01', NULL, '', 'Verificado problema, a nota foi feita com data 010617 mesmo mas estava acusando o erro do mesmo jeito, verificado fuso horario da maquina estava errado\r\nalterado para padrao brasilia, e atualizado hora pela internet\r\nfeito logoff no servidor\r\nreenviado nota e autorizada ok', '2', '00:10:00', 'feda02770780d1c8b183eb7b55894a08'),
(657, 114, 2, 'Erro de privacidade ao acessar portal da receita', '2017-06-01', NULL, '', 'Mostrado opcao para continuar usando o site, clicando no avancado\r\ntestado com cliente ok', '2', '00:10:00', '5e08a48449f3d6cbb36876b4b9282e75'),
(658, 84, 4, 'arquivos contabilidade', '2017-06-01', '2017-06-01', '824107951    dgk631', 'Gerado arquivos NFE E NFCE\r\nenviado email support@', '2', '00:35:00', '072312a3a9af853c4f1cb2e572935fb2'),
(659, 83, 4, 'arquivos contabilidade', '2017-06-01', '2017-06-02', '', 'gerado arquivos e enviado no email support@', '2', '00:30:00', '1fdc4166aa2a0ac265e15e7632038434'),
(660, 79, 4, 'arquivos contabilidade', '2017-06-01', '2017-06-01', '', 'gerado arquivos e passado no email support@', '2', '00:20:00', '91541f14067369bfca6c60d24d7a936f'),
(661, 35, 4, 'arquivos contabilidade', '2017-06-01', '2017-06-02', '', 'gerado arquivos enviado no email support@', '2', '00:15:00', '38e8a6879a1fb0398c4efd3dec6a57f4'),
(662, 88, 4, 'arquivos contabilidade', '2017-06-01', '2017-06-05', '', 'Gerado arquivos enviado no email support@', '2', '00:25:00', '3ac205e6f6836daba664418114756123'),
(663, 90, 4, 'arquivos contabilidade', '2017-06-01', '2017-06-05', '', 'Gerado arquivos enviado no email support@', '2', '00:25:00', '6d1133c437e5ab0faabcbd8e21359987'),
(664, 87, 4, 'arquivos contabilidade', '2017-06-01', '2017-06-05', '', 'Gerado arquivos e enviado no email support@', '2', '00:10:00', '741fe9b3b17a3208fa9f029e703b2f15'),
(665, 50, 2, 'reg paf ecf', '2017-06-01', '2017-06-01', 'registros paf-ecf mes de maio - verificar ? -  enviar pra email support@   411059238  9p22uw', 'OK gerado e enviado no email', '2', '00:00:00', 'cd230aa94241b3ccff90b498c7de535a'),
(666, 135, 2, 'reg paf ecf', '2017-06-01', '2017-06-01', 'reg paf-ecf - enviar pra support@  321184082  u93ab4', 'OK gerado e enviado', '2', '00:00:00', '5d2754c222d5e2122e3dbec367c43498'),
(667, 1, 2, 'luteranos-email', '2017-06-01', '2017-06-01', '274 835 525    1443  -  erro ao carregar LIVE MAIL  -  trava ao chamar - ver se ha reparador pra LIVE MAIL', 'Reparado aplicativos do windows live essentials\r\nreiniciado maquina, iniciado live mail\r\ncarregou emails da caixa de entrada a principio tudo ok', '2', '00:20:00', 'e524d4692dc8dd858af5b1e3f91859c1'),
(668, 86, 2, 'gerar nfce arquivos', '2017-06-01', '2017-06-01', 'tozo\r\n730272485\r\n4bkr44\r\ngerar arquivos nfce\r\n', '', '2', '00:20:00', 'eb1f9d9a9b140653ebeac8bcfb773398'),
(669, 158, 2, 'sintegra checkout', '2017-06-01', '2017-06-01', 'gerar sintegra checkout pra contabilidade - 795773256  432bwx - enviar pra email support@', 'Gerado e enviado email OK', '2', '00:10:00', '0e7669a3c69552dec60f98d73ca66e35'),
(670, 116, 2, 'gerar reg paf ecf', '2017-06-01', '2017-06-01', 'lideranca\r\n495465945\r\n9m6zk9\r\nemerson@universalcontabilidadesc.com.br\r\nenviar arquivo registros paf-ecf\r\nou deixar na area de trabalho pra ela enviar\r\n', 'Gerado arquivo de registro salvo na area de trabalho para cliente enviar', '2', '00:25:00', '80afdc9c51bd6fca356c17a965938d59'),
(671, 49, 2, 'erro sisauto', '2017-06-01', '2017-06-01', 'trevo\r\n720460957\r\nutw841\r\nerro ao acionar sisauto\r\n', 'Reparado base de dados\r\nrodado bdupdate2 \r\ntestado acesso ok\r\nsalvo arquivos nfce na area de trabalho ok', '2', '00:15:00', '242d78bc68f7a54a8d52c6455015b2c8'),
(672, 58, 2, 'Encerrar mdfe', '2017-06-01', NULL, '', 'Encerrado mdfe e emitido novo ok', '2', '00:10:00', '49c94d9cc19154cf041e470030a7499e'),
(673, 44, 2, 'email xml erro', '2017-06-01', '2017-06-02', 'habil transportes\r\nerro unable to send\r\nao enviar email do cte\r\n607119546\r\ngenus', 'Testado envio de emails\r\nretornando ENVIADO COM SUCESSO', '2', '00:00:00', '21bdf217ec151ec1b63a78b412187833'),
(674, 104, 2, 'Nota autorizada mas nao mostra no gernfe', '2017-06-02', NULL, '', 'Cadastrado nota no gernfe.mdb\r\nprovavelmente nao pegou retorno devido conexao com internet', '2', '00:10:00', 'c7cee6f2746e5a1a5c917e580b548a99'),
(675, 167, 2, 'inst genus', '2017-06-02', '2017-06-02', '352423338\r\nestacao nfce genus -  419898894   5283\r\nP8XLZSQIFO2EDQDKH3UEDKAIUUARF2ZVFTVA', 'Instalado GENUS ERP\r\nInstalado GENUS NFCE\r\nImportado dados de PRODUTOS, CLIENTES E FORNECEDORES\r\nInstalado driver Spooler MP4200\r\nAlterado ultima NFCE e testado emissao NFCE OK', '2', '02:30:00', '1bc05bd83eefb439743002540afd950d'),
(676, 85, 2, 'gerar nfce arquivos', '2017-06-02', '2017-06-02', '591633938\r\njx8c85\r\ngerar arquivos nfce pen drive- area de trabalho\r\navisar na tela quando pronto\r\notica marly\r\n', 'ARQUIVOS NO PENDRIVE OK', '2', '00:10:00', '152f5f97af874cc443639fb404278b57'),
(677, 50, 2, 'Deletar registros da reducao fisco', '2017-06-02', NULL, '', 'Deletado registros da tabela oK\r\nja estava na nona', '2', '00:10:00', '1aabfb2a346936972709aee3ac139efa'),
(678, 53, 2, 'Erro gerar relatorio saidas produtos', '2017-06-05', '2017-06-05', '', 'Verificado base de dados, tamnho exagerado com mais de 400MB\r\nreparado base foi para 54mb\r\nexecutado bdupdate2, testado relatorio em seguida\r\ngerou normalmente \r\nqualquer problema avisam', '2', '00:35:00', '2029c8b068962eceef5f30eac1c874df'),
(679, 165, 2, 'Certificado digital novo baixar', '2017-06-05', '2017-06-05', '', 'Baixado certificado digital novo\r\nusado cod emissao 1 e 2 \r\ndeixado copia no servidor, configurado no cliente OK', '2', '00:20:00', '1e2220a3e27f63d097c70e5b3eb9baab'),
(680, 99, 2, 'Erro validar cte', '2017-06-05', '2017-06-05', '', 'Verificado retorno, RNTRC NULL\r\neditado cte, cliente nao informou o veiculo\r\ncorrigido dados e enviado novamente, autorizou ok', '2', '00:10:00', '2c8eb62bbd7020dd1eb42b4a358cd70f'),
(681, 168, 2, 'alterar dados genus', '2017-06-05', '2017-06-05', 'no genus grams interiores - ajustar os dados GRAMS MAQUINAS EIRELI ME\r\ncnpj 21529846000193\r\nIE 257531467\r\ne atualizar genus - ele precisa emitir nota ate o meio-dia   432070662  h843gx', 'Alterado dados cadastrais da empresa\r\nInstalado sqlyog\r\nAlterado data atualizacao\r\nE atualizado genus\r\nqualquer duvida entra em contato', '2', '00:20:00', '37aecc004fcd36f2a7988aa69a5a0607'),
(682, 21, 2, 'AT SISAUTO ETIQUETAS', '2017-06-05', '2017-06-05', 'ATUALIZAR SISAUTO ETIQUETAS SOMENTE - QUESTAO PODER CITAR COD DE BARRAS NA ETIQ.  239664382  rs27v6', 'Atualizado sisauto + bdupdate2 ok', '2', '00:15:00', 'ca396b24fcc54edd08d5b028e7bf7433'),
(683, 127, 2, 'ajuste valor outras nf', '2017-06-05', '2017-06-05', 'nf 11365 santana - ajustar valor outras no total para 900,64-   374734090  jqr666', 'alterado no arquivo txt o valor\r\nnota autorizada ok', '2', '00:05:00', 'dfbb0ec0c1e527a7759a1691cb9fe356'),
(684, 72, 2, 'atualizar sisauto e bdupdate2 ', '2017-06-06', '2017-06-06', 'checkout tambem - enviar nova pasta acbrenvioxml que esta na public e configurar certificado digital deles - e tambem copiar a pasta acbrenvioxml pra dentro da pasta base1 ou marcelo\r\n\r\nmudar em utilit-config - titulos\r\nmodelo impressao recibo rapido pra 2\r\n-verificar primeiro se tem o totalizador carne (25)pela leitura x\r\n  ele so gera inicio do dia\r\nSE NAO TIVER- IR EM CONFIGURACOES DO CHECKOUT - CLICAR NO ICONE  PROGRAMAR TOTALIZADORES NAO FISCAIS e PROGRAMAR FORMAS DE PAGAMENTO', 'Nao tinha totalizador carne - programado formas de pagamento e totalizadores\r\nAlterado modelo recibo rapido - 2\r\nAtualizado e copiado pasta acbr_envio para - c:/basesisauto/base1\r\n', '2', '00:00:00', 'd5052dbae3106ff0b8964849990b2a41'),
(685, 137, 2, 'enviar sisauto', '2017-06-06', '2017-06-06', 'enviar pra gabriel sisauto e novo bdupdate2 -  mudar config 3243 pra N (avisar ele dai)', 'enviado', '2', '00:00:00', '35dc9bcf864282b17f5f1547dce65968'),
(686, 91, 2, 'erro captcha imagem genus', '2017-06-06', '2017-06-06', 'nossah\r\nimportar nota eletronica\r\n295397444\r\n4447\r\nerro na tela - \r\nao importar xml de nota de entrada\r\n', 'Restaurado definicoes do IE\r\nconfigurado opcoes avancadas \r\npara usar SSL e TLS, sem usar revogacao de certificados no servidor\r\ntestado baixando XML OK', '2', '00:10:00', '9393892708861a6021bfa92e7b6c6a30'),
(687, 101, 4, 'caljuris', '2017-06-06', '2017-06-06', 'koshinski\r\n585013287\r\n3ua23f\r\nerro ao iniciar maq virtual\r\n', 'resetado maq virtual ok', '2', '00:00:00', 'dd10a5a55c15b5af0041b015e8d78306'),
(688, 64, 2, 'icones media player', '2017-06-06', '2017-06-06', 'icones area de trabalho- chamando media player - verificar associacao .lnk - retirar media player - 134617345', 'Enviado arquivo .reg\r\npara recriar registros dos atalhos\r\ntodos voltaram ao normal ok', '2', '00:10:00', '4bc7a7f96139dbc11bb80b90b3a61b85'),
(689, 72, 2, 'erro ao gravar nfe', '2017-06-06', '2017-06-06', 'trazer base de dados sisauto  906275596', 'Reparado base de dados \r\ntestado emissao da ultima nota autorizou ok', '2', '00:10:00', '31f626c8774a83d26c9f2c80117a338e'),
(690, 134, 2, 'nova coluna rel pedido', '2017-06-06', '2017-06-06', 'kerico - adicionar coluna de unidade\r\n para relatorio de pedidos do kerico - ao gerar pedido nao sai coluna da unidade vendida\r\n06-06-17\r\ntrazer relatorio atual deles pra alterarmos nele\r\n762820960\r\n5492\r\n', 'Alterado rpt OK', '2', '00:40:00', 'c1cf333bdf1b422edb049b947b7a91ce'),
(691, 48, 2, 'arq contabilidade ', '2017-06-07', '2017-06-07', 'para cintia@universalcontabilidadesc.com.br -    752734465  cec434', 'gerado enviado support@', '2', '00:15:00', '8ab951b47617b3781d1cd36a3f80e1e9'),
(692, 152, 2, 'arq contabilidade ', '2017-06-07', '2017-06-07', '172029628  qp87k8  nfce pra email support@', 'Gerado enviado no email', '2', '00:10:00', 'f7b5085c5bdf2665720781940e3c3de7'),
(693, 167, 2, 'at genus', '2017-06-07', '2017-06-07', '352423338\r\ngb3v13\r\nrocar - atualizacao genus\r\n', 'Usado novo migrador.exe\r\natualizado clientes CPF\r\ncliente conferiu, cadastros a principio OK', '2', '00:00:00', 'aceb38054cb52782243f9041ce6de051'),
(694, 63, 2, 'Instalar Assinador CNPJ rumo all', '2017-06-07', '2017-06-07', '', 'Baixado instalador, feito instalacao e procedimentos para assinatura do contrato\r\nvalidou ok \r\n', '2', '00:10:00', '55aed41316f13587c3ecc5c077cffc88'),
(695, 141, 2, 'Nota devolucao', '2017-06-07', NULL, '', 'Finalizado nota pelo acbr devido erros do uninfe OK', '2', '00:10:00', '051b721be5f48adbc8937282284c091e');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `tipo_pessoa` char(1) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `telefone` varchar(15) NOT NULL,
  `cnpj` varchar(18) NOT NULL,
  `situacao` char(1) NOT NULL,
  `atualiza` char(1) NOT NULL DEFAULT 'N',
  `observacao` text,
  `contato` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `senhas` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `tipo_pessoa`, `nome`, `telefone`, `cnpj`, `situacao`, `atualiza`, `observacao`, `contato`, `email`, `senhas`) VALUES
(1, '1', 'Base De Conhecimento', '(00) 0000-0000', '000.000.000-00', '1', 'S', '', '', '', ''),
(19, '1', 'Wellisson Luiz Antunes RIbeiro', '(47) 3642-2100', '101.483.239-01', '1', 'N', 'Teste UPDATE OK', 'OK', 'welleh10@gmail.com', 'user: molasul2016\r\npass: molasul'),
(21, '2', 'Marcela - Mandirituba', '(41) 3633-1385', '04.923.341/0001-29', '1', 'N', '', '', '', 'backup3dalimenticios@gmail.com\nsupport@123    criado cobian e google no usuario administrador  13-04-17'),
(22, '2', 'Lany Alimentos/Eliane', '(47) 3642-6212', '23.253.195/0001-87', '2', 'N', NULL, NULL, NULL, NULL),
(23, '2', 'Nobre Color Tintas/Tobias', '(41) 3644-9089', '07.221.441/0001-62', '2', 'N', NULL, NULL, NULL, NULL),
(24, '2', 'Medicar/Cleonice', '(41) 3642-3736', '03.061.431/0001-56', '1', 'N', '', '', '', ''),
(25, '2', 'Bom Apetite/ Regina Dolinski ME', '(41) 3642-2379', '08.033.944/0001-77', '1', 'N', '', 'Marcos', '', ''),
(26, '2', 'Planauto GÃ¡s/Dorival', '(41) 3642-2839', '01.631.457/0002-48', '2', 'N', NULL, NULL, NULL, NULL),
(27, '2', 'Jose Valmir/Francisco', '(41) 3628-1340', '04.997.150/0001-72', '2', 'N', NULL, NULL, NULL, NULL),
(28, '2', 'Forest Green/Nourdes', '(41) 3628-1482', '09.501.256/0001-57', '2', 'N', NULL, NULL, NULL, NULL),
(29, '2', 'Renove/Herlon', '(47) 3643-0266', '12.531.995/0001-04', '2', 'N', NULL, NULL, NULL, NULL),
(30, '2', 'Tecset/Roberto', '(47) 3645-2814', '09.414.101/0001-83', '2', 'N', NULL, NULL, NULL, NULL),
(31, '2', 'AgropecuÃ¡ria Mafra/Carlos', '(47) 3642-2742', '75.275.511/0001-15', '2', 'N', NULL, NULL, NULL, NULL),
(32, '2', 'VidraÃ§aria Sampaio', '(47) 3642-7030', '17.495.864/0001-24', '1', 'N', NULL, NULL, NULL, NULL),
(33, '2', 'OrtoBrasil/Douglas', '(47) 3643-6953', '01.026.231/0001-37', '1', 'N', 'hortobrasil@terra.com.br', '', 'hortobrasil@terra.com.br', ''),
(34, '2', 'Adler SeguranÃ§a e VigilÃ¢ncia', '(47) 3642-7444', '00.000.000/0000-00', '1', 'N', NULL, NULL, NULL, NULL),
(35, '2', 'Aquarela Papelaria', '(47) 3642-0008', '10.949.967/0001-78', '1', 'N', 'mensal pra contab dalmir', '', '', ''),
(36, '2', 'Arena sports', '(47) 3642-7295', '23.473.034/0001-07', '2', 'N', NULL, NULL, NULL, NULL),
(37, '2', 'Bebidas Camponeiras/Alida', '(47) 3642-2074', '07.971.623/0001-50', '2', 'N', NULL, NULL, NULL, NULL),
(38, '2', 'Amazonia Balas', '(47) 3642-6704', '06.210.866/0001-03', '2', 'N', NULL, NULL, NULL, NULL),
(39, '2', 'Flaviano Multimarcas/flaviano', '(47) 3643-9999', '09.214.928/0001-43', '2', 'N', NULL, NULL, NULL, NULL),
(40, '2', 'Lb Solutions/Cleomara', '(47) 3645-3716', '07.964.793/0001-08', '1', 'N', NULL, NULL, NULL, NULL),
(41, '2', 'Contra Chama/Paulo', '(47) 3642-1164', '85.241.693/0001-67', '1', 'N', '', '', '', 'bkpcontrachama@gmail.com\r\nsupport@123     cobian e google no micro das notas em 13-04-17'),
(42, '2', 'El quÃ­mica/Elcir', '(47) 3645-2135', '81.747.313/0001-00', '1', 'N', NULL, NULL, NULL, NULL),
(43, '2', 'Rumasa materiais de construcao', '(47) 3645-0760', '85.481.620/0001-42', '1', 'N', '', 'daniel', '', 'bkprumasa@gmail.com\r\n\r\nsupport@123\r\n\r\ncobian e goog em 04-2017'),
(44, '2', 'Habil Transportes/Sandro', '(47) 3624-0491', '09.647.507/0001-06', '1', 'N', NULL, NULL, NULL, NULL),
(45, '2', 'Rede Rural', '(47) 3643-5189', '11.519.828/0001-77', '1', 'N', '', '', 'rederural@outlook.com', 'endereÃ§o: \r\nbkprederural1@gmail.com\r\nsenha:\r\njean19sms\r\ncobian e google drive 31-03-17'),
(46, '2', 'Idealle/Caio', '(47) 3642-2524', '15.464.968/0001-46', '1', 'N', NULL, NULL, NULL, NULL),
(47, '2', 'Desigual/AÃ©cio', '(47) 3642-0115', '10.820.393/0001-33', '1', 'N', NULL, NULL, NULL, NULL),
(48, '2', 'Otica Real - (Daise)', '(47) 3642-0870', '80.860.182/0001-00', '1', 'N', 'envio de xml nfe e nfce para Cintia da universal - cintia@universalcontabilidadesc.com.br', 'daise', '', ''),
(49, '2', 'Trevo Auto Center', '(47) 3645-0970', '05.158.615/0001-64', '1', 'N', '', '', '', ''),
(50, '2', 'Cofepar/Cornelio', '(47) 3643-7373', '06.205.337/0001-11', '1', 'N', '', 'Neto', 'Neto_Garcia@hotmail.com', 'Acesso remoto\r\nuser: administrador\r\npass: nova@123'),
(51, '2', 'Depaiva/Amilson', '(47) 3642-1620', '01.929.811/0001-34', '1', 'N', NULL, NULL, NULL, NULL),
(53, '2', 'Ari Dreher escamentos/Ari Ivan', '(47) 3622-7817', '20.277.252/0001-70', '1', 'N', NULL, NULL, NULL, NULL),
(54, '2', 'Inove tintas', '(47) 3643-6121', '19.908.725/0001-00', '1', 'N', NULL, NULL, NULL, NULL),
(55, '2', 'Tonho motos', '(47) 3645-0321', '03.397.248/0001-26', '1', 'N', '', 'claudiane', '', 'Google Drive\r\nuser: tonhomotocar@gmail.com\r\npass: notas2015'),
(56, '2', 'Madeireira Campo da lanÃ§a', '(47) 9189-6727', '20.622.267/0001-28', '1', 'N', '', 'Valmor', '', ''),
(57, '2', 'CelMotos', '(47) 3642-2566', '84.866.771/0001-56', '1', 'N', NULL, NULL, NULL, NULL),
(58, '2', 'Tertulia/Distribuidora de alimentos Augustini', '(47) 3643-6265', '05.269.772/0001-47', '1', 'N', NULL, NULL, NULL, NULL),
(59, '2', 'DVR drogaria/Dirceu', '(47) 3645-0559', '05.220.102/0001-36', '1', 'N', NULL, NULL, NULL, NULL),
(61, '2', 'Fundir/Patricia', '(47) 3645-0346', '40.323.339/0001-43', '1', 'N', NULL, NULL, NULL, NULL),
(62, '2', 'Boa costura', '(47) 3642-0282', '83.489.088/0001-84', '1', 'N', NULL, NULL, NULL, NULL),
(63, '2', 'MetalPlas ', '(47) 3642-8883', '06.127.888/0001-04', '1', 'N', '', '', '', 'Domain: metalplas.ind.br\r\nUserName: metal5\r\nPassWord: gat_tes1\r\n\r\nferplax\r\nDomain: ferplax.com.br\r\nUserName: fer450\r\nPassWord: nova899\r\n\r\ncontato@ferplax.com.br\r\nsenha: tes500\r\n\r\nemail: ferplax@ferplax.com.br\r\nsenha: fer600\r\n\r\nemail: nfe@ferplax.com.br\r\nsenha: efn700'),
(64, '2', 'Auto center Vila Nova', '(47) 3642-3456', '06.354.051/0001-06', '1', 'N', 'contato alcemir\r\nem 27-03-17 avisado pra iniciar boletos pelo acbr - sicoob - ira emitir e qualquer duvida ira nos ligar', '', '', 'autocenter vilanova\r\nConta Corrente: 100.955-9 / AUTO CENTER VILA NOVA LTDA ME\r\n\r\nBanco: 756\r\n\r\nAgÃªncia (Com DV): 3084-8\r\n\r\nCÃ³digo Carteira/Modalidade: 01 (SIMPLES COM REGISTRO)\r\n\r\nCÃ³digo Cedente (Cod. Cliente): 131857\r\n\r\nNosso Numero Inicial: 200001 (Segue em anexo a sequÃªncia de nosso\r\nnÃºmero a ser utilizada)\r\n\r\nConvÃªnio: 000000\r\n\r\nEmissor: CEDENTE\r\n\r\nDistribuiÃ§Ã£o: CEDENTE\r\n'),
(67, '2', 'Plastruck', '(47) 3643-6056', '09.196.188/0001-60', '1', 'N', '', '', '', NULL),
(68, '2', 'BoticÃ¡rio', '(47) 3642-2758', '10.841.438/0003-18', '1', 'N', '', 'Bruna', '', NULL),
(69, '2', 'Ideias Brasil', '(47) 3642-8042', '13.955.825/0001-01', '1', 'N', '', 'Antony ou Alisson', '', NULL),
(70, '2', '3D dist. produtos alimenticios', '(47) 3642-8118', '06.310.770/0001-17', '1', 'N', '', '', '', NULL),
(71, '2', 'Personalize', '(47) 3642-5601', '07.954.234/0001-17', '1', 'N', '', 'Herivelton  Lorena', '', NULL),
(72, '2', 'CEFEQ suprimentos industriais', '(47) 3645-3348', '09.248.043/0001-65', '1', 'S', '', 'Gerson Stochero Pacheco', '', ''),
(73, '2', 'Molasul', '(47) 3643-1400', '02.734.357/0001-29', '1', 'N', '', 'Sheisa', '', NULL),
(74, '2', 'Manias moda (lucimara)', '(47) 3642-7920', '06.350.864/0002-10', '1', 'N', 'contador 3642-2437 adriana - nilton brandt\r\nnanyadri@msn.com\r\n\r\nenviar todo mes arquivos nfce via email-support enviar', 'lucimara', '', 'Server\r\nID:836 610 578   \r\npass: 123456\r\n\r\nEstacao\r\nid : 823 109 643\r\npass :123456'),
(75, '2', 'Tomasoni Materiais de ConstruÃ§Ã£o', '(47) 3642-2234', '82.150.418/0001-40', '1', 'N', '', 'Marcos', '', ''),
(76, '2', 'Mano a Mano', '(41) 3623-1411', '02.152.534/0001-69', '1', 'N', '', '', '', ''),
(77, '2', 'New cosmeticos', '(47) 3025-6656', '12.212.436/0001-23', '1', 'N', '', 'Ivan', '', ''),
(78, '1', 'Moacir CTE', '(00) 0000-0000', '999.999.999-99', '1', 'N', '', 'Moacir', '', ''),
(79, '2', 'Box da XV', '(47) 9223-1712', '00.406.164/0001-13', '1', 'N', '', 'Tsuguio Marcio', '', 'WTS \r\nserver: 187.17.226.158:3389\r\nuser: support\r\npass: a123456@'),
(80, '2', 'Imac', '(47) 3643-1482', '05.353.914/0001-50', '1', 'N', '', 'Edson', 'imac.carreteis@hotmail.com', 'conta gdrive\r\ndriveimac2@gmail.com\r\nnotas2017'),
(81, '2', 'ProvidÃªncia materias de construÃ§Ã£o', '(47) 3643-0285', '04.780.475/0001-07', '1', 'N', '', 'AELCIO', '', 'Server novo --> providencia-srv\r\nadministrator\r\nadmin@123\r\n\r\nremoto1\r\na@1\r\n\r\nremoto2\r\na@2\r\n\r\nremoto3\r\na@3\r\n\r\nremoto4\r\na@4\r\n\r\nremoto5\r\na@5'),
(82, '2', 'Arte & Pava', '(47) 3642-2100', '00.100.000/0000-00', '1', 'N', '', '', '', ''),
(83, '2', 'Michele Materiais', '(47) 3642-0479', '03.251.668/0001-08', '1', 'N', '', 'Michele', '', ''),
(84, '2', 'Celi Materiais', '(47) 3645-0062', '02.477.809/0001-34', '1', 'N', '', 'Michel', '', ''),
(85, '2', 'Otica marly', '(47) 3642-1326', '14.562.601/0001-00', '1', 'N', '', 'PEDRO', 'otica_marly@hotmail.com', ''),
(86, '2', 'Mercado Toso', '(47) 3642-5459', '07.388.864/0001-71', '1', 'N', '', 'SANDRA OU BETO', '', ''),
(87, '2', 'Sorvete Mania', '(47) 3642-5768', '00.832.354/0002-83', '1', 'N', 'Mandar XML do mÃªs > acessar genus ERP com ID:g senha:1 > PAF - ECF > NFCE de consumidor > colocar o periodo > verificar se nÃ£o hÃ¡ faltantes > agora no genus NFCE > menu principal > gerente > digitar id e senha > pesquisar mÃªs > marcar todas > clicar em salvar > criar pasta com ANOMÃŠS NFCE> clicar em ok > Gerar relatÃ³rio pelo genus ERP > na mesma aba de NFCE de consumidor clicar em imprimir > salvar como pdf > colocar na pasta dos XMLs ', 'Gerson', '', ''),
(88, '2', 'Manias Kids', '(47) 3645-1357', '06.350.864/0001-10', '1', 'N', '', 'Priscilla', '', ''),
(89, '2', 'Madeireira Serpol', '(47) 3645-2614', '77.510.048/0001-74', '1', 'N', '', 'ROSANGELA', '', ''),
(90, '2', 'Kel cosmeticos', '(47) 3642-4700', '11.763.640/0001-70', '1', 'N', '', '4736424700', '', ''),
(91, '2', 'Nossah Auto PeÃ§as', '(47) 3645-2003', '19.829.549/0001-11', '1', 'N', '', '', '', ''),
(92, '2', 'Spolti transportes', '(47) 9932-2263', '13.852.010/0001-05', '1', 'N', '', 'FABRICIO', 'transportesspolti@gmail.com', ''),
(93, '2', 'Teoaldo Schroeder', '(47) 3642-2632', '83.612.853/0001-01', '1', 'N', '', '', '', ''),
(94, '2', 'LIMA E COLO TRANSPORTES', '(47) 3642-8964', '10.811.058/0001-79', '1', 'N', '', 'VALDIR( CONTADOR)92540271', '', ''),
(95, '2', 'Vitrine', '(47) 8426-7354', '20.777.537/0001-70', '1', 'N', '', 'Tatiane', '', ''),
(96, '2', 'Madelenha Transportes', '(47) 3642-0228', '95.192.001/0001-91', '1', 'N', '', 'CARLOS EDUARDO', '', 'backupmadelenha@gmail.com\r\nsupport@123  -  google drive em 11-04-17'),
(97, '2', 'VidraÃ§aria SÃ£o Pedro', '(47) 3642-2855', '02.126.391/0001-10', '1', 'N', '', 'Bruna', '', ''),
(98, '2', 'Daglog', '(99) 9999-9999', '22.995.952/0001-25', '1', 'N', '', 'Pietro', '', ''),
(99, '2', 'Pedro Peters', '(47) 3643-0035', '85.257.095/0001-86', '1', 'N', '', 'Pedro', 'pedropeters@outlook.com', ''),
(100, '2', 'Robetti', '(47) 3643-1619', '09.587.512/0001-70', '1', 'N', '', '(49) 3443-0017', '', 'Acesso Sisauto Web\r\nCNPJ: 09587512000170 - HomolagaÃ§Ã£o\r\nUser: admin\r\nPass: vanir@123'),
(101, '2', 'Koschinski Advocacia', '(47) 3642-0585', '01.873.658/0001-70', '1', 'N', '', '', '', ''),
(102, '2', 'Tockesch Moda Ãntima', '(47) 3643-6345', '04.915.436/0001-61', '1', 'N', '', 'Scheila', 'tockemodaintima@hotmail.com', 'bkptocke@gmail.com\r\nsupport@123\r\n\r\ngoogledrive'),
(103, '2', 'Cetec Eletro-Comercial', '(47) 3643-0332', '03.098.974/0001-48', '1', 'N', '', 'Nice', '', ''),
(104, '2', 'Mauro Fonseca', '(47) 3642-0437', '77.509.602/0001-01', '1', '1', '', '', '', ''),
(105, '2', 'Indianara Presentes', '(47) 3642-7340', '22.870.327/0001-57', '1', 'N', '', 'INDIA OU HELO', '', ''),
(106, '2', 'Tj AcessÃ³rios', '(47) 3645-0167', '02.800.240/0001-04', '1', 'N', '', 'ROSELI DA CONC.BAUMGARTNER TAB', '', ''),
(107, '2', 'Nortevet', '(47) 3642-7584', '18.563.284/0001-90', '2', '1', '', '', '', ''),
(108, '1', 'Denise dos Santos', '(47) 3642-2222', '135.925.340-00', '1', 'N', '', 'Denise', '', ''),
(109, '2', 'Borges e Borges', '(47) 3642-3265', '07.660.925/0001-08', '1', 'N', '', 'EDER', '', ''),
(110, '2', 'Max carnes', '(47) 3643-1031', '06.004.239/0001-16', '1', 'N', '', 'Juce', '', ''),
(111, '2', 'Rio Motoserras', '(47) 3645-0529', '00.856.946/0001-04', '1', 'N', '', 'Giuliano', '', 'Google Drive\r\nuser: nferiomotos@gmail.com\r\npass: notas2017'),
(112, '2', 'AlianÃ§a Tintas', '(47) 3652-1616', '08.171.658/0001-78', '1', 'N', '', 'Elezir', '', ''),
(113, '2', 'Kinder Rio Negro', '(47) 3645-0968', '75.192.898/0001-46', '1', 'N', 'wts instalado em 13-03-17 - senhas contem as senhas', 'Junior', '', 'WTS\r\nNome servidor: SERVER\r\nuser: administrator\r\npass: admin@123\r\nuser: remoto1\r\npass: a@1\r\nuser: remoto2\r\npass: a@2\r\nuser: remoto3\r\npass: a@3\r\nuser: remoto4\r\npass: a@4\r\nuser: remoto5\r\npass: a@5'),
(114, '2', 'Mapelle', '(47) 3642-1039', '85.376.846/0001-83', '1', 'N', '', 'Angelita', '', 'backupmapelle@gmail.com\r\nsupport@123'),
(115, '2', 'Wilson Veiculos ', '(47) 3642-1080', '01.332.481/0002-03', '1', 'N', '', 'Sidnei', '', 'bkpwveiculos@gmail.com\r\n\r\nsupport@123\r\ncob e goog em 04-2017'),
(116, '2', 'LideranÃ§a Tintas', '(47) 3642-0781', '82.174.186/0001-60', '1', 'N', '', 'Lisandro', '', ''),
(117, '2', 'Vidro-Car', '(47) 3645-3131', '09.263.671/0001-10', '1', 'N', '', 'LUCIANO TIBUSCKI', '', ''),
(118, '2', 'Itavel locadora de veÃ­culos', '(47) 3652-1757', '07.756.690/0001-52', '1', 'N', '', '', '', ''),
(119, '2', 'Montfer', '(47) 3642-6666', '08.939.197/0001-30', '1', 'N', '', 'Sonia', '', ''),
(120, '2', 'Relvado', '(47) 3628-1378', '02.367.200/0001-02', '1', 'N', '', 'Claudio', '', ''),
(121, '1', 'Nolli', '(47) 3642-4787', '352.544.329-34', '1', 'N', '', '', '', ''),
(122, '2', 'Stall Casa de Carnes', '(47) 3642-7638', '08.854.893/0001-04', '1', 'N', '', 'Diego', '', ''),
(123, '2', 'Ari Lajes', '(47) 3645-2735', '03.507.319/0001-04', '1', 'N', '', 'Wesley ou Junior', 'arilajes@bol.com.br', 'cnpj 03507319000104\r\namauri o dono\r\nwesley ou junior filho dele pra contato\r\narilajes@bol.com.br\r\n(47) 3645-2735	\r\nsistema admin\r\nadmin@123\r\npra contador do ari lajes\r\ntributacao\r\nse Ã© simples nacional sem permissao de credito\r\ncfop 5101\r\nsituacao do ipi 53 nao tributada\r\npis e cofins 08 - sem incidencia\r\nultima nfe emitida - serie 10 - numero 040 - do dia 15-03\r\nCertificado em anexo.\r\nSenha: 1234\r\nUsuÃ¡rio receitapr AR Internet: 72973560934 \r\nSenha: queli4014    '),
(124, '2', 'Cilmar pneus ', '(47) 3643-6079', '21.833.013/0001-11', '1', 'N', '', 'Djenifer/CILMAR', 'cilmarpneus@gmail.com', ''),
(125, '2', 'Mina manutencao', '(47) 3643-7101', '05.478.673/0001-75', '1', 'N', '', 'Carmen', '', ''),
(126, '2', 'Guth MÃ¡quinas', '(47) 3645-2176', '11.092.255/0001-48', '2', 'N', '', 'Arlei', '', ''),
(127, '2', 'Santanna Embalagens', '(47) 3642-3066', '06.329.842/0001-78', '1', 'N', '', 'Camillo', '', ''),
(128, '2', 'Fazenda Potrich', '(00) 0000-0000', '00000000000000', '1', 'N', '', '', '', ''),
(129, '2', 'Otica Santa Cecilia', '(47) 3642-4951', '12.687.148/0001-25', '1', 'N', '', '', '', ''),
(130, '2', 'Arten', '(47) 3642-5965', '82.878.851/0001-04', '1', 'N', '', '', '', ''),
(131, '2', 'AL Transportes', '(47) 6666-0000', '06.297.002/0001-70', '1', 'N', '', '', '', '06297002000170\r\n\r\nuser: admin\r\n\r\npass: al@123'),
(132, '2', 'JHP Comercio de Pecas', '(47) 3643-1640', '00.906.335/0001-73', '1', 'N', '', 'Paulinho', '', 'endereÃ§o:\r\nBackupjhp@gmail.com\r\nsenha:\r\njean19sms  cobian e google drive 31-03-17'),
(133, '2', 'Viviane Gabardo', '(47) 3645-3222', '04.249.195/0001-69', '1', 'N', '', 'Viviane', '', ''),
(134, '2', 'Kerico Materiais de Construcao', '(41) 3623-1284', '77.508.976/0001-02', '1', 'N', '', 'Lucas', '', ''),
(135, '2', 'Suvicor Tintas', '(47) 3635-0853', '03.817.857/0001-97', '1', 'N', '', 'Juarez', '', ''),
(136, '2', 'FarmÃ¡cia Viva Vida', '(47) 3642-1780', '79.300.349/0001-07', '1', 'N', '', '', '', 'Team Viewer [FIXO]\r\nid 819 321 870\r\npass 123456 ou t2gx59'),
(137, '2', 'Dipafer', '(47) 3642-5340', '85.177.293/0001-30', '1', 'N', '', 'Gabriel', 'gabriel@dipafer.com.br', ''),
(138, '2', 'VWA Luvas', '(47) 3645-1246', '73.523.128/0001-03', '1', 'N', '', 'Miguel ValÃ©rio', 'mj.valerio2@gmail.com', ''),
(139, '2', 'Kalla Interiores', '(47) 3642-0023', '11.967.217/0001-91', '1', 'N', '', 'Rita', '', 'Team Viewer\r\nid 959 971 487\r\npsw 123456'),
(140, '2', 'Cirineu Wormsbecher', '(47) 4747-4747', '08.882.569/0001-30', '1', 'N', '', 'Cirineu', '', ''),
(141, '2', 'Loja Otilia', '(41) 3623-1563', '79.332.755/0001-43', '1', 'N', '', 'Jessica', '', ''),
(142, '2', 'Canal C', '(47) 3645-3000', '00.840.689/0001-62', '1', 'N', '', '', '', ''),
(143, '2', 'Monisul Transportes', '(47) 2222-2222', '16.900.507/0001-31', '1', 'N', '', 'Sonia', '', ''),
(144, '2', 'Pedro Veiga Suctas', '(47) 3642-2103', '02.253.793/0002-67', '1', 'N', '', 'Eliane', '', ''),
(145, '2', 'Caloi', '(47) 3642-4876', '76.520.808/0001-61', '1', 'N', '', 'Edson Luiz Correia R.', '', ''),
(146, '2', 'Herkon', '(47) 3642-4444', '83.849.935/0001-74', '1', 'N', '', '', 'herkon@terra.com.br', ''),
(147, '2', 'OqCacau', '(47) 3642-4899', '26.093.750/0001-49', '1', 'N', '', 'Claudiane', '', ''),
(148, '2', 'Ferromax', '(47) 3645-3577', '01.167.195/0001-21', '1', 'N', '', 'Silvane', 'estrela_ve@hotmail.com', ''),
(149, '2', 'Roda Vida Transportes', '(47) 3643-0564', '03.211.270/0002-10', '1', 'N', '', 'Analia', '', ''),
(150, '2', 'Grafite Papelaria', '(47) 3642-9666', '24.647.721/0001-56', '1', 'N', '', 'Viviane', '', ''),
(151, '2', 'Da casa esquadrias', '(47) 3643-5029', '03.943.717/0001-65', '1', 'N', '', 'Ilde', '', ''),
(152, '2', 'Dupla Gula Pizzaria', '(47) 3642-6060', '16.436.324/0001-07', '1', 'N', 'FONE 988665559 - TRABALHA APOS AS 18H', 'ELAINE', '', 'bkpduplagula@gmail.com\r\n\r\nsupport@123   COBIAN E GOOGLE DRIVE'),
(153, '2', 'assoart - artesanato rio negro', '(47) 3642-1267', '11.111.111/1111-11', '1', 'N', '', 'relindes/neuci', '', 'seminario 3642-3280 r. 457\r\nrelindes 36422507 casa\r\ne loja-36421267 (neuci)\r\n'),
(154, '2', 'Mercado Prado', '(47) 7777-7777', '03.241.054/0001-37', '1', 'N', '', 'Celio', '', 'Team Viewer Servidor\r\n117 092 240\r\npass 123456\r\n\r\nTeam Viewer Caixa2\r\n225 591 073\r\npass 123456'),
(155, '2', 'Reiturbo comercio de auto pecas', '(47) 3645-2834', '05.349.775/0001-90', '1', 'N', '', '', '', ''),
(156, '2', 'Stefanes mercado dos pneus', '(47) 3642-9122', '74.156.688/0001-30', '1', 'N', '', '', '', ''),
(157, '2', 'Agropecuaria riomafra', '(47) 3642-7370', '11.991.563/0001-05', '1', 'N', '', 'Rafael', '', ''),
(158, '2', 'Boi Nobre', '(47) 0000-0000', '07.609.545/0001-49', '1', 'N', '', 'Junior', '', 'CX1\r\n795 796 285\r\n123456\r\nCX2\r\n795 773 256\r\n123456'),
(159, '2', 'Bossy Distribuidora', '(47) 3642-4064', '18.922.136/0001-14', '1', 'N', '', 'Erenilson', '', ''),
(160, '2', 'Prefeitura Quitandinha', '(41) 3623-1231', '76.002.674/0001-97', '1', 'N', '', '', '', ''),
(161, '2', 'Tiago Tratores', '(41) 3623-1387', '78.420.890/0001-88', '1', 'N', '', 'Tiago', '', ''),
(162, '1', 'Edson Worms', '(00) 0000-0000', '121.212.121-12', '1', 'N', '', 'Edson', '', ''),
(163, '2', 'Lagoa Serena', '(41) 3623-2003', '03.893.743/0001-26', '1', 'N', '', 'Carla', '', ''),
(164, '2', 'PISKE-JOSSP MAT HIGIENICOS LTDA', '(47) 3642-2865', '01.298.958/0001-73', '1', 'N', 'USA WHATS', 'JUNIOR', '', ''),
(165, '2', 'Tornearia Santa Barbara', '(47) 3645-1802', '79.093.530/0001-81', '1', '1', '', 'IBSEN', '', ''),
(166, '2', 'ziemmer motores eletricos ltda', '(47) 3645-1418', '09.303.619/0001-40', '1', 'N', '', '', '', ''),
(167, '2', 'Rocar Auto Pecas', '(47) 3642-8781', '23.113.658/0001-05', '1', '1', '', 'Vanessa (genus) - roberto', 'autopecasrocar@gmail.com', ''),
(168, '2', 'Grams Maquinas', '(47) 3642-4171', '21.529.846/0001-93', '1', 'S', 'GRAMS MAQUINAS EIRELI ME\r\ncnpj 21529846000193\r\nIE 257531467\r\nGENUS\r\n', 'DIEGO', 'gramsinteriores@gmail.com', ''),
(169, '2', 'BC Consultoria', '(47) 3642-4453', '02.523.079/0001-60', '1', 'N', '', 'Bornemann', 'bcconsultoria@bcconsultoriasc.com.br', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `logusuarios`
--

CREATE TABLE `logusuarios` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `login` varchar(100) NOT NULL,
  `data_hora` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `logusuarios`
--

INSERT INTO `logusuarios` (`id`, `id_usuario`, `login`, `data_hora`) VALUES
(1, 2, 'Wellisson Ribeiro', '2017-05-11 11:17:13'),
(2, 2, 'Wellisson Ribeiro', '2017-05-11 11:17:55'),
(3, 2, 'Wellisson Ribeiro', '2017-05-11 08:25:04'),
(4, 2, 'Wellisson Ribeiro', '2017-05-11 08:32:25'),
(5, 2, 'Wellisson Ribeiro', '2017-05-11 08:55:39'),
(6, 2, 'Wellisson Ribeiro', '2017-05-11 10:58:38'),
(7, 2, 'Wellisson Ribeiro', '2017-05-11 13:51:24'),
(8, 4, 'Jean', '2017-05-11 14:05:02'),
(9, 2, 'Wellisson Ribeiro', '2017-05-11 15:05:59'),
(10, 2, 'Wellisson Ribeiro', '2017-05-11 15:58:35'),
(11, 4, 'Jean', '2017-05-11 16:08:02'),
(12, 4, 'Jean', '2017-05-11 17:24:34'),
(13, 2, 'Wellisson Ribeiro', '2017-05-12 08:23:15'),
(14, 2, 'Wellisson Ribeiro', '2017-05-12 08:41:59'),
(15, 2, 'Wellisson Ribeiro', '2017-05-12 10:21:00'),
(16, 4, 'Jean', '2017-05-12 11:38:33'),
(17, 2, 'Wellisson Ribeiro', '2017-05-12 11:40:54'),
(18, 2, 'Wellisson Ribeiro', '2017-05-12 13:29:57'),
(19, 4, 'Jean', '2017-05-12 13:33:31'),
(20, 2, 'Wellisson Ribeiro', '2017-05-12 14:09:21'),
(21, 2, 'Wellisson Ribeiro', '2017-05-12 14:50:43'),
(22, 4, 'Jean', '2017-05-12 16:35:04'),
(23, 2, 'Wellisson Ribeiro', '2017-05-12 17:24:30'),
(24, 2, 'Wellisson Ribeiro', '2017-05-12 17:41:16'),
(25, 2, 'Wellisson Ribeiro', '2017-05-15 08:43:54'),
(26, 4, 'Jean', '2017-05-15 10:26:27'),
(27, 2, 'Wellisson Ribeiro', '2017-05-15 15:29:17'),
(28, 4, 'Jean', '2017-05-15 15:31:50'),
(29, 2, 'Wellisson Ribeiro', '2017-05-15 17:05:47'),
(30, 2, 'Wellisson Ribeiro', '2017-05-16 11:30:13'),
(31, 4, 'Jean', '2017-05-16 12:34:42'),
(32, 2, 'Wellisson Ribeiro', '2017-05-16 13:49:54'),
(33, 2, 'Wellisson Ribeiro', '2017-05-16 17:26:47'),
(34, 2, 'Wellisson Ribeiro', '2017-05-17 08:24:51'),
(35, 2, 'Wellisson Ribeiro', '2017-05-17 08:37:21'),
(36, 2, 'Wellisson Ribeiro', '2017-05-17 09:18:08'),
(37, 4, 'Jean', '2017-05-17 09:35:53'),
(38, 2, 'Wellisson Ribeiro', '2017-05-17 10:58:00'),
(39, 2, 'Wellisson Ribeiro', '2017-05-17 11:03:38'),
(40, 2, 'Wellisson Ribeiro', '2017-05-17 11:31:32'),
(41, 4, 'Jean', '2017-05-17 12:05:19'),
(42, 2, 'Wellisson Ribeiro', '2017-05-17 13:15:25'),
(43, 4, 'Jean', '2017-05-17 14:15:51'),
(44, 2, 'Wellisson Ribeiro', '2017-05-17 16:12:14'),
(45, 4, 'Jean', '2017-05-17 17:06:01'),
(46, 2, 'Wellisson Ribeiro', '2017-05-17 17:33:32'),
(47, 2, 'Wellisson Ribeiro', '2017-05-18 08:21:24'),
(48, 4, 'Jean', '2017-05-18 09:33:12'),
(49, 2, 'Wellisson Ribeiro', '2017-05-18 10:23:26'),
(50, 4, 'Jean', '2017-05-18 10:33:38'),
(51, 2, 'Wellisson Ribeiro', '2017-05-18 11:22:16'),
(52, 2, 'Wellisson Ribeiro', '2017-05-18 15:48:40'),
(53, 2, 'Wellisson Ribeiro', '2017-05-18 16:33:50'),
(54, 4, 'Jean', '2017-05-18 17:05:48'),
(55, 2, 'Wellisson Ribeiro', '2017-05-18 17:37:51'),
(56, 2, 'Wellisson Ribeiro', '2017-05-19 09:26:03'),
(57, 2, 'Wellisson Ribeiro', '2017-05-19 10:53:42'),
(58, 4, 'Jean', '2017-05-19 11:06:51'),
(59, 2, 'Wellisson Ribeiro', '2017-05-19 13:52:15'),
(60, 4, 'Jean', '2017-05-19 13:59:49'),
(61, 2, 'Wellisson Ribeiro', '2017-05-19 15:43:54'),
(62, 4, 'Jean', '2017-05-19 17:26:56'),
(63, 4, 'Jean', '2017-05-21 22:32:39'),
(64, 4, 'Jean', '2017-05-22 10:08:50'),
(65, 2, 'Wellisson Ribeiro', '2017-05-22 10:11:53'),
(66, 4, 'Jean', '2017-05-22 13:27:45'),
(67, 4, 'Jean', '2017-05-22 15:13:37'),
(68, 2, 'Wellisson Ribeiro', '2017-05-22 15:36:21'),
(69, 4, 'Jean', '2017-05-22 16:52:17'),
(70, 2, 'Wellisson Ribeiro', '2017-05-23 08:22:50'),
(71, 2, 'Wellisson Ribeiro', '2017-05-23 08:58:46'),
(72, 4, 'Jean', '2017-05-23 09:00:32'),
(73, 4, 'Jean', '2017-05-23 10:03:15'),
(74, 2, 'Wellisson Ribeiro', '2017-05-23 11:52:47'),
(75, 2, 'Wellisson Ribeiro', '2017-05-23 13:29:54'),
(76, 4, 'Jean', '2017-05-23 13:52:13'),
(77, 2, 'Wellisson Ribeiro', '2017-05-23 13:52:16'),
(78, 4, 'Jean', '2017-05-23 15:27:17'),
(79, 2, 'Wellisson Ribeiro', '2017-05-23 16:12:11'),
(80, 4, 'Jean', '2017-05-23 16:38:51'),
(81, 2, 'Wellisson Ribeiro', '2017-05-23 17:37:19'),
(82, 2, 'Wellisson Ribeiro', '2017-05-23 17:51:00'),
(83, 4, 'Jean', '2017-05-23 17:53:45'),
(84, 2, 'Wellisson Ribeiro', '2017-05-23 19:20:25'),
(85, 2, 'Wellisson Ribeiro', '2017-05-24 08:10:13'),
(86, 2, 'Wellisson Ribeiro', '2017-05-24 08:38:57'),
(87, 4, 'Jean', '2017-05-24 09:46:28'),
(88, 4, 'Jean', '2017-05-24 11:11:32'),
(89, 2, 'Wellisson Ribeiro', '2017-05-24 11:18:43'),
(90, 2, 'Wellisson Ribeiro', '2017-05-24 14:21:12'),
(91, 4, 'Jean', '2017-05-24 15:26:18'),
(92, 2, 'Wellisson Ribeiro', '2017-05-24 16:32:50'),
(93, 2, 'Wellisson Ribeiro', '2017-05-24 17:41:58'),
(94, 2, 'Wellisson Ribeiro', '2017-05-25 08:40:20'),
(95, 4, 'Jean', '2017-05-25 09:21:12'),
(96, 4, 'Jean', '2017-05-25 10:17:50'),
(97, 2, 'Wellisson Ribeiro', '2017-05-25 13:25:09'),
(98, 4, 'Jean', '2017-05-25 13:52:34'),
(99, 2, 'Wellisson Ribeiro', '2017-05-25 16:02:25'),
(100, 2, 'Wellisson Ribeiro', '2017-05-25 16:12:26'),
(101, 2, 'Wellisson Ribeiro', '2017-05-25 16:20:02'),
(102, 2, 'Wellisson Ribeiro', '2017-05-25 16:39:00'),
(103, 2, 'Wellisson Ribeiro', '2017-05-25 16:53:28'),
(104, 2, 'Wellisson Ribeiro', '2017-05-25 17:05:45'),
(105, 2, 'Wellisson Ribeiro', '2017-05-25 17:18:58'),
(106, 4, 'Jean', '2017-05-25 17:44:50'),
(107, 2, 'Wellisson Ribeiro', '2017-05-26 08:09:28'),
(108, 4, 'Jean', '2017-05-26 09:34:27'),
(109, 2, 'Wellisson Ribeiro', '2017-05-26 09:52:02'),
(110, 4, 'Jean', '2017-05-26 10:52:59'),
(111, 2, 'Wellisson Ribeiro', '2017-05-26 10:54:09'),
(112, 4, 'Jean', '2017-05-26 14:14:13'),
(113, 2, 'Wellisson Ribeiro', '2017-05-26 14:24:22'),
(114, 4, 'Jean', '2017-05-26 14:57:06'),
(115, 2, 'Wellisson Ribeiro', '2017-05-26 15:26:51'),
(116, 2, 'Wellisson Ribeiro', '2017-05-26 15:31:51'),
(117, 2, 'Wellisson Ribeiro', '2017-05-26 16:34:13'),
(118, 4, 'Jean', '2017-05-26 16:35:33'),
(119, 2, 'Wellisson Ribeiro', '2017-05-26 17:21:55'),
(120, 2, 'Wellisson Ribeiro', '2017-05-29 08:47:00'),
(121, 4, 'Jean', '2017-05-29 09:50:28'),
(122, 2, 'Wellisson Ribeiro', '2017-05-29 10:09:24'),
(123, 2, 'Wellisson Ribeiro', '2017-05-29 11:49:27'),
(124, 2, 'Wellisson Ribeiro', '2017-05-29 12:54:03'),
(125, 2, 'Wellisson Ribeiro', '2017-05-29 14:20:54'),
(126, 4, 'Jean', '2017-05-29 15:20:03'),
(127, 2, 'Wellisson Ribeiro', '2017-05-29 16:26:31'),
(128, 2, 'Wellisson Ribeiro', '2017-05-29 17:41:40'),
(129, 2, 'Wellisson Ribeiro', '2017-05-30 08:14:53'),
(130, 2, 'Wellisson Ribeiro', '2017-05-30 08:16:17'),
(131, 2, 'Wellisson Ribeiro', '2017-05-30 09:21:04'),
(132, 4, 'Jean', '2017-05-30 10:35:52'),
(133, 2, 'Wellisson Ribeiro', '2017-05-30 11:38:34'),
(134, 2, 'Wellisson Ribeiro', '2017-05-30 13:47:39'),
(135, 4, 'Jean', '2017-05-30 14:26:19'),
(136, 2, 'Wellisson Ribeiro', '2017-05-30 15:14:46'),
(137, 2, 'Wellisson Ribeiro', '2017-05-30 16:13:14'),
(138, 4, 'Jean', '2017-05-30 17:08:27'),
(139, 2, 'Wellisson Ribeiro', '2017-05-30 17:35:21'),
(140, 2, 'Wellisson Ribeiro', '2017-05-31 08:24:12'),
(141, 2, 'Wellisson Ribeiro', '2017-05-31 10:09:33'),
(142, 4, 'Jean', '2017-05-31 11:31:00'),
(143, 2, 'Wellisson Ribeiro', '2017-05-31 11:38:09'),
(144, 2, 'Wellisson Ribeiro', '2017-05-31 13:50:37'),
(145, 2, 'Wellisson Ribeiro', '2017-06-01 08:09:44'),
(146, 2, 'Wellisson Ribeiro', '2017-06-01 08:59:00'),
(147, 2, 'Wellisson Ribeiro', '2017-06-01 09:26:04'),
(148, 4, 'Jean', '2017-06-01 10:05:28'),
(149, 4, 'Jean', '2017-06-01 11:37:56'),
(150, 4, 'Jean', '2017-06-01 12:09:44'),
(151, 2, 'Wellisson Ribeiro', '2017-06-01 13:31:06'),
(152, 4, 'Jean', '2017-06-01 13:38:29'),
(153, 4, 'Jean', '2017-06-01 14:56:11'),
(154, 2, 'Wellisson Ribeiro', '2017-06-01 16:09:14'),
(155, 4, 'Jean', '2017-06-01 18:38:34'),
(156, 2, 'Wellisson Ribeiro', '2017-06-02 09:50:44'),
(157, 4, 'Jean', '2017-06-02 09:51:41'),
(158, 2, 'Wellisson Ribeiro', '2017-06-02 11:50:09'),
(159, 2, 'Wellisson Ribeiro', '2017-06-02 13:22:56'),
(160, 4, 'Jean', '2017-06-02 13:35:00'),
(161, 2, 'Wellisson Ribeiro', '2017-06-02 14:17:11'),
(162, 4, 'Jean', '2017-06-02 15:06:17'),
(163, 2, 'Wellisson Ribeiro', '2017-06-02 16:16:11'),
(164, 2, 'Wellisson Ribeiro', '2017-06-02 17:27:00'),
(165, 2, 'Wellisson Ribeiro', '2017-06-02 17:41:16'),
(166, 2, 'Wellisson Ribeiro', '2017-06-05 08:09:16'),
(167, 2, 'Wellisson Ribeiro', '2017-06-05 08:44:58'),
(168, 4, 'Jean', '2017-06-05 11:13:23'),
(169, 2, 'Wellisson Ribeiro', '2017-06-05 11:20:26'),
(170, 2, 'Wellisson Ribeiro', '2017-06-05 14:15:46'),
(171, 4, 'Jean', '2017-06-05 14:59:18'),
(172, 2, 'Wellisson Ribeiro', '2017-06-05 15:46:11'),
(173, 4, 'Jean', '2017-06-05 16:35:24'),
(174, 2, 'Wellisson Ribeiro', '2017-06-05 17:49:30'),
(175, 2, 'Wellisson Ribeiro', '2017-06-06 08:07:29'),
(176, 4, 'Jean', '2017-06-06 08:56:19'),
(177, 2, 'Wellisson Ribeiro', '2017-06-06 11:44:17'),
(178, 2, 'Wellisson Ribeiro', '2017-06-06 14:09:37'),
(179, 4, 'Jean', '2017-06-06 14:53:01'),
(180, 4, 'Jean', '2017-06-06 15:39:53'),
(181, 2, 'Wellisson Ribeiro', '2017-06-06 15:49:21'),
(182, 2, 'Wellisson Ribeiro', '2017-06-06 17:24:19'),
(183, 2, 'Wellisson Ribeiro', '2017-06-07 08:08:52'),
(184, 2, 'Wellisson Ribeiro', '2017-06-07 08:48:47'),
(185, 4, 'Jean', '2017-06-07 10:13:24'),
(186, 2, 'Wellisson Ribeiro', '2017-06-07 10:56:04'),
(187, 2, 'Wellisson Ribeiro', '2017-06-07 14:03:28'),
(188, 4, 'Jean', '2017-06-07 14:55:29'),
(189, 2, 'Wellisson Ribeiro', '2017-06-07 17:25:47'),
(190, 2, 'Wellisson Ribeiro', '2017-07-10 17:21:01'),
(191, 2, 'Wellisson Ribeiro', '2017-07-10 17:21:52'),
(192, 2, 'Wellisson Ribeiro', '2017-07-11 16:45:49');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mensagens`
--

CREATE TABLE `mensagens` (
  `id` int(11) NOT NULL,
  `id_de` int(11) NOT NULL,
  `id_para` int(11) NOT NULL,
  `nick` varchar(100) NOT NULL,
  `mensagem` varchar(255) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `data_hora` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `mensagens`
--

INSERT INTO `mensagens` (`id`, `id_de`, `id_para`, `nick`, `mensagem`, `ip`, `data_hora`) VALUES
(48, 2, 2, 'Wellisson Ribeiro', 'teste', '::1', '2017-07-12 11:11:11'),
(49, 4, 4, 'Jean', 'Opa', '::1', '2017-07-12 11:13:07');

-- --------------------------------------------------------

--
-- Estrutura da tabela `nivel_acessos`
--

CREATE TABLE `nivel_acessos` (
  `id` int(11) NOT NULL,
  `nome_nivel` varchar(220) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `nivel_acessos`
--

INSERT INTO `nivel_acessos` (`id`, `nome_nivel`, `created`, `modified`) VALUES
(1, 'Administrador', '2015-09-19 00:00:00', NULL),
(2, 'Usuario', '2015-09-27 17:30:26', '2015-09-27 17:34:37');

-- --------------------------------------------------------

--
-- Estrutura da tabela `online`
--

CREATE TABLE `online` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `time` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ordem_servicos`
--

CREATE TABLE `ordem_servicos` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `data_inicial` date NOT NULL,
  `data_final` date NOT NULL,
  `garantia` varchar(100) DEFAULT NULL,
  `descricao` varchar(250) NOT NULL,
  `defeito` varchar(250) NOT NULL,
  `laudo` text NOT NULL,
  `observacoes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `ordem_servicos`
--

INSERT INTO `ordem_servicos` (`id`, `id_cliente`, `id_usuario`, `status`, `data_inicial`, `data_final`, `garantia`, `descricao`, `defeito`, `laudo`, `observacoes`) VALUES
(21, 88, 2, 3, '2017-04-27', '2017-04-27', 'nao possui', 'Notebook com fonte', 'Notebook nao liga', 'asdasdf', 'asdf'),
(22, 82, 2, 2, '2017-04-27', '2017-04-27', '1 ano', 'Produto teste', 'Verificar entradas usb', 'asd', 'asd'),
(24, 123, 3, 1, '2017-04-27', '2017-04-27', 'teste', 'teste', 'teste', 'teste', 'teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `plataforma`
--

CREATE TABLE `plataforma` (
  `id` int(11) NOT NULL,
  `nome_plataforma` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `plataforma`
--

INSERT INTO `plataforma` (`id`, `nome_plataforma`) VALUES
(1, 'Genus'),
(2, 'Sisauto'),
(3, 'Outros');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE `servicos` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `preco` double(10,2) NOT NULL,
  `descricao` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `nome`, `preco`, `descricao`) VALUES
(2, 'Reparacao', 30.00, 'Reparacao Sistema Operacional'),
(3, 'Formatacao sem Backup', 90.00, 'Formatacao de computadores sem salvar os dados');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `cod_usuario` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `senha` longtext NOT NULL,
  `dt_ult_acesso` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(220) NOT NULL,
  `email` varchar(220) NOT NULL,
  `login` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `nivel_acesso_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `inicio` datetime NOT NULL,
  `limite` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `email`, `login`, `senha`, `nivel_acesso_id`, `created`, `modified`, `inicio`, `limite`) VALUES
(2, 'Wellisson Ribeiro', 'welleh10@gmail.com', 'wellisson', 'e803adae1f5acc155699ad43e9b77629', 1, '2017-01-16 10:09:24', '2017-03-20 14:49:41', '2017-07-12 17:40:18', '2017-07-12 17:41:18'),
(4, 'Jean', 'support@support-br.com.br', 'admin', 'e6e061838856bf47e1de730719fb2609', 1, '2017-04-26 08:48:17', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `base_conhecimento`
--
ALTER TABLE `base_conhecimento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_plataforma_fk` (`id_plataforma`);

--
-- Indexes for table `chamados`
--
ALTER TABLE `chamados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `id_usuario_fk` (`id_usuario`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cnpj` (`cnpj`);

--
-- Indexes for table `logusuarios`
--
ALTER TABLE `logusuarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario_fk` (`id_usuario`);

--
-- Indexes for table `mensagens`
--
ALTER TABLE `mensagens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nivel_acessos`
--
ALTER TABLE `nivel_acessos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online`
--
ALTER TABLE `online`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario_fk` (`id`);

--
-- Indexes for table `ordem_servicos`
--
ALTER TABLE `ordem_servicos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plataforma`
--
ALTER TABLE `plataforma`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servicos`
--
ALTER TABLE `servicos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`cod_usuario`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `base_conhecimento`
--
ALTER TABLE `base_conhecimento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `chamados`
--
ALTER TABLE `chamados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=696;
--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;
--
-- AUTO_INCREMENT for table `logusuarios`
--
ALTER TABLE `logusuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;
--
-- AUTO_INCREMENT for table `mensagens`
--
ALTER TABLE `mensagens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `nivel_acessos`
--
ALTER TABLE `nivel_acessos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `online`
--
ALTER TABLE `online`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1460;
--
-- AUTO_INCREMENT for table `ordem_servicos`
--
ALTER TABLE `ordem_servicos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `plataforma`
--
ALTER TABLE `plataforma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `servicos`
--
ALTER TABLE `servicos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `cod_usuario` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `chamados`
--
ALTER TABLE `chamados`
  ADD CONSTRAINT `id_usuario_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
